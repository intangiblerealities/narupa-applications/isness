﻿// Copyright 2018 Mike O'Connor, Mark Wonnacott, Figuring Project

using System.Collections.Generic;
using UnityEngine;

using Text = TMPro.TextMeshProUGUI;

public class CameraSwitcher : MonoBehaviour {

    [SerializeField]
    private Narupa.VR.Multiplayer.SteamVrObjectRenderer headsetRenderer;

    private int currentCameraIndex;

    private Camera currentCamera;


    private Camera mainCamera; 
    [SerializeField]
    private List<Camera> presetCameras;

	[SerializeField]
	private Canvas debugCanvas;
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Text camNameText;

	[SerializeField]
	private bool showDebugMenu = true; 
	
	private void Start()
	{
	    mainCamera = presetCameras[0];
        currentCamera = presetCameras[0];
	    
	}
	// Update is called once per frame
	void Update () {

        var totalCams = presetCameras.Count + headsetRenderer.headsets.count;
	    
        foreach (var camera in presetCameras)
        {
            var cam = camera.GetComponent<UltimateOrbitCamera>();

            if (cam != null)
            {
                cam.target = target;
            }
        }

        //handle if the camera we're currently using gets removed. 
        if(totalCams < currentCameraIndex)
        {
            currentCameraIndex = 0;
            mainCamera.gameObject.SetActive(true);
        }
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            //turn off the current camera object
            currentCamera.gameObject.SetActive(false);
            currentCameraIndex = (currentCameraIndex + 1) % totalCams;
            //cycle through the preset cameras.
            if(currentCameraIndex < presetCameras.Count)
            {
	            currentCamera = presetCameras[currentCameraIndex];
	            currentCamera.gameObject.SetActive(true);
                camNameText.text = currentCamera.name;
            }
            //otherwise, grab the camera by index from the headset renderers, 
            //and set it as the active camera. 
            else
            {
                int camIndex = currentCameraIndex - presetCameras.Count;
                currentCamera = headsetRenderer.headsets[camIndex].GetComponentInChildren<Camera>(true);
                currentCamera.gameObject.SetActive(true);
                camNameText.text = currentCamera.name;
            }
        }

		if (Input.GetKeyDown(KeyCode.M))
		{
			showDebugMenu = !showDebugMenu;
			debugCanvas.gameObject.SetActive(showDebugMenu);
		}
	}
}
