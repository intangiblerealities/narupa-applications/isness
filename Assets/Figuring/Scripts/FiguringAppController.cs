﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Narupa.VR;
using Narupa.VR.Multiplayer;
using VRDevice = Nano.Transport.Variables.VRDevice;
using Narupa.VR.Player.Control;
using Nano.Client;

using Text = TMPro.TextMeshProUGUI;
using InputField = TMPro.TMP_InputField;
using System.IO;
using Nano.Transport.Variables;
using NSB.Utility;
using NSB.MMD.Styles;
using NSB.MMD;
using NSB.Processing;
using System;
using UnityEngine.Profiling;
using Narupa.Math;
using Valve.VR;
using Nano.Transport.Variables.Interaction;
using Narupa.Frontend.Input;
using Narupa.VR.Interaction;

public class FiguringAppController : MonoBehaviour
{
    public static byte PlayerID => SteamVrObjectSender.LocalPlayerID;

    public bool IsReady => playback.Ready;
    public bool IsLive => !useRecording;
    public bool IsRecorded => useRecording;

    public XRInteractionManager XRInteraction;
    public LayerInteractionController LayerInteraction;

    [SerializeField]
    private VrPlaybackRenderer playback;

    [Header("Debug")]
    [SerializeField]
    private bool useRecording;
    [SerializeField]
    private TextAsset recording;
    [SerializeField]
    private int frame, minFrame, maxFrame;
    [SerializeField]
    private Text playbackText;
    [SerializeField]
    private Text fpsText;
    [SerializeField]
    private Camera uicamera;

    [Header("VR Scale")]
    [SerializeField]
    private float VRScale;

    [Header("VR")]
    [SerializeField]
    private WorldLocalSpaceTransformer spaceTransformer;
    [SerializeField]
    private GameObject headsetObject;
    [SerializeField]
    private VRTK.VRTK_HeadsetFade fader;

    [Header("Configuration")]
    [SerializeField]
    private Slider crossfadeSlider;
    [SerializeField]
    private Text crossfadeTimeText;
    [SerializeField]
    private Slider crossfadeTimeSlider;
    [SerializeField]
    private Text lastCrossfadeText;
    [SerializeField]
    private Slider timeSlider;
    [SerializeField]
    private Text timeText;
    [SerializeField]
    private InstancePoolSetup configTabsSetup;
    private InstancePool<FiguringConfig> configTabs;
    [SerializeField]
    private InstancePoolSetup variablesSetup;
    private InstancePool<FiguringConfig.Variable> variableInstances;

    [Header("Sequencing")]
    [SerializeField]
    private InputField sequenceStartInput;
    [SerializeField]
    private InputField sequenceEndInput;
    [SerializeField]
    private Slider sequenceSceneDurationSlider;
    [SerializeField]
    private Slider sequenceCrossDurationSlider;
    [SerializeField]
    private Text sequenceSceneDurationSliderText;
    [SerializeField]
    private Text sequenceCrossDurationSliderText;
    [SerializeField]
    private InputField heartPullIdInput;
    [SerializeField]
    private Text playStatus;

    [Header("Renderering")]
    [SerializeField]
    private Material equipmentMaterial;
    [SerializeField]
    private Material pulseMaterial;
    [SerializeField]
    private Material mannequinMaterial;
    [SerializeField]
    private Material shadowMaterial;
    [SerializeField]
    private Material cordBodyMaterial;
    [SerializeField]
    private Material interactMaterial;
    [SerializeField]
    private Material bodySmokeMaterial;
    [SerializeField]
    private Material stringSmokeMaterial;
    [SerializeField]
    private Material bodyRainbowMaterial;
    [SerializeField]
    private HueStyle hueStyle;
    [SerializeField]
    private HueStyle bodyTrailsStyle;
    [SerializeField]
    private HueStyle blobsStyle;
    [SerializeField]
    private HueStyle bodyBlobsStyle;
    [SerializeField]
    private AtomTrailRenderer trails;
    [SerializeField]
    private AtomTrailRenderer bodyTrails;
    [SerializeField]
    private BondSmokeRenderer stringSmoke;
    [SerializeField]
    private Transform floor;
    [SerializeField]
    private FiguringIKRenderer ikrenderer;
    [SerializeField]
    private Material leftHandStarMaterial, rightHandStarMaterial, bodyStarMaterial;
    [SerializeField]
    private Material stringStarMaterial;
    [SerializeField]
    private GameObject stringStarRenderer;

    [SerializeField]
    private Transform leftHandStar, rightHandStar;

    [SerializeField]
    private LinesRepresentation hueSkeletonLines;
    [SerializeField]
    private LinesRepresentation bodyBondLines;

    private NSBStyle hueSkeletonStyle = new NSBStyle();
    private NSBStyle bodyBondStyle = new NSBStyle();

    private FiguringConfig clipboardConfig;
    private FiguringConfig liveConfig;
    private FiguringConfig activeConfig;
    private FiguringConfig freezeConfig;

    public FiguringConfig Config => activeConfig;

    private Config fadePrevConfig = new FiguringConfig("prev");
    private Config fadeNextConfig = new FiguringConfig("next");
    private Config fadeCurrConfig = new FiguringConfig("curr");

    private List<FiguringConfig> configs = new List<FiguringConfig>();

    public bool receivedCrossfade;
    public float PrevCrossfade;
    public float NextCrossfade;

    private object CrossfadeLock = new object();
    private object ConfigLock = new object();

    private FiguringConfig[] configSequence = new FiguringConfig[64];

    private LiveConfigSequencer<FiguringConfig> sequencer;

    private FiguringRecording activeRecording = new FiguringRecording();
    [SerializeField]
    public bool areRecording;

    private void CheckArguments()
    {
        var args = Environment.GetCommandLineArgs();

        // build launched with command line argument (recording file to playback)
        if (args.Length >= 2 && !Application.isEditor)
        {
            useRecording = true;

            string path = args[1];
            playbackText.text = $"Playing File: '{path}'";
            playback.EnterRecording(path);
        }
        // editor running with useRecording checkbox set
        else if (useRecording)
        {
            playbackText.text = $"Playing: '{recording.name}'";
            playback.EnterRecording(recording.bytes);
        }
        else
        // autoconnect
        {
            StartCoroutine(ConnectToFirstServer());
        }
    }

    private float leftHandValue, leftHandVelocity;
    private float rightHandValue, rightHandVelocity;

    private void UpdateController(XRInteractionManager.Controller controller,
                                  byte deviceID,
                                  Transform transform)
    {

        int heartPullId;

        if (int.TryParse(heartPullIdInput.text, out heartPullId)
         && activeConfig.HeartPullStrength.value > 0.05f
         && controller.Hand.Pose.HasValue)
        {
            heartPullSelection.Clear();
            heartPullSelection.Add(heartPullId);

            var interaction = new Interaction
            {
                GradientScaleFactor = activeConfig.HeartPullStrength.value,
                InteractionType = (byte) InteractionType.SingleAtom,
                PlayerID = SteamVrObjectSender.PlayerId,
                InputID = (byte) (deviceID + 10),
            };

            LayerInteraction.AddLegacyInteraction(controller.Hand.Pose.Value.Position,
                                                  interaction,
                                                  heartPullSelection);
        }
        else
        {
            LayerInteraction.DeleteInteraction((byte) (deviceID + 10));
        }

        if (controller.Interact.IsPressed && controller.Hand.Pose.HasValue)
        {
            var interaction = new Interaction
            {
                GradientScaleFactor = 1,
                InteractionType = (byte) InteractionType.NearestAtomUpdate,
                PlayerID = PlayerID,
                InputID = deviceID,
            };

            LayerInteraction.AddLegacyInteraction(transform.position, interaction);
        }
        else if (controller.Glow.IsPressed && controller.Hand.Pose.HasValue)
        {
            var interaction = new Interaction
            {
                GradientScaleFactor = 0,
                InteractionType = (byte) InteractionType.NearestAtomUpdate,
                PlayerID = PlayerID,
                InputID = deviceID,
            };

            LayerInteraction.AddLegacyInteraction(transform.position, interaction);
        }
        else
        {
            LayerInteraction.DeleteInteraction(deviceID);
        };
    }

    private void UpdateControllers()
    {
        UpdateController(XRInteraction.LeftController, 
                         (byte) SteamVR_Input_Sources.LeftHand,
                         XRInteraction.LeftHandOffset);
        UpdateController(XRInteraction.RightController, 
                         (byte) SteamVR_Input_Sources.RightHand,
                         XRInteraction.RightHandOffset);
    }

    private void Start()
    {
        CustomEncoding.Register();

        playback.OnReady += () =>
        {
            playback.VisibleAtoms.Selection.SetAll(true);
        };

        sequencer = new LiveConfigSequencer<FiguringConfig>(name => new FiguringConfig(name));

        hueSkeletonLines.Frame = playback.InterpolatedFrame;
        hueSkeletonLines.Selection = playback.VisibleAtoms.Selection;
        hueSkeletonLines.Style = hueSkeletonStyle;
        bodyBondLines.Style = bodyBondStyle;

        configTabsSetup.Finalise(ref configTabs);
        variablesSetup.Finalise(ref variableInstances);

        clipboardConfig = new FiguringConfig("clipb");
        liveConfig = new FiguringConfig("live");
        freezeConfig = new FiguringConfig("freeze");
        activeConfig = liveConfig;

        configs.Clear();
        configs.Add(liveConfig);
        configs.Add(clipboardConfig);

        int count = 11;

        for (int i = 0; i < count; ++i)
        {
            var left = new FiguringConfig($"{i + 1}");
            var right = new FiguringConfig($"{i + count + 1}");

            configs.Add(left);
            configs.Add(right);

            configSequence[i] = left;
            configSequence[i + count] = right;
        }

        foreach (var config in configs)
        {
            ReloadConfig(config);
        }

        //configs.Add(sequencer.OutputConfig);

        configTabs.SetActive(configs);
        (configTabs.Get(activeConfig) as ConfigToggleView).Select();

        UpdateVisualsFromVariables();

        CheckArguments();

        timeSlider.gameObject.SetActive(useRecording);
        timeSlider.onValueChanged.AddListener(value =>
        {
            OnTimelineChange(value);
        });
    }

    private IEnumerator ConnectToFirstServer()
    {
        playbackText.text = $"Are you on the right network? Looking for NSB servers";

        SimboxConnectionInfo info = null;
        var search = new SearchForConnections();
        search.DeviceDiscovered += info_ => info = info_;
        search.BeginSearch();

        while (info == null)
        {
            yield return new WaitForSeconds(0.5f);

            playbackText.text += ".";
        }

        search.EndSearch();

        playbackText.text = $"Attempting to join server: {info.Descriptor}";

        playback.NetworkManager.ServerConnectionInitialised += delegate { OnConnected(info); };
        playback.NetworkManager.ServerDiscovered += delegate { OnConnectionLost(); };
        playback.NetworkManager.JoinServer(info);
    }

    private void OnConnected(SimboxConnectionInfo info)
    {
        AddOSCListeners();

        playbackText.text = $"Joined server: {info.Descriptor}";

        playback.MediaPlay();
    }

    private void OnConnectionLost()
    {
        Debug.Log("Connection failed or lost, restarting");
        Restart();
    }

    public void Restart()
    {

        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    private bool frozen;

    private void Freeze()
    {
        frozen = true;
        CopyConfigToConfig(liveConfig, freezeConfig);
        activeConfig = freezeConfig;
    }

    private void UnFreeze()
    {
        activeConfig = liveConfig;
        frozen = false;
    }

    private void AddOSCListeners()
    {
        var osc = playback.CurrentConnection.client.TransportContext.OscManager;

        for (int i = 0; i < fadePrevConfig.variables.Count; ++i)
        {
            int index = i;

            osc.Attach(fadePrevConfig.variables[index].address, e =>
            {
                fadePrevConfig.variables[index].value = e.Get<float>(0);
                fadeNextConfig.variables[index].value = e.Get<float>(1);
            });
        }

        // crossfade between the two values
        osc.Attach("/broadcast/crossfade", ReceiveCrossfadeMessage);

        // freeze config values while receiving crossfade data
        osc.Attach("/broadcast/freeze", e =>
        {
            Debug.Log($"Receive Freeze");

            int senderID = e.Get<byte>();

            if (PlayerID != senderID && activeConfig == liveConfig)
            {
                Freeze();
            }
        });

        // unfreeze config values
        osc.Attach("/broadcast/unfreeze", e =>
        {
            Debug.Log($"Receive Unfreeze");

            int senderID = e.Get<byte>();

            if (PlayerID != senderID && activeConfig == freezeConfig)
            {
                UnFreeze();
            }
        });
    }

    private bool lockTimeline;

    private void OnTimelineChange(float value)
    {
        if (!lockTimeline)
        {
            playback.SetFrame(value);
        }
    }

    private void ReceiveCrossfadeMessage(Rug.Osc.OscMessage message)
    {
        ReceiveCrossfade(message.Get<float>());
    }

    private void ReceiveCrossfade(float crossfade)
    {
        lock (CrossfadeLock)
        {
            receivedCrossfade = true;
            NextCrossfade = crossfade;
            PrevCrossfade = NextCrossfade;
        }
    }

    private void CheckHotkeys()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            Screen.fullScreen = !Screen.fullScreen;
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            uicamera.targetDisplay = 1 - uicamera.targetDisplay;
        }
    }

    private void Update()
    {
        CheckHotkeys();

        UpdateControllers();

        if (IsLive && IsReady)
        {
            SendVRObjects();
            SendChangedLiveVariables();
        }
        
        UpdateVisualsFromVariables();
        OptimiseForHiddenRenderers();

        crossfadeSlider.value = NextCrossfade;
        crossfadeTimeText.text = $"Timed-crossfade: {NextCrossfade}s";

        if (PrevCrossfade != NextCrossfade)
        {
            if (IsLive)
            {
                var client = playback.CurrentConnection.client;
                var message = new Rug.Osc.OscMessage("/broadcast/crossfade", NextCrossfade);

                client.Send(message);
            }

            PrevCrossfade = NextCrossfade;
        }

        InterpolateFadeVariables(NextCrossfade);

        VRScale = MultiplayerSteamVrCalibration.MultiplayerScale.x;

        // sequencing
        //sequenceCrossDurationSlider.value = Mathf.Min(sequenceCrossDurationSlider.value, sequenceSceneDurationSlider.value);

        sequenceSceneDurationSliderText.text = $"Scene Duration: {sequenceSceneDurationSlider.value}s";
        sequenceCrossDurationSliderText.text = $"Fade Duration: {sequenceCrossDurationSlider.value}s";

        fpsText.text = $"{1f / Time.smoothDeltaTime:0.0}fps";
    }

    private bool sequencePaused = false;
    private Coroutine sequenceCoroutine;

    public void PlaySequence()
    {
        if (sequencePaused)
        {
            sequencePaused = false;
            return;
        }

        ResetSequence();

        sequenceCoroutine = StartCoroutine(SequenceCO());
    }

    private void SetStatus(string status)
    {
        playStatus.text = status;
        lastCrossfadeText.text = status;
    }

    private IEnumerator SequenceCO()
    {
        //StartRecording();

        int begin = int.Parse(sequenceStartInput.text) - 1;
        int end = int.Parse(sequenceEndInput.text) - 1;

        Debug.Log($"{begin} -> {end}");

        if (begin >= end)
            yield break;

        Debug.Log($"Set {begin}");
        StopCrossfade();
        FadeLiveToSelectedConfig(1, configSequence[begin]);

        //float sceneDuration = sequenceSceneDurationSlider.value;
        float crossDuration = sequenceCrossDurationSlider.value;

        for (int i = begin; i < end; ++i)
        {
            var sceneDuration = configSequence[i].SceneDuration.value;
            var realCrossDuration = Mathf.Min(crossDuration, sceneDuration);

            Debug.Log($"Wait {i}");
            SetStatus($"waiting in {configSequence[i].name}");
            yield return new WaitForSeconds(sceneDuration - realCrossDuration);

            StopCrossfade();
            FadeLiveToSelectedConfig(0, configSequence[i + 1]);
            crossfade = StartCoroutine(TimedCrossfade(realCrossDuration));

            Debug.Log($"Fade {i} -> {i + 1}");
            SetStatus($"crossfade {configSequence[i].name} to {configSequence[i + 1].name}");
            yield return crossfade;
        }

        SetStatus($"waiting on {configSequence[end].name}");

        //StopRecoding();

        yield break;
    }

    public void PauseSequence()
    {
        sequencePaused = true;
    }

    public void ResetSequence()
    {
        sequencePaused = false;
        if (sequenceCoroutine != null)
            StopCoroutine(sequenceCoroutine);
        StopCrossfade();

        StopRecoding();

        SetStatus("Stopped.");
    }

    /// <summary>
    /// See if any live variables were changed manually and broadcast new values if
    /// they were
    /// </summary>
    private void SendChangedLiveVariables()
    {
        var client = playback.CurrentConnection.client;

        foreach (var variable in liveConfig.variables)
        {
            if (variable.dirty)
            {
                // set prev/next as the same value to force a constant value during crossfade
                client.Send(new Rug.Osc.OscMessage(variable.address, variable.value, variable.value));
                variable.dirty = false;
            }
        }
    }

    public void SendFadeVariables(Config prevConfig, Config nextConfig)
    {
        var client = playback.CurrentConnection.client;

        CopyConfigToConfig(prevConfig, fadePrevConfig);
        CopyConfigToConfig(nextConfig, fadeNextConfig);

        for (int i = 0; i < fadePrevConfig.variables.Count; ++i)
        {
            string address = fadePrevConfig.variables[i].address;
            float prev = fadePrevConfig.variables[i].value;
            float next = fadeNextConfig.variables[i].value;

            client.Send(new Rug.Osc.OscMessage(address, prev, next));
        }
    }

    public void InterpolateFadeVariables(float u)
    {
        for (int i = 0; i < fadePrevConfig.variables.Count; ++i)
        {
            float prev = fadePrevConfig.variables[i].value;
            float next = fadeNextConfig.variables[i].value;

            liveConfig.variables[i].value = Mathf.Lerp(prev, next, u);
        }
    }

    private void SnapMaterialHSV(Material material, FiguringConfig.HSVColor hsv)
    {
        Color color = hsv.value;

        if (hsv.Val.value <= 0.0099f)
        {
            color.a = 0;
        }

        material.SetColor("_TintColor", color);
    }

    private static void VariableToMaterialGrayscale(Material material, Config.Variable variable)
    {
        material.SetColor("_TintColor", Color.white * variable.value);
    }

    private static void VariableToMaterialTint(Material material, FiguringConfig.HSVColor variable)
    {
        material.SetColor("_TintColor", variable.value);
    }

    private void OptimiseForHiddenRenderers()
    {
        bodyTrails.gameObject.SetActive(activeConfig.BodyAtomTrailsColor.Val.value >= 0.0001f);
        stringSmoke.gameObject.SetActive(activeConfig.StringSmokeColor.Val.value >= 0.0001f);
        trails.gameObject.SetActive(activeConfig.StringAtomTrailsColor.Val.value >= 0.0001f);

        bool stringStar = activeConfig.StringStarColor.Val.value >= 0.0001f
                       && activeConfig.StringStarSize.value >= 0.0001f;

        stringStarRenderer.gameObject.SetActive(stringStar);
    }

    private void UpdateVisualsFromVariables()
    {
        variableInstances.SetActive(activeConfig.variables);
        //variableInstances.sort = false;
        variableInstances.Refresh();

        //MultiplayerSteamVrCalibration.MultiplayerScale = Vector3.one * activeConfig.StringScaling.value;
        ikrenderer.Scaling = activeConfig.BodyScaling.value;
        
        fader.Fade(Color.Lerp(Color.black, Color.clear, activeConfig.Lighting.value), 0);

        VariableToMaterialGrayscale(shadowMaterial, activeConfig.ShadowBrightness);
        //VariableToMaterialGrayscale(pulseMaterial, activeConfig.PulseBrightness);

        VariableToMaterialTint(interactMaterial, activeConfig.InteractColor);

        VariableToMaterialGrayscale(equipmentMaterial, activeConfig.EquipmentValue);
        

        //SnapMaterialHSV(mannequinMaterial, activeConfig.MannequinColor);

        VariableToMaterialTint(bodySmokeMaterial, activeConfig.BodySmokeColor);
        VariableToMaterialTint(bodyRainbowMaterial, activeConfig.BodySmokeColor);

        VariableToMaterialTint(stringSmokeMaterial, activeConfig.StringSmokeColor);

        VariableToMaterialTint(stringStarMaterial, activeConfig.StringStarColor);

        var frame = playback.InterpolatedFrame;
        int atomCount = frame.AtomCount;

        {
            hueSkeletonLines.Settings.Width.Value = activeConfig.StringBondLinesThickness.value;

            hueSkeletonStyle.AtomColors.Resize(atomCount);
            hueSkeletonStyle.AtomRadiuses.Resize(atomCount);

            float u = 1f / atomCount;
            float off = activeConfig.StringBondLinesColor.Hue.value;
            float sat = activeConfig.StringBondLinesColor.Sat.value;
            float val = activeConfig.StringBondLinesColor.Val.value;

            for (int i = 0; i < atomCount; ++i)
            {
                float hue = (i * u + off) % 1;

                if (off > 0) hue = off;

                hueSkeletonStyle.AtomRadiuses[i] = 1;
                hueSkeletonStyle.AtomColors[i] = FasterMath.HSV(hue, sat, val);
            }
        }

        {
            bodyBondLines.Settings.Width.Value = 0;

            int ikAtomCount = ikrenderer.fakeFrame.AtomCount;

            bodyBondStyle.AtomColors.Resize(ikAtomCount);
            bodyBondStyle.AtomRadiuses.Resize(ikAtomCount);

            float u = 1f / ikAtomCount;
            float off = 0;
            float sat = 0;
            float val = activeConfig.BodyBondLinesValue.value;

            for (int i = 0; i < ikAtomCount; ++i)
            {
                float hue = (i * u + off) % 1;

                if (off > 0) hue = off;

                bodyBondStyle.AtomRadiuses[i] = 1;
                bodyBondStyle.AtomColors[i] = FasterMath.HSV(hue, sat, val);
            }
        }

        {
            hueStyle.Cycle = activeConfig.StringAtomTrailsColor.Hue.value;
            hueStyle.fixedHue = hueStyle.Cycle > 0;
            hueStyle.Saturation = activeConfig.StringAtomTrailsColor.Sat.value;
            hueStyle.Value = activeConfig.StringAtomTrailsColor.Val.value;

            bodyTrailsStyle.Cycle = activeConfig.BodyAtomTrailsColor.Hue.value;
            bodyTrailsStyle.fixedHue = bodyTrailsStyle.Cycle > 0;
            bodyTrailsStyle.Saturation = activeConfig.BodyAtomTrailsColor.Sat.value;
            bodyTrailsStyle.Value = activeConfig.BodyAtomTrailsColor.Val.value;

            blobsStyle.Cycle = activeConfig.StringAtomBlobsColor.Hue.value;
            blobsStyle.fixedHue = blobsStyle.Cycle > 0;
            blobsStyle.Saturation = activeConfig.StringAtomBlobsColor.Sat.value;
            blobsStyle.Value = activeConfig.StringAtomBlobsColor.Val.value;
            blobsStyle.Scaling = activeConfig.StringAtomBlobsSize.value;

            bodyBlobsStyle.Cycle = 0;
            bodyBlobsStyle.fixedHue = bodyBlobsStyle.Cycle > 0;
            bodyBlobsStyle.Saturation = 0;
            bodyBlobsStyle.Value = 0;
            bodyBlobsStyle.Scaling = activeConfig.BodyAtomBlobsSize.value;

            trails.Settings.Scaling.Value = activeConfig.StringAtomBlobsSize.value * 0.2f;
            trails.Settings.Time.Value = activeConfig.StringAtomTrailsLength.value;
            trails.Settings.MinimumLength.Value = activeConfig.StringAtomTrailsScaling.value;

            bodyTrails.Settings.Scaling.Value = activeConfig.BodyAtomBlobsSize.value * 0.2f;
            bodyTrails.Settings.Time.Value = activeConfig.BodyAtomTrailsLength.value;
            bodyTrails.Settings.MinimumLength.Value = activeConfig.BodyAtomTrailsScaling.value;

            floor.transform.position = Vector3.zero;
            
            VariableToMaterialTint(bodyStarMaterial, activeConfig.HeartColor);
        }
    }

    private bool spectate = false;

    public void ToggleSpectate()
    {
        spectate = !spectate;
    }

    private int FindInteractionIndex(VRInteraction[] interactions, 
                                     VRDevice device,
                                     byte playerID)
    {
        for (int i = 0; i < interactions.Length; ++i)
        {
            var interaction = interactions[i];

            if (interaction.DeviceType == (byte) device
             && interaction.Interaction.PlayerID == playerID)
            {
                return i;
            }
        }

        return 0;
    }

    private NSBSelection heartPullSelection = new NSBSelection();

    private void SendVRObjects()
    {
        var connection = playback.NetworkManager.CurrentConnection;
        
        // don't send vr positions if we haven't loaded any frames yet
        if (playback.NextFrame == null) return;

        // only send vr positions if a headset is connected and you are not spectating
        if (spectate || !UnityEngine.XR.XRDevice.isPresent) return;
        
        if (connection == null) return;

        connection.ClearInputs();

        var zero = new Transformation(Vector3.zero, Quaternion.identity, Vector3.one);

        var head = SteamVrObjectSender.CreateVRInteraction(spaceTransformer, headsetObject.transform, VRDevice.Headset, PlayerID);
        var left = SteamVrObjectSender.CreateVRInteraction(spaceTransformer, XRInteraction.LeftController.Hand.Pose ?? zero, VRDevice.LeftController, PlayerID);
        var right = SteamVrObjectSender.CreateVRInteraction(spaceTransformer, XRInteraction.RightController.Hand.Pose ?? zero, VRDevice.RightController, PlayerID);

        var inputs = new List<VRInteraction> { head, right, left };

        connection.UpdateVRInputs(inputs);

        var interactions = playback.NextFrame.VRInteractions;

        if (interactions.Length > 0)
        {
            // overwrite local frame with our positions
            int headi = FindInteractionIndex(interactions, VRDevice.Headset, PlayerID);
            int lefti = FindInteractionIndex(interactions, VRDevice.LeftController, PlayerID);
            int righti = FindInteractionIndex(interactions, VRDevice.RightController, PlayerID);

            interactions[headi] = head;
            interactions[lefti] = left;
            interactions[righti] = right;
        }
    }

    public void TestSaveConfig()
    {
        File.WriteAllText($"{Application.persistentDataPath}/{activeConfig.name}.json", activeConfig.ToJson());
    }

    public void TestLoadConfig()
    {
        ReloadConfig(activeConfig);
    }

    public void MakeSelectedConfigLive()
    {
        SetStatus($"waiting in {activeConfig.name}");
        FadeLiveToSelectedConfig(1, activeConfig);
    }

    public void FadeLiveToSelectedConfig(float u, FiguringConfig config)
    {
        if (!IsLive) return;

        var client = playback.CurrentConnection.client;

        client.Send(new Rug.Osc.OscMessage("/broadcast/freeze", PlayerID));
        client.Send(new Rug.Osc.OscMessage("/broadcast/crossfade", u));
        SendFadeVariables(liveConfig, config);
        client.Send(new Rug.Osc.OscMessage("/broadcast/unfreeze", PlayerID));
        
        PrevCrossfade = u;
        NextCrossfade = u;
    }

    public void ReloadConfig(Config config)
    {
        try
        {
            config.FromJson(File.ReadAllText($"{Application.persistentDataPath}/{config.name}.json"));
        }
        catch (Exception)
        {
            try
            {
                config.FromJson(File.ReadAllText($"{Application.streamingAssetsPath}/DefaultConfigs/{config.name}.json"));
                Debug.Log($"Loaded default config: {config.name}");
            }
            catch (Exception e)
            {
                Debug.Log($"Couldn't load config: {config.name}");
                Debug.LogException(e);
            }
        }
    }

    public void CopyConfigToConfig(Config a, Config b)
    {
        foreach (var variable in a.variables)
        {
            b.variables.First(v => v.address == variable.address).value = variable.value;
        }
    }

    public void CopyToClipboard()
    {
        CopyConfigToConfig(activeConfig, clipboardConfig);
    }

    public void PasteFromClipboard()
    {
        CopyConfigToConfig(clipboardConfig, activeConfig);
    }

    public void SetActiveConfig(FiguringConfig config)
    {
        activeConfig = config;
        variableInstances.SetActive(config.variables);
        variableInstances.Refresh();
    }

    private Coroutine crossfade;

    public void StopCrossfade()
    {
        if (crossfade != null)
            StopCoroutine(crossfade);
    }

    public IEnumerator TimedCrossfade(float duration)
    {
        float min = 0;
        float max = duration;
        float time = min;

        while (true)
        {
            while (sequencePaused)
                yield return null;

            time += Time.deltaTime;
            float u = Mathf.InverseLerp(0, duration, time);

            NextCrossfade = u;

            if (u == 1)
                yield break;

            yield return null;
        }
    }

    public void OpenConfigsFolder()
    {
        Application.OpenURL($"file:///{Application.persistentDataPath}");
    }

    public void StartRecording()
    {
        areRecording = true;
        activeRecording.Reset();
        activeRecording.Start();
    }

    public void StopRecoding()
    {
        areRecording = false;
        activeRecording.Stop();

        string name = activeRecording.startTime.ToString("yyyy-MM-dd-HH-mm");
        string path = $"{Application.persistentDataPath}/recordings";
        Directory.CreateDirectory(path);

        File.WriteAllText($"{path}/{name}.json", LitJson.JsonMapper.ToJson(activeRecording));
    }
}
