﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using System.Collections.Generic;
using Nano.Transport.Variables;
using UnityEngine;

using Narupa.VR.Multiplayer;
using Narupa.VR.Player.Control;

using Nano.Client;
using NSB.Utility;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using Narupa.VR;

/// <summary>
/// Multiplayer SteamVR renderer.
/// </summary>
public class FiguringIKRenderer : MonoBehaviour, IFrameSource, ISelectionSource, IRefreshable
{
    [SerializeField]
    private FiguringAppController controller;
    [SerializeField]
    private VrPlaybackRenderer playback;
    [SerializeField]
    private Transform eye;

    public float Scaling = 1;

    [SerializeField]
    private InstancePoolSetup bodiesSetup;
    private InstancePool<byte> bodiesPool;

    [SerializeField]
    private Transform sceneScaling;

    private WorldLocalSpaceTransformer vrTransform;
    private float lerpSpeed = 2f;

    public NSBFrame fakeFrame = new NSBFrame();
    public NSBSelection selection = new NSBSelection();

    NSBFrame IFrameSource.Frame => fakeFrame;
    NSBSelection ISelectionSource.Selection => selection;

    [Header("Interaction")]
    public LineRenderer interactionTemplate;
    public IndexedPool<LineRenderer> interactionPool;

    private void Start()
    {
        bodiesSetup.Finalise(ref bodiesPool);
        interactionPool = new IndexedPool<LineRenderer>(interactionTemplate);

        vrTransform = FindObjectOfType<WorldLocalSpaceTransformer>();

        int maxAtoms = 4 * 256;

        fakeFrame = new NSBFrame
        {
            AtomPositions = new Vector3[maxAtoms],
            AtomTypes = new ushort[maxAtoms],
            BondPairs = new BondPair[maxAtoms],
        };
    }

    private void SetTransform(Transform transform, VRInteraction interaction)
    {
        {
            var raw = interaction.Interaction.Position;
            var prev = new Vector3(raw.X, raw.Y, raw.Z);
            var next = vrTransform.TransformNarupaSpacePointToWorldSpace(prev);

            transform.position = next;
        }

        {
            var raw = interaction.Quaternion;
            var prev = new Quaternion(raw.X, raw.Y, raw.Z, raw.W);
            var next = vrTransform.TransformNarupaSpaceQuaternionToWorldSpace(prev);

            transform.rotation = next;
        }

        {
            var scale = Vector3.one * (1f / MultiplayerSteamVrCalibration.MultiplayerScale.z);
            transform.localScale = scale;
        }
    }

    private List<Transform> points = new List<Transform>();
    private Dictionary<Transform, int> pointIDs = new Dictionary<Transform, int>();
    private List<BondPair> lines = new List<BondPair>();
    private HashSet<byte> clearList = new HashSet<byte>();

    private void Update()
    {
        if (playback.NextFrame == null
         || playback.NextFrame.VRInteractions == null)
        {
            return;
        }

        clearList.Clear();
        bodiesPool.MapActive(i => clearList.Add(i.config));

        foreach (var interaction in playback.NextFrame.VRInteractions)
        {
            byte id = interaction.Interaction.PlayerID;
            var body = bodiesPool.Get<FiguringIKIndividual>(id);
            var device = (VRDevice) interaction.DeviceType;

            clearList.Remove(id);

            if (device == VRDevice.Headset)
            {
                SetTransform(body.headTransform, interaction);

                if (id == FiguringAppController.PlayerID)
                {
                    eye.position = body.headTransform.position;
                }
            }
            else if (device == VRDevice.LeftController)
            {
                SetTransform(body.leftHandTransform, interaction);
            }
            else if (device == VRDevice.RightController)
            {
                SetTransform(body.rightHandTransform, interaction);
            }
        }

        foreach (var clear in clearList)
        {
            bodiesPool.Discard(clear);
        }
    }

    public void Refresh()
    {
        points.Clear();
        pointIDs.Clear();
        lines.Clear();

        if (playback.NextFrame == null) return;

        var interactions = playback.NextFrame.InteractionForceInfo;
        float width = controller.Config.InteractThickness.value;

        interactionPool.SetActive(interactions.Length);
        interactionPool.MapActive((i, line) =>
        {
            var interaction = interactions[i];
            
            float mult = interaction.Force.LengthSquared <= 0.1f ? 0 : 1f;

            line.widthMultiplier = width * mult;
            line.SetPosition(0, Narupa.VR.Renderers.Interaction.InteractionRenderer.ToUnityVec3(interaction.Position));
            line.SetPosition(1, Narupa.VR.Renderers.Interaction.InteractionRenderer.ToUnityVec3(interaction.SelectedAtomsCentre));
        });

        bodiesPool.MapActive(body =>
        {
            body.transform.localScale = Vector3.one * Scaling;

            // populate frame
            var ik = body as FiguringIKIndividual;
            
            for (int i = 0; i < ik.transforms.Count; ++i)
            {
                var transform = ik.transforms[i];
                pointIDs[transform] = points.Count;
                points.Add(transform);
            }

            foreach (var bone in ik.bones)
            {
                lines.Add(new BondPair(pointIDs[bone.a], pointIDs[bone.b]));
            }
        });

        fakeFrame.AtomCount = points.Count;

        for (int i = 0; i < points.Count; ++i)
        {
            fakeFrame.AtomPositions[i] = points[i].position;
            fakeFrame.AtomTypes[i] = 0;
        }

        fakeFrame.BondCount = lines.Count;

        for (int i = 0; i < lines.Count; ++i)
        {
            fakeFrame.BondPairs[i] = lines[i];
        }

        selection.SetSourceAtomCount(fakeFrame.AtomCount);
        selection.SetAll(true);
    }
}
