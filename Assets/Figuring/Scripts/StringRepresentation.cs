﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using UnityEngine;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;
using NSB.MMD.Base;
using NSB.MMD.MeshGeneration;
using NSB.Utility;

public class StringRepresentation : MMDRenderer
{
    [Serializable]
    public class StringConfig : RendererConfig
    {
        //public FloatParameter Width;
        //public FloatParameter Sharpness;

        protected override void Setup()
        {
            //Width = AddFloat(nameof(Width), .025f, 0, .1f, dirtying: false);
            //Sharpness = AddFloat(nameof(Sharpness), 1, 0, 1, dirtying: false);
        }
    }

    [Header("Test")]
    public Material material;
    public int triangleCount;
    public float triangleMinLifespan, triangleMaxLifespan;
    public int jumpLimit = 3;

    public StringConfig Settings = new StringConfig();
    public override RendererConfig Config => Settings;

    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;

    protected MeshTool mesh;
    protected List<Vector3Int> triangles = new List<Vector3Int>();
    protected List<float> time = new List<float>();
    protected List<float> speed = new List<float>();

    private void Start()
    {
        AddMesh(ref meshFilter, ref meshRenderer);

        mesh = new MeshTool(new Mesh(), MeshTopology.Triangles);
        meshFilter.sharedMesh = mesh.mesh;
        meshRenderer.material = material;

        Resize(triangleCount);
    }
    
    protected virtual void RandomiseTriangle(int i)
    {
        int jump1 = Random.Range(jumpLimit / 2, jumpLimit + 1);
        int jump2 = Random.Range(jumpLimit / 2, jumpLimit + 1);

        var tri = new Vector3Int();
        tri.x = Random.Range(0, Frame.AtomCount);
        tri.y = (tri.x + jump1) % Frame.AtomCount;
        tri.z = (tri.y + jump2) % Frame.AtomCount;

        triangles[i] = tri;
        time[i] = 0;
        speed[i] = 1f / Random.Range(triangleMinLifespan, triangleMaxLifespan);
    }

    public void Resize(int triangleCount)
    {
        triangles.Resize(triangleCount);
        time.Resize(triangleCount);
        speed.Resize(triangleCount);
        mesh.SetVertexCount(triangleCount * 3);
        mesh.SetIndexCount(triangleCount * 3);

        var a = new Vector2(0, 0);
        var b = new Vector2(1, 0);
        var c = new Vector2(.5f, 1);

        for (int i = 0; i < triangleCount; ++i)
        {
            mesh.indices[i * 3 + 0] = i * 3 + 0;
            mesh.indices[i * 3 + 1] = i * 3 + 1;
            mesh.indices[i * 3 + 2] = i * 3 + 2;

            mesh.uv0s[i * 3 + 0] = a;
            mesh.uv0s[i * 3 + 1] = b;
            mesh.uv0s[i * 3 + 2] = c;

            time[i] = 1;
            speed[i] = 1;
        }

        mesh.Apply(indices: true, positions: true, uv0s: true);
    }

    public override void Refresh()
    {
        if (Frame == null || Frame.AtomPositions == null) return;

        var white = new Color32(255, 255, 255, 255);

        for (int i = 0; i < triangles.Count; ++i)
        {
            var tri = triangles[i];
            var time = this.time[i];
            
            mesh.positions[i * 3 + 0] = Frame.AtomPositions[tri.x];
            mesh.positions[i * 3 + 1] = Frame.AtomPositions[tri.y];
            mesh.positions[i * 3 + 2] = Frame.AtomPositions[tri.z];

            var color = FasterMath.Mul(white, Mathf.PingPong(time * 2, 1));
            mesh.colors[i * 3 + 0] = color;
            mesh.colors[i * 3 + 1] = color;
            mesh.colors[i * 3 + 2] = color;
            
            time = Mathf.Clamp01(time + speed[i] * Time.deltaTime);

            if (time >= 1)
            {
                RandomiseTriangle(i);
            }
            else
            {
                this.time[i] = time;
            }
        }

        mesh.Apply(positions: true, colors: true);
    }
}