﻿using Narupa.Frontend.Input;
using Narupa.Frontend.XR;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR;
using UnityEngine.XR;
using Narupa.Math;
using System;

public class XRInteractionManager : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField]
    private bool useTrackers;

    [Header("Controller Actions")]
    [SerializeField]
    private SteamVR_Action_Boolean controllerInteractAction;
    [SerializeField]
    private SteamVR_Action_Boolean controllerGlowAction;
    [SerializeField]
    private SteamVR_Action_Pose controllerPose;
    #pragma warning restore 0649

    public Controller LeftController, RightController;

    public Transform LeftHandPivot, LeftHandOffset;
    public Transform RightHandPivot, RightHandOffset;

    private List<Action> updateCallbacks = new List<Action>();

    private void Awake()
    {
        SetupControllers();
    }

    private void Update()
    {
        foreach (var update in updateCallbacks)
            update();
    }

    public void SetupControllers()
    {
        LeftController = CreateSteamVRController(SteamVR_Input_Sources.LeftHand, XRNode.LeftHand);
        RightController = CreateSteamVRController(SteamVR_Input_Sources.RightHand, XRNode.RightHand);

        RightController.Hand.PoseChanged += () => RightController.Hand.Pose.Value.CopyToTransform(RightHandPivot);
        LeftController.Hand.PoseChanged += () => LeftController.Hand.Pose.Value.CopyToTransform(LeftHandPivot);
    }

    [ContextMenu("Swap Hand Poses")]
    public void SwapHandPoses()
    {
        var temp = LeftController.Hand;
        LeftController.Hand = RightController.Hand;
        RightController.Hand = temp;
    }

    [ContextMenu("Swap Hand Buttons")]
    public void SwapHandButtons()
    {
        var temp = LeftController.Interact;
        LeftController.Interact = RightController.Interact;
        RightController.Interact = temp;
    }

    private Controller CreateSteamVRController(SteamVR_Input_Sources source, XRNode unityNode)
    {
        var handPose = controllerPose.WrapAsPosedObject(source);
        var altPose = new DirectPosedObject();

        updateCallbacks.Add(() =>
        {
            altPose.SetPose(unityNode.GetSinglePose());
        });

        var multiplex = new DirectPosedObject();

        altPose.PoseChanged += () =>
        {
            if (altPose.Pose.HasValue)
                multiplex.SetPose(altPose.Pose);
        };

        handPose.PoseChanged += () =>
        {
            if (handPose.Pose.HasValue)
                multiplex.SetPose(handPose.Pose);
        };

        var controller = new Controller
        {
            Hand = multiplex,
            Interact = controllerInteractAction.WrapAsButton(source),
            Glow = controllerGlowAction.WrapAsButton(source),
        };

        return controller;
    }

    private IEnumerator TrackerControllerCO(ulong trackerID, DirectPosedObject poser)
    {
        while (true)
        {
            var state = XRNode.HardwareTracker.GetNodeStates()
                                              .FirstOrDefault(s => s.uniqueID == trackerID);

            if (state.uniqueID == trackerID)
            {
                Vector3 position;
                Quaternion rotation;

                if (state.TryGetPosition(out position)
                 && state.TryGetRotation(out rotation))
                {
                    poser.SetPose(new Transformation(position, rotation, Vector3.one));
                }
                else
                {
                    poser.SetPose(null);
                }
            }

            yield return null;
        }
    }

    private Controller CreateNullController()
    {
        var controller = new Controller
        {
            Hand = new DirectPosedObject(),
            Interact = new DirectButton(),
            Glow = new DirectButton(),
        };

        return controller;
    }

    public class Controller
    {
        public IPosedObject Hand;
        public IButton Interact;
        public IButton Glow;
    }
}
