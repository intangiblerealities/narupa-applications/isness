﻿// Copyright 2018 Mark Wonnacott, Alex Jones, Figuring Project

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using System.Linq;
using UnityEngine.Audio;
using System;
using NSB.Processing;
using NSB.MMD.RenderingTypes;
using NSB.Utility;

public class FiguringContactsRenderer : StringRepresentation, 
                                        ISelectionSource,
                                        IStyleSource
{
    public FrameSource FrameSource;
    public AudioMixer mixer;

    public new NSBSelection Selection = new NSBSelection();
    NSBSelection ISelectionSource.Selection => Selection;

    public NSBStyle Style = new NSBStyle();
    NSBStyle IStyleSource.Style => Style;

    //===========================================================================================
    private int numSynthVoices = 16;
    private List<float> secondCounters = new List<float>();
    private List<float> timers = new List<float>();
    private List<float> triggerPause = new List<float>();

    public float distanceRange = 3.0f;
    public float normDistanceInverted;
    //===========================================================================================

    private List<TopologyTools.AtomPairDistance> contacts = new List<TopologyTools.AtomPairDistance>();
    private List<TopologyTools.AtomPairDistance> distances = new List<TopologyTools.AtomPairDistance>();

    public float threshold;
    public int jumpThreshold;

    [Range(1f, 10f)]
    public float starSpeed;
    public Transform testStar;
    public int starP, starA, starB;
    public float starU;

    public LineRenderer loop;
    public LineRenderer shadow;
    public IndexedPool<LineRenderer> shadowLines;
    public Transform floor;

    private List<int> nearestBackbone;

    private HashSet<Nano.Client.BondPair> activeContacts = new HashSet<Nano.Client.BondPair>();

    private void Awake()
    {
        shadowLines = new IndexedPool<LineRenderer>(shadow);

        for (int i = 0; i < numSynthVoices; i++)
        {
            secondCounters.Add(0);
            timers.Add(0);
            triggerPause.Add(0);
        }
    }

    private int FindIndexInSelection(int id)
    {
        for (int i = 0; i < Selection.Count; ++i)
        {
            if (Selection[i] == id) return i;
        }

        return 0;
    }

    private int DeltaID(int a, int b)
    {
        int l = Mathf.Min(a, b);
        int r = Mathf.Max(a, b);

        int dr = Frame.AtomCount - r - 1;
        int dl = l;

        return Mathf.Min(Mathf.Abs(l - r), Mathf.Abs(dr - dl));
    }

    public void ResetTopology()
    {
        Selection.Clear();

        if (TopologyTools.FindBackboneFromBonds(Frame, Selection) > 0)
        {
            var sorted = TopologyTools.TopoSort(Frame, Selection);
            // fix 60-ALA issue?
            sorted.Sort();
            Selection.Clear();
            Selection.AddRange(sorted);
        }
    }

    public override void Refresh()
    {
        if (Frame == null) return;

        Profiler.BeginSample("Refresh FiguringContactsRenderer");
        
        // disable eletricity thingy
        //base.Refresh();

        // keep trying until the first time it works
        if (Selection.Count == 0)
        {
            ResetTopology();
        }

        // find pairwise distances then filter distances by some measure of "contact"
        TopologyTools.ComputePairwiseDistances(Frame, Selection, distances);

        contacts.Clear();

        for (int i = 0; i < distances.Count; ++i)
        {
            var pair = distances[i];

            if (pair.distance < threshold && DeltaID(pair.a, pair.b) >= jumpThreshold)
            {
                contacts.Add(pair);
            }
        }

        // draw the backbone as a line loop
        loop.positionCount = Selection.Count;
        float y = floor.position.y;

        for (int i = 0; i < Selection.Count; ++i)
        {
            var pos = Frame.AtomPositions[Selection[i]];

            pos = transform.TransformPoint(pos);

            pos.y = y;

            loop.SetPosition(i, pos);
        }

        //DoAudioStuff();

        activeContacts.Clear();
        activeContacts.UnionWith(contacts.Select(contact => new Nano.Client.BondPair(contact.a, contact.b)));

        // draw the contact lines
        shadowLines.SetActive(contacts.Count);

        for (int i = 0; i < contacts.Count; ++i)
        {
            var line = shadowLines[i];
            var pair = contacts[i];

            Vector3 apos = Frame.AtomPositions[pair.a];
            Vector3 bpos = Frame.AtomPositions[pair.b];

            var a = transform.TransformPoint(apos);
            var b = transform.TransformPoint(bpos);

            a.y = y;
            b.y = y;

            line.SetPosition(0, a);
            line.SetPosition(1, b);
        };
        
        Profiler.EndSample();
    }

    public void DoAudioStuff()
    {
        for (int i = 0; i < numSynthVoices; i++)
        {
            secondCounters[i] = secondCounters[i] + Time.deltaTime;
        }

        System.Random randy = new System.Random();
        // detect contact changes
        foreach (var contact in contacts)
        {
            var pair = new Nano.Client.BondPair(contact.a, contact.b);

            if (!activeContacts.Contains(pair))
            {
                // new contact
                //Debug.Log("new contact");

                // Find a timer that is at 0 and trigger a note for that voice 
                bool stop = false;
                for (int i = 0; i < numSynthVoices && !stop; i++)
                {
                    if (timers[i] == 0.0)
                    {
                        String freqID = "freq" + (i + 1);
                        String trigID = "trig" + (i + 1);

                        float r = randy.Next(0, 100) * 0.01f;
                        r *= 0.3f;
                       
                        float waitTime = randy.Next(0, 100) * 0.01f;
                        waitTime = (waitTime * 0.5f) + 0.2f;

                        // first calculate the frequency corresponding to the distance of the contact
                        //float freqVal = Mathf.Clamp01(distances[i] * 0.4f + r);
                        float d = distances[i].distance;

                        float dNorm = d / distanceRange;

                        float dNormInverted = 1.0f - dNorm;

                        normDistanceInverted = dNormInverted;

                        //float freqVal = randy.Next(0, 100) * 0.01f;
                        float freqVal = normDistanceInverted;


                        // set the voice to that frequency
                        mixer.SetFloat(freqID, freqVal);
                        // set the trigger to 1
                        mixer.SetFloat(trigID, 1.0f);
                        // and then start the timer that will reset it 0 after a period

                       // Debug.Log("Starting " + i);
                        StartCoroutine(ResetTriggerAfterTime(i, 1f));
                        // start the timer that will make the voice available to be reset after a given time
                        //StartCoroutine(StartVoiceTimer(i, waitTime));
                        StartCoroutine(StartVoiceTimer(i, 0.3f));
                        stop = true;
                    }
                }
            }
        }
    }

    public IEnumerator StartVoiceTimer(int index, float speed)
    {
        while (timers[index] < 1.0f)
        {
            timers[index] += (Time.deltaTime * speed);
            yield return null;
        }
        timers[index] = 0.0f;
    }

    public IEnumerator ResetTriggerAfterTime(int index, float speed)
    {
        while (triggerPause[index] < 0.5)
        {
            triggerPause[index] += Time.deltaTime * speed;
            yield return null;
        }
        String id = "trig" + (index + 1);
        mixer.SetFloat(id, 0.0f);
        triggerPause[index] = 0;
    }
}
