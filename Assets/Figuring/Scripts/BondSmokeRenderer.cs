﻿using NSB.MMD.Base;
using NSB.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BondSmokeRenderer : MMDRenderer
{
    [System.Serializable]
    public class BondSmokeConfig : RendererConfig
    {
        public FloatParameter Scaling, Time, MinimumLength;

        protected override void Setup()
        {
            Scaling = AddFloat("Scaling", .333f, 0, 2, dirtying: false);
            Time = AddFloat("Time", .333f, 0, 15, dirtying: false);
            MinimumLength = AddFloat("Minimum Length", 1, 0.01f, 8, dirtying: false);
        }
    }

    public BondSmokeConfig Settings = new BondSmokeConfig();
    public override RendererConfig Config => Settings;

    public FiguringAppController controller;

    [Header("Smoke")]
    public ParticleSystem smokeTemplate;
    private IndexedPool<ParticleSystem> smokePool;

    private void Awake()
    {
        smokePool = new IndexedPool<ParticleSystem>(smokeTemplate);
    }

    public override void Refresh()
    {
        smokePool.SetActive(Frame.BondCount);

        float scale = 1 / transform.lossyScale.z;

        float lifetime = controller.Config.StringSmokeLifetime.value;
        float distribution = controller.Config.StringSmokeDistribution.value;
        float density = controller.Config.StringSmokeDensity.value;

        for (int i = 0; i < Frame.BondCount; ++i)
        {
            var bond = Frame.BondPairs[i];
            var part = smokePool[i];

            Vector3 atomA = Frame.AtomPositions[bond.A];
            Vector3 atomB = Frame.AtomPositions[bond.B];

            var pos = Vector3.LerpUnclamped(atomA, atomB, 0.5f);
            var rot = Quaternion.LookRotation(atomA - atomB);
            var len = (atomA - atomB).magnitude * scale * 0.5f * 0.7f;

            part.transform.localPosition = pos;
            part.transform.localRotation = rot;

            var main = part.main;
            main.startLifetime = lifetime;
            main.startSize = distribution;

            var emission = part.emission;
            emission.rateOverTimeMultiplier = density;

            var shape = part.shape;
            shape.radius = len;
        }
    }
}
