﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.Base;
using NSB.Utility;
using UnityEngine;

namespace NSB.MMD
{
    public class AtomPrefabRenderer : MMDRenderer
    {
        [System.Serializable]
        public class PrefabConfig : RendererConfig
        {
            public FloatParameter Scaling;

            protected override void Setup()
            {
                Scaling = AddFloat("Scaling", .333f, 0, 2, dirtying: false);
            }
        }

        [Header("Setup")]
        [SerializeField]
        private Transform template;
        private IndexedPool<Transform> instances;

        public PrefabConfig Settings = new PrefabConfig();
        public override RendererConfig Config => Settings;

        [SerializeField]
        private FiguringAppController app;

        private void Awake()
        {
            instances = new IndexedPool<Transform>(template);
        }

        public override void Refresh()
        {
            if (!gameObject.activeInHierarchy) return;

            var positions = Frame.AtomPositions;

            instances.SetActive(Selection.Count);

            for (int i = 0; i < instances.count; ++i)
            {
                var transform = instances[i];
                
                transform.localPosition = positions[Selection[i]];
                transform.localScale = Vector3.one * (0.05f * app.Config.StringStarSize.value);
            }
        }
    }

}
