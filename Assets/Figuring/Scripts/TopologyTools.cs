﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using NSB.Processing;
using NSB.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Assertions;

public static class TopologyTools
{
    public struct AtomPairDistance
    {
        public int a, b;
        public float distance;
    }

    [ThreadStatic]
    private static List<int> selection = new List<int>();
    [ThreadStatic]
    private static List<byte> counts = new List<byte>();

    [ThreadStatic]
    private static Queue<int> open = new Queue<int>();
    [ThreadStatic]
    private static HashSet<int> closed = new HashSet<int>();

    /// <summary>
    /// Expand the given selection to include with atom ids forming a backbone of 
    /// carbon atoms bonded to at least two other carbon atoms, ordered by id.
    /// </summary>
    public static int FindBackboneFromBonds(NSBFrame frame, NSBSelection selection)
    {
        FindBackboneFromBonds(frame, TopologyTools.selection);
        selection.AddRange(TopologyTools.selection);

        return TopologyTools.selection.Count;
    }

    /// <summary>
    /// Repopulate the given list with a selection of atom ids forming a backbone
    /// of carbon atoms bonded to at least two other carbon atoms, ordered by id.
    /// </summary>
    public static int FindBackboneFromBonds(NSBFrame frame, List<int> backbone)
    {
        int carbon = (int) Nano.Science.Element.Carbon;
        int atomCount = frame.AtomCount;
        int bondCount = frame.BondCount;

        // reset neighbour counts
        counts.Resize(atomCount);
        
        for (int id = 0; id < atomCount; ++id)
        {
            counts[id] = 0;
        }

        // when two carbons are bonded together, increment both their neighbour
        // counts
        for (int i = 0; i < frame.BondCount; ++i)
        {
            var bond = frame.BondPairs[i];

            if (frame.AtomTypes[bond.A] == carbon
             && frame.AtomTypes[bond.B] == carbon)
            {
                counts[bond.A] += 1;
                counts[bond.B] += 1;
            }
        }

        // the backbone is all carbons bonded to at least two other carbons
        backbone.Clear();

        for (int id = 0; id < frame.AtomCount; ++id)
        {
            if (counts[id] >= 2)
            {
                backbone.Add(id);
            }
        }

        return backbone.Count;
    }

    /// <summary>
    /// Repopulate the given list with the pairwise distances of atoms in the given
    /// selection.
    /// </summary>
    public static void ComputePairwiseDistances(NSBFrame frame, 
                                                NSBSelection selection, 
                                                List<AtomPairDistance> distances)
    {
        int selectionCount = selection.Count;

        distances.Clear();

        var distance = default(AtomPairDistance);

        for (int i = 0; i < selectionCount; ++i)
        {
            distance.a = selection[i];

            for (int j = i + 1; j < selectionCount; ++j)
            {
                distance.b = selection[j];
                distance.distance = Vector3.Distance(frame.AtomPositions[distance.a],
                                                     frame.AtomPositions[distance.b]);
                distances.Add(distance);
            }
        }
    }

    /// <summary>
    /// Generate and return a mapping of atom id to list of bonded atom ids.
    /// 
    /// TODO: it would be possible to pre-allocate memory for this if
    /// neighbours are limited to a certain count (4 is probably fine in most
    /// cases?)
    /// </summary>
    public static List<List<int>> BondsToAdjacency(NSBFrame frame)
    {
        var adjacency = new List<List<int>>();
        adjacency.Resize(frame.AtomCount);
        
        for (int i = 0; i < adjacency.Count; ++i)
        {
            adjacency[i] = new List<int>();
        }

        for (int i = 0; i < frame.BondCount; ++i)
        {
            var bond = frame.BondPairs[i];

            adjacency[bond.A].Add(bond.B);
            adjacency[bond.B].Add(bond.A);
        }

        return adjacency;
    }

    public static List<int> TopoSort(NSBFrame frame, NSBSelection selection)
    {
        Assert.IsTrue(selection.Count > 0, "Selection to sort is empty.");

        open.Clear();
        closed.Clear();

        var sorted = new List<int>();
        var adjacency = BondsToAdjacency(frame);

        int start = selection[0];

        open.Enqueue(start);

        while (open.Count > 0 && sorted.Count < selection.Count)
        {
            int sub = open.Dequeue();
            var neighbours = adjacency[sub];
            
            foreach (int neighbour in neighbours)
            {
                if (!closed.Contains(neighbour)
                 && !open.Contains(neighbour))
                {
                    open.Enqueue(neighbour);
                }
            }

            closed.Add(sub);

            if (selection.Contains(sub))
            {
                sorted.Add(sub);
            }
        }

        return sorted;
    }

    /// <summary>
    /// Generate a mapping of atom id to nearest selected atom id
    /// </summary>
    public static List<int> NearestSelected(NSBFrame frame, NSBSelection selection)
    {
        Assert.IsTrue(selection.Count > 0, "Selection is empty.");

        open.Clear();
        closed.Clear();
        
        var nearest = new List<int>();
        nearest.Resize(frame.AtomCount);
        var adjacency = BondsToAdjacency(frame);
        
        for (int i = 0; i < selection.Count; ++i)
        {
            int id = selection[i];

            open.Enqueue(id);
            nearest[id] = id;
        }

        while (open.Count > 0)
        {
            int sub = open.Dequeue();
            var neighbours = adjacency[sub];

            foreach (int neighbour in neighbours)
            {
                if (closed.Contains(neighbour)) continue;

                if (!open.Contains(neighbour))
                {
                    open.Enqueue(neighbour);
                    nearest[neighbour] = nearest[sub];
                }
            }

            closed.Add(sub);
        }

        return nearest;
    }
}
