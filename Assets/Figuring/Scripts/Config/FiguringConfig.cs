﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using UnityEngine;

public class FiguringConfig : Config
{
    //public Variable StringScaling;
    public Variable SceneDuration;
    public Variable HeartPullStrength;
    public Variable BodyScaling;

    public HSVColor StringStarColor;
    public Variable StringStarSize;

    public Variable Lighting;
    //public Variable FloorHeight;
    public Variable ShadowBrightness;
    
    public HSVColor InteractColor;
    public Variable InteractThickness;

    public Variable EquipmentValue;
    public Variable BodyAtomBlobsSize;
    public Variable BodyBondLinesValue;
    //public HSVColor MannequinColor;

    public HSVColor BodySmokeColor;
    public Variable BodySmokeLifetime;
    public Variable BodySmokeDistribution;
    public Variable BodySmokeDensity;

    public HSVColor StringSmokeColor;
    public Variable StringSmokeLifetime;
    public Variable StringSmokeDistribution;
    public Variable StringSmokeDensity;

    public HSVColor StringAtomBlobsColor;
    public Variable StringAtomBlobsSize;
    public HSVColor StringBondLinesColor;
    public Variable StringBondLinesThickness;

    public HSVColor StringAtomTrailsColor;
    public Variable StringAtomTrailsLength;
    public Variable StringAtomTrailsScaling;

    public HSVColor BodyAtomTrailsColor;
    public Variable BodyAtomTrailsLength;
    public Variable BodyAtomTrailsScaling;

    public HSVColor HeartColor;
    public HSVColor HandColor;
    public Variable HandStarMinimum;
    public Variable HandSize;
    public Variable BodyStarSize;
    public Variable HandStarStrength;

    protected int groupCount;

    public class HSVColor
    {
        public Variable Hue, Sat, Val;

        public Color value => Color.HSVToRGB(Hue.value, Sat.value, Val.value);
    }

    public FiguringConfig(string name) : base(name)
    {
        //StringScaling = AddVariable("STRING-scaling", value: 1, max: 1.5f);

        SceneDuration = AddVariable("SCENE-DURATION", value: 30f, min: 1f, max: 600f);
        HeartPullStrength = AddVariable("HEART-PULL-STRENGTH", value: 0, max: 5f);
        BodyScaling = AddVariable("BODY-scaling", value: 1, max: 10f);

        EquipmentValue = AddVariable("equipment-value");
        BodyBondLinesValue = AddVariable("BODY-bonds-value");
        BodyAtomBlobsSize = AddVariable("BODY-atom-size", value: .5f, max: 5, logarithmic: true);

        //FloorHeight = AddVariable("floor-height", value: 0f, min: -5f);

        Lighting = AddVariable("lighting", value: 1, logarithmic: true);
        ShadowBrightness = AddVariable("shadow-brightness", value: .5f, logarithmic: true);
        
        StringAtomBlobsColor = AddHSVColor("STRING-atom-color");
        StringAtomBlobsSize = AddVariable("STRING-atom-size", value: .5f, logarithmic: true);
        StringBondLinesColor = AddHSVColor("STRING-bonds-color");
        StringBondLinesThickness = AddVariable("STRING-bonds-thickness", value: 0, max: .15f, logarithmic: true);

        StringStarColor = AddHSVColor("STRING-STAR-color", value: Color.white);
        StringStarSize = AddVariable("STRING-STAR-size", value: 1f, max: 5f);

        StringAtomTrailsColor = AddHSVColor("string-trails-color");
        StringAtomTrailsLength = AddVariable("string-trails-length", value: 5f, min: 0f, max: 15f, logarithmic: true);
        //StringAtomTrailsThickness = AddVariable("string-trails-thickness", value: .333f, min: 0.01f, max: 1f, logarithmic: true);
        StringAtomTrailsScaling = AddVariable("string-trails-scaling", value: 1f, min: 0.01f, max: 8f);

        BodyAtomTrailsColor = AddHSVColor("body-trails-color");
        BodyAtomTrailsLength = AddVariable("body-trails-length", value: 5f, min: 0f, max: 15f, logarithmic: true);
        BodyAtomTrailsScaling = AddVariable("body-trails-scaling", value: 1f, min: 0.01f, max: 8f);

        //CordColor = AddHSVColor("cord-color", new Color(.45f, .45f, .37f));
        InteractColor = AddHSVColor("interact-color");
        InteractThickness = AddVariable("interact-thickness", value: 1f, min: 0f, max: 10f);

        
        //MannequinColor = AddHSVColor("mannequin-color");

        BodySmokeColor = AddHSVColor("body-smoke-color");
        BodySmokeLifetime = AddVariable("body-smoke-lifetime", value: 1, min: 0f, max: 2f);
        BodySmokeDistribution = AddVariable("body-smoke-distribution", value: 0.5f);
        BodySmokeDensity = AddVariable("body-smoke-density", value: 1f, max: 100f, logarithmic: true);

        StringSmokeColor = AddHSVColor("string-smoke-color");
        StringSmokeLifetime = AddVariable("string-smoke-lifetime", value: 1, min: 0f, max: 2f);
        StringSmokeDistribution = AddVariable("string-smoke-distribution", value: 0.5f);
        StringSmokeDensity = AddVariable("string-smoke-density", value: 1f, max: 100f, logarithmic: true);

        HeartColor = AddHSVColor("star-color", value: Color.white);
        HandColor = AddHSVColor("hand-color", value: Color.white);
        HandSize = AddVariable("hand-size", value: 1f, min: 0.1f, max: 5f);
        BodyStarSize = AddVariable("star-body-size", value: 1f, min: .1f, max: 5f);
        HandStarMinimum = AddVariable("star-hand-minimum", value: .1f);
        HandStarStrength = AddVariable("star-hand-time", min: .1f, max: 10f, value: 1f);

        //BorderDistance = AddVariable("border-distance", value: 7f, min: 5f, max: 50f);
        //BorderWidth = AddVariable("border-width", value: 8f, min: 1f, max: 20f);
        //BorderValue = AddVariable("border-value", value: 0f);
    }

    protected Color GetNewGroupColor()
    {
        float hue = (groupCount * (.5f + .2f)) % 1;
        groupCount += 1;

        return Color.HSVToRGB(hue, .75f, .75f);
    }

    public override Variable AddVariable(string name, 
                                         string address = null, 
                                         float value = 0, 
                                         float min = 0, 
                                         float max = 1,
                                         bool logarithmic = false)
    {
        var variable = base.AddVariable(name, address, value, min, max, logarithmic);

        variable.groupColor = GetNewGroupColor();

        return variable;
    }

    public HSVColor AddHSVColor(string name, Color value = default(Color), bool hue = true)
    {
        float h, s, v;

        Color.RGBToHSV(value, out h, out s, out v);

        var color = new HSVColor
        {

        };

        if (hue)
        {
            color.Hue = base.AddVariable($"{name}-hue", value: h);
        }
        else
        {
            color.Hue = new Variable(this, $"{name}-hue", "null-address");
        }

        color.Sat = base.AddVariable($"{name}-sat", value: s);
        color.Val = base.AddVariable($"{name}-val", value: v, logarithmic: true);

        Color group = GetNewGroupColor();
        color.Hue.groupColor = group;
        color.Sat.groupColor = group;
        color.Val.groupColor = group;

        return color;
    }
}
