﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using UnityEngine;
using System.Collections.Generic;
using LitJson;

public class Config
{
    public class Variable
    {
        public Config config;
        public string name;
        public string address;
        public float value;
        public float min = 0, max = 1;
        public bool dirty;

        public Color groupColor;
        public bool logarithmic;

        public Variable(Config config, 
                        string name, 
                        string address, 
                        float value = 0,
                        float min = 0,
                        float max = 1,
                        Color groupColor = default(Color),
                        bool logarithmic = false)
        {
            this.config = config;
            this.name = name;
            this.address = address;
            this.value = value;
            this.min = min;
            this.max = max;
            this.groupColor = groupColor;
            this.logarithmic = logarithmic;
        }

        public void Set(float value)
        {
            if (Mathf.Abs(value - this.value) > 0.001f)
            {
                this.value = value;
                dirty = true;
            }
        }
    }

    public string name;
    public List<Variable> variables = new List<Variable>();

    public Config(string name)
    {
        this.name = name;
    }

    public virtual Variable AddVariable(string name,
                                        string address = null,
                                        float value = 0,
                                        float min = 0,
                                        float max = 1,
                                        bool logarithmic = false)
    {
        address = address ?? $"{name}";
        address = $"/broadcast/{address}";

        var variable = new Variable(this, name, address, value, min, max, logarithmic: logarithmic);

        variables.Add(variable);

        return variable;
    }

    public string ToJson()
    {
        var json = new JsonData();
        json.SetJsonType(JsonType.Object);
        
        foreach (var variable in variables)
        {
            json[variable.address] = variable.value;
        }

        return json.ToJson();
    }

    public void FromJson(string json)
    {
        var data = JsonMapper.ToObject(json);

        foreach (var variable in variables)
        {
            if (data.Keys.Contains(variable.address))
            {
                variable.value = (float) data[variable.address].GetReal();
            }
        }
    }
}
