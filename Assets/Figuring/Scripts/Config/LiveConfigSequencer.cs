﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using System.Collections.Generic;
using UnityEngine;

public class LiveConfigSequencer<TConfig>
    where TConfig : Config
{
    public readonly TConfig OutputConfig;
    public string PrevConfigName { get; private set; }
    public string NextConfigName { get; private set; }
    public float CrossfadeProgress { get; private set; }

    private readonly Dictionary<string, TConfig> configs = new Dictionary<string, TConfig>();
    private readonly System.Func<string, TConfig> factory;

    private TConfig prevConfig, nextConfig;

    public LiveConfigSequencer(System.Func<string, TConfig> factory)
    {
        this.factory = factory;
        OutputConfig = factory("cross");
    }

    private TConfig GetStageConfig(string stage)
    {
        TConfig config;

        if (!configs.TryGetValue(stage, out config))
        {
            config = factory(stage);
            configs[stage] = config;
        }

        return config;
    }

    private void SetStageVariable(string stage, string address, float value)
    {
        var config = GetStageConfig(stage);

        Config.Variable variable = config.variables.Find(v => v.address == address);
        variable.Set(value);
    }

    private void SetCrossfadeStages(string prevStage, string nextStage)
    {
        PrevConfigName = prevStage;
        NextConfigName = nextStage;

        prevConfig = GetStageConfig(prevStage);
        nextConfig = GetStageConfig(nextStage);
    }

    private void SetCrossfadeProgress(float progress)
    {
        CrossfadeProgress = progress;

        for (int i = 0; i < OutputConfig.variables.Count; ++i)
        {
            float prevValue = prevConfig.variables[i].value;
            float nextValue = nextConfig.variables[i].value;
            float lerpValue = Mathf.LerpUnclamped(prevValue, nextValue, progress);
            OutputConfig.variables[i].Set(lerpValue);
        }
    }

    private void CrossfadeStages(string prevStage, string nextStage, float u)
    {
        SetCrossfadeStages(prevStage, nextStage);
        SetCrossfadeProgress(u);
    }
}
