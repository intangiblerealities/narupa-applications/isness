﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using NSB.Processing;
using UnityEngine;

public class ConfigHistory<TConfig>
    where TConfig : Config
{
    public readonly TConfig Config;
    private int frameOffset;
    private float[,] valuesHistory;

    public ConfigHistory(TConfig config)
    {
        Config = config;
    }

    public void LoadFromFrameHistory(FrameHistory<NSBFrame> history)
    {
        frameOffset = history.FirstFrame;
        valuesHistory = new float[history.Count, Config.variables.Count];

        for (int i = 1; i < history.Count; ++i)
        {
            // copy values from last frame
            for (int v = 0; v < Config.variables.Count; ++v)
            {
                valuesHistory[i, v] = valuesHistory[i - 1, v];
            }

            // update the values from messages
            var messages = history[i + frameOffset].Messages;

            // TODO: freeze/unfreeze, variable prev/next, crossfade
        }
    }

    public void SetConfigFromFrame(TConfig config, float frame)
    {
        int frameIndex = frameOffset + Mathf.RoundToInt(frame);

        for (int v = 0; v < config.variables.Count; ++v)
        {
            config.variables[v].value = valuesHistory[frameIndex, v];
        }
    }
}
