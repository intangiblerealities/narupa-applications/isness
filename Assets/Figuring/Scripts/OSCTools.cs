﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using Rug.Osc;

public static class OSCExtensions
{
    public static T Get<T>(this OscMessage @event, int index=0)
    {
        return (T) @event[index];
    }
}
