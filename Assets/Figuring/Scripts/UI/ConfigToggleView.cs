﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;
using Text = TMPro.TextMeshProUGUI;

public class ConfigToggleView : InstanceView<FiguringConfig>
{
    [SerializeField]
    private FiguringAppController app;
    [SerializeField]
    private Toggle toggle;
    [SerializeField]
    private Text labelText;

    public void Select()
    {
        toggle.isOn = true;
    }

    public override void Setup()
    {
        toggle.onValueChanged.AddListener(active =>
        {
            if (active) app.SetActiveConfig(config);
        });
    }

    public override void Refresh()
    {
        labelText.text = config.name;
    }
}
