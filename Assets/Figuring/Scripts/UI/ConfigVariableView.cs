﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;

using Text = TMPro.TextMeshProUGUI;

public class ConfigVariableView : InstanceView<Config.Variable>
{
    [SerializeField]
    private Text labelText;
    [SerializeField]
    private Text valueText;
    [SerializeField]
    private Slider valueSlider;
    [SerializeField]
    private Image fillImage;
    [SerializeField]
    private Image backgroundImage;

    private bool locked = false;

    public override void Setup()
    {
        valueSlider.onValueChanged.AddListener(v =>
        {
            if (locked) return;

            if (config.logarithmic && v > 0)
            {
                v = Mathf.Pow(v, 2);
            }

            config.Set(Mathf.Lerp(config.min, config.max, v));
        });
    }

    public override void Refresh()
    {
        if (labelText.isActiveAndEnabled)
        {
            labelText.text = config.name;
            valueText.text = config.value.ToString("0.00");
        }

        float value = Mathf.InverseLerp(config.min, config.max, config.value);

        if (config.logarithmic && value > 0)
        {
            value = Mathf.Pow(value, .5f);
        }

        locked = true;
        valueSlider.minValue = 0;
        valueSlider.maxValue = 1;
        valueSlider.value = value;
        locked = false;

        fillImage.color = config.groupColor;
        var color = config.groupColor * 0.5f;
        color.a = 1;
        backgroundImage.color = color;
    }
}
