﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using UnityEngine;
using Narupa.VR.Multiplayer;

public class BadParticleScaler : MonoBehaviour
{
    private new ParticleSystemRenderer renderer;

    private void Start()
    {
        renderer = GetComponent<ParticleSystemRenderer>();
    }

    private void Update()
    {
        renderer.maxParticleSize = 1 * MultiplayerSteamVrCalibration.MultiplayerScale.z;
    }
}
