﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using UnityEngine;
using System.Collections.Generic;

using System;
using Random = UnityEngine.Random;
using NSB.MMD.Base;
using NSB.MMD.MeshGeneration;
using Narupa.VR.Multiplayer;
using NSB.Utility;

public class StringInteractions : MMDRenderer
{
    [Serializable]
    public class StringInteractionConfig : RendererConfig
    {
        //public FloatParameter Width;
        //public FloatParameter Sharpness;

        protected override void Setup()
        {
            //Width = AddFloat(nameof(Width), .025f, 0, .1f, dirtying: false);
            //Sharpness = AddFloat(nameof(Sharpness), 1, 0, 1, dirtying: false);
        }
    }

    [Header("Test")]
    public Material material;
    public int triangleCount;
    public float triangleMinLifespan, triangleMaxLifespan;
    public float radius;

    public StringInteractionConfig Settings = new StringInteractionConfig();
    public override RendererConfig Config => Settings;

    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;

    private MeshTool mesh;
    private List<float> time = new List<float>();
    private List<float> speed = new List<float>();

    private void Start()
    {
        AddMesh(ref meshFilter, ref meshRenderer);

        mesh = new MeshTool(new Mesh(), MeshTopology.Triangles);
        meshFilter.sharedMesh = mesh.mesh;
        meshRenderer.material = material;

        Resize(triangleCount);
    }
    
    private void EraseTriangle(int i)
    {
        mesh.positions[i * 3 + 0] = Vector3.zero;
        mesh.positions[i * 3 + 1] = Vector3.zero;
        mesh.positions[i * 3 + 2] = Vector3.zero;
    }

    private Vector3 ToUnityVec3(SlimMath.Vector3 v)
    {
        return new Vector3(v.X, v.Y, v.Z);
    }

    private void RandomiseTriangle(int i)
    {
        if (Frame.InteractionForceInfo.Length == 0)
        {
            EraseTriangle(i);
            return;
        }

        var interaction = Frame.InteractionForceInfo[Random.Range(0, Frame.InteractionForceInfo.Length)];

        var p1 = ToUnityVec3(interaction.Position);
        var p2 = ToUnityVec3(interaction.SelectedAtomsCentre);

        var p3 = p2 + Random.insideUnitSphere * radius;

        if (Random.value < 0.5f)
        {
            p3 = p1 + Random.insideUnitSphere * radius / MultiplayerSteamVrCalibration.MultiplayerScale.z;
        }

        //float radius = this.radius / MultiplayerSteamVrCalibration.MultiplayerScale.z;

        mesh.positions[i * 3 + 0] = p1 + Random.insideUnitSphere * radius;
        mesh.positions[i * 3 + 1] = p2 + Random.insideUnitSphere * radius;
        mesh.positions[i * 3 + 2] = p3;
       
        time[i] = 0;
        speed[i] = 1f / Random.Range(triangleMinLifespan, triangleMaxLifespan);
    }

    public void Resize(int triangleCount)
    {
        time.Resize(triangleCount);
        speed.Resize(triangleCount);
        mesh.SetVertexCount(triangleCount * 3);
        mesh.SetIndexCount(triangleCount * 3);

        var a = new Vector2(0, 0);
        var b = new Vector2(1, 0);
        var c = new Vector2(.5f, 1);

        for (int i = 0; i < triangleCount; ++i)
        {
            mesh.indices[i * 3 + 0] = i * 3 + 0;
            mesh.indices[i * 3 + 1] = i * 3 + 1;
            mesh.indices[i * 3 + 2] = i * 3 + 2;

            mesh.uv0s[i * 3 + 0] = a;
            mesh.uv0s[i * 3 + 1] = b;
            mesh.uv0s[i * 3 + 2] = c;

            time[i] = 1;
            speed[i] = 1;
        }

        mesh.Apply(indices: true, positions: true, uv0s: true);
    }

    public override void Refresh()
    {
        if (Frame == null || Frame.AtomPositions == null) return;

        var white = new Color32(255, 255, 255, 255);

        for (int i = 0; i < this.time.Count; ++i)
        {
            var time = this.time[i];
            
            var color = FasterMath.Mul(white, Mathf.PingPong(time * 2, 1));
            mesh.colors[i * 3 + 0] = color;
            mesh.colors[i * 3 + 1] = color;
            mesh.colors[i * 3 + 2] = color;
            
            time = Mathf.Clamp01(time + speed[i] * Time.deltaTime);

            if (time >= 1)
            {
                RandomiseTriangle(i);
            }
            else
            {
                this.time[i] = time;
            }
        }

        mesh.Apply(positions: true, colors: true);
    }
}