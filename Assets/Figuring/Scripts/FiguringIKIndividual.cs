﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using NSB.Utility;
using NSB.MMD.MeshGeneration;
using Narupa.VR.Player.Control;
using System.Linq;
using Valve.VR;

public class FiguringIKIndividual : InstanceView<byte>
{
    public VRIK vrik;
    public FiguringAppController controller;
    public Transform headTransform;
    public Transform leftHandTransform;
    public Transform rightHandTransform;

    public Transform modelTransform;
    public List<Transform> transforms;

    public SkinnedMeshRenderer mannequinRenderer;

    private MeshTool meshtool;

    protected List<IntVector3> triangles = new List<IntVector3>();
    protected List<float> time = new List<float>();
    protected List<float> speed = new List<float>();

    [SerializeField]
    private VrPlaybackRenderer playback;

    [Header("Smoke")]
    public ParticleSystem smokeTemplate;
    private IndexedPool<ParticleSystem> smokePool;

    [Header("Skeleton & Shadow")]
    public Material skeletonMaterial;
    public float cycleSpeed;
    public LineRenderer lineTemplate;
    private IndexedPool<LineRenderer> linesPool;
    public List<TransformPairs> bones = new List<TransformPairs>();

    [Header("Stars")]
    [SerializeField]
    private ParticleSystemRenderer leftStarRenderer;
    [SerializeField]
    private ParticleSystemRenderer rightStarRenderer;
    [SerializeField]
    private Transform bodyStarTransform;

    public LineRenderer shadowTemplate;
    private IndexedPool<LineRenderer> shadowPool;

    private float leftHandValue, leftHandVelocity;
    private float rightHandValue, rightHandVelocity;

    public struct TransformPairs
    {
        public Transform a, b;
    }

    public override void Setup()
    {
        leftStarRenderer.sharedMaterial = new Material(leftStarRenderer.sharedMaterial);
        rightStarRenderer.sharedMaterial = new Material(rightStarRenderer.sharedMaterial);
        leftStarRenderer.trailMaterial = new Material(leftStarRenderer.trailMaterial);
        rightStarRenderer.trailMaterial = new Material(rightStarRenderer.trailMaterial);

        linesPool = new IndexedPool<LineRenderer>(lineTemplate);
        shadowPool = new IndexedPool<LineRenderer>(shadowTemplate);
        smokePool = new IndexedPool<ParticleSystem>(smokeTemplate);

        transforms.AddRange(modelTransform.GetComponentsInChildren<Transform>());

        meshtool = new MeshTool(topology: MeshTopology.Triangles);
        meshtool.SetVertexCount(transforms.Count);

        GetComponent<MeshFilter>().sharedMesh = meshtool.mesh;

        Resize(32);

        foreach (var transform in transforms)
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                var child = transform.GetChild(i);

                if (child.gameObject.activeSelf)
                {
                    bones.Add(new TransformPairs { a = transform, b = child });
                }
            }
        }

        transforms.Add(leftStarRenderer.transform);
        transforms.Add(rightStarRenderer.transform);
    }

    public void Resize(int triangleCount)
    {
        triangles.Resize(triangleCount);
        time.Resize(triangleCount);
        speed.Resize(triangleCount);
        meshtool.SetVertexCount(triangleCount * 3);
        meshtool.SetIndexCount(triangleCount * 3);

        var a = new Vector2(0, 0);
        var b = new Vector2(1, 0);
        var c = new Vector2(.5f, 1);

        for (int i = 0; i < triangleCount; ++i)
        {
            meshtool.indices[i * 3 + 0] = i * 3 + 0;
            meshtool.indices[i * 3 + 1] = i * 3 + 1;
            meshtool.indices[i * 3 + 2] = i * 3 + 2;

            meshtool.uv0s[i * 3 + 0] = a;
            meshtool.uv0s[i * 3 + 1] = b;
            meshtool.uv0s[i * 3 + 2] = c;

            time[i] = 1;
            speed[i] = 1;
        }

        meshtool.Apply(indices: true, positions: true, uv0s: true);
    }

    private void Update()
    {
        mannequinRenderer.enabled = false;//controller.Config.MannequinColor.Val.value >= 0.1f;

        UpdateSmoke();
        UpdateStars();
    }

    private void UpdateStars()
    {
        var interactions = playback.NextFrame.InteractionForceInfo;
        float strength = controller.Config.HandStarStrength.value;

        bool leftInteract = interactions.Any(i => i.PlayerID == config 
                                               && i.InputID == ((byte) SteamVR_Input_Sources.LeftHand));
        bool rightInteract = interactions.Any(i => i.PlayerID == config
                                                && i.InputID == ((byte) SteamVR_Input_Sources.RightHand));

        var min = controller.Config.HandStarMinimum.value;
        leftHandValue = Mathf.SmoothDamp(leftHandValue, leftInteract ? 1 : min, ref leftHandVelocity, strength);
        rightHandValue = Mathf.SmoothDamp(rightHandValue, rightInteract ? 1 : min, ref rightHandVelocity, strength);

        var leftColor = controller.Config.HandColor.value * leftHandValue;
        var rightColor = controller.Config.HandColor.value * rightHandValue;

        leftStarRenderer.sharedMaterial.SetColor("_TintColor", leftColor);
        rightStarRenderer.sharedMaterial.SetColor("_TintColor", rightColor);
        leftStarRenderer.trailMaterial.SetColor("_TintColor", leftColor);
        rightStarRenderer.trailMaterial.SetColor("_TintColor", rightColor);

        leftStarRenderer.transform.localScale = Vector3.one * controller.Config.HandSize.value * .05f;
        rightStarRenderer.transform.localScale = Vector3.one * controller.Config.HandSize.value * .05f;
        bodyStarTransform.localScale = Vector3.one * controller.Config.BodyStarSize.value * 0.05f;
    }

    private void UpdateSmoke()
    {
        if (controller.Config.BodySmokeColor.Val.value <= 0.0001f)
        {
            smokePool.SetActive(0);
            return;
        }

        smokePool.SetActive(bones.Count);

        float scale = 1 / transform.lossyScale.z;

        float lifetime = controller.Config.BodySmokeLifetime.value;
        float distribution = controller.Config.BodySmokeDistribution.value;
        float density = controller.Config.BodySmokeDensity.value;

        for (int i = 0; i < bones.Count; ++i)
        {
            var bone = bones[i];
            var part = smokePool[i];

            var pos = Vector3.LerpUnclamped(bone.a.position, bone.b.position, 0.5f);
            var rot = Quaternion.LookRotation(bone.a.position - bone.b.position);
            var len = (bone.a.position - bone.b.position).magnitude * scale * 0.5f * 0.7f;

            part.transform.position = pos;
            part.transform.rotation = rot;

            var main = part.main;
            main.startLifetime = lifetime;
            main.startSize = distribution;

            var emission = part.emission;
            emission.rateOverTimeMultiplier = density;

            var shape = part.shape;
            shape.radius = len;
        }
    }
}
