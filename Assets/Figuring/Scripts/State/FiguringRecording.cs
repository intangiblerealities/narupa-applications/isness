﻿using Nano.Client;
using Nano.Transport.Variables;
using NSB.Processing;
using NSB.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class CustomEncoding
{
    public static void Register()
    {
        LitJson.JsonMapper.RegisterExporter<Vector3>(ExportVector3);
        LitJson.JsonMapper.RegisterExporter<Quaternion>(ExportQuaternion);
        LitJson.JsonMapper.RegisterExporter<BondPair>(ExportBondPair);
    }

    public static void ExportVector3(Vector3 vector, LitJson.JsonWriter writer)
    {
        writer.WriteArrayStart();
        writer.Write(Math.Round(vector.x, 2));
        writer.Write(Math.Round(vector.y, 2));
        writer.Write(Math.Round(vector.z, 2));
        writer.WriteArrayEnd();
    }

    public static void ExportQuaternion(Quaternion quaternion, LitJson.JsonWriter writer)
    {
        writer.WriteArrayStart();
        writer.Write(quaternion.x);
        writer.Write(quaternion.y);
        writer.Write(quaternion.z);
        writer.Write(quaternion.w);
        writer.WriteArrayEnd();
    }

    public static void ExportBondPair(BondPair pair, LitJson.JsonWriter writer)
    {
        writer.WriteArrayStart();
        writer.Write(pair.A);
        writer.Write(pair.B);
        writer.WriteArrayEnd();
    }
}

public class FiguringRecording
{
    public class Pose
    {
        public Vector3 Position;
        public Quaternion Rotation;

        public static Pose FromVRInteraction(VRInteraction interaction)
        {
            var position = interaction.Interaction.Position;
            var rotation = interaction.Quaternion;

            return new Pose
            {
                Position = new Vector3(position.X, position.Y, position.Z),
                Rotation = new Quaternion(rotation.X, rotation.Y, rotation.Z, rotation.W),
            };  
        }
    }

    public class PlayerFrame
    {
        public byte PlayerID;
        public Pose HeadPose;
        public Pose LeftHandPose;
        public Pose RightHandPose;
    }

    public class ParticlesFrame
    {
        public List<Vector3> AtomPositions = new List<Vector3>();
        public List<BondPair> BondPairs = new List<BondPair>();
        public List<PlayerFrame> Players = new List<PlayerFrame>();

        private static Dictionary<byte, PlayerFrame> playerCache
            = new Dictionary<byte, PlayerFrame>();

        public void CopyFromNSBFrame(NSBFrame frame)
        {
            AtomPositions.AddRange(frame.AtomPositions.Take(frame.AtomCount));
            //BondPairs.AddRange(frame.BondPairs.Take(frame.BondCount));

            CopyVRInteractions(frame);
        }

        private void CopyVRInteractions(NSBFrame frame)
        {
            playerCache.Clear();

            if (frame.VRInteractions != null)
            {
                foreach (var interaction in frame.VRInteractions)
                {
                    PlayerFrame playerFrame;

                    byte playerID = interaction.Interaction.PlayerID;
                    var deviceType = (VRDevice) interaction.DeviceType;

                    if (!playerCache.TryGetValue(playerID, out playerFrame))
                    {
                        playerFrame = new PlayerFrame();
                        playerFrame.PlayerID = playerID;
                        playerCache[playerID] = playerFrame;
                    }

                    if (deviceType == VRDevice.Headset)
                        playerFrame.HeadPose = Pose.FromVRInteraction(interaction);
                    if (deviceType == VRDevice.LeftController)
                        playerFrame.LeftHandPose = Pose.FromVRInteraction(interaction);
                    if (deviceType == VRDevice.RightController)
                        playerFrame.RightHandPose = Pose.FromVRInteraction(interaction);
                }
            }

            Players.AddRange(playerCache.Values);
            playerCache.Clear();
        }
    }

    public DateTime startTime, endTime;
    public int frameCount;
    public Dictionary<string, List<float>> variableHistory
        = new Dictionary<string, List<float>>();
    public List<ParticlesFrame> particleHistory = new List<ParticlesFrame>();

    private void SetFrameCount(int frameCount)
    {
        this.frameCount = frameCount;

        foreach (var history in variableHistory.Values)
        {
            history.Resize(frameCount);
        }

        particleHistory.Resize(frameCount);
    }

    private void SetVariableValueAtFrame(int frame, string variable, float value)
    {
        SetFrameCount(Mathf.Max(frame, frameCount));

        List<float> history;

        if (!variableHistory.TryGetValue(variable, out history))
        {
            history = new List<float>();
            history.Resize(frameCount);
            variableHistory.Add(variable, history);
        }

        history[frame] = value;
    }

    private void SetParticlesAtFrame(int frame, ParticlesFrame particles)
    {
        particleHistory[frame] = particles;
        Debug.Log($"Add Particles: {particles}");
    }

    private float GetVariableValueAtFrame(int frame, string variable)
    {
        List<float> history;

        if (frame < frameCount
         && variableHistory.TryGetValue(variable, out history))
        {
            return history[frame];
        }

        return 0;
    }

    public void SetFrameVariablesFromConfig(int frame, Config config)
    {
        foreach (var variable in config.variables)
        {
            SetVariableValueAtFrame(frame, variable.name, variable.value);
        }
    }

    public void SetFrameParticlesFromNSBFrame(int frame, NSBFrame nsbFrame)
    {
        var particles = new ParticlesFrame();
        particles.CopyFromNSBFrame(nsbFrame);
        SetParticlesAtFrame(frame, particles);
    }

    public void AddFrameFromConfigAndFrame(Config config, NSBFrame frame)
    {
        SetFrameCount(frameCount + 1);
        SetFrameVariablesFromConfig(frameCount - 1, config);
        SetFrameParticlesFromNSBFrame(frameCount - 1, frame);
    }

    public void GetFrameVariablesToConfig(int frame, Config config)
    {
        foreach (var variable in config.variables)
        {
            variable.Set(GetVariableValueAtFrame(frame, variable.name));
        }
    }

    public void Start()
    {
        startTime = DateTime.Now;
    }

    public void Stop()
    {
        endTime = DateTime.Now;
    }

    public void Reset()
    {
        variableHistory.Clear();
        SetFrameCount(0);
    }
}
