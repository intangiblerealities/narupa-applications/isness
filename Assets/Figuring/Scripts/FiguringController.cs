﻿// Copyright 2018 Mark Wonnacott, Figuring Project

using UnityEngine;
using Nano.Transport.Variables.Interaction;
using Narupa.VR.Interaction;
using Valve.VR;

public class FiguringController : MonoBehaviour
{
    [SerializeField]
    private FiguringAppController appController;
    [SerializeField]
    private LayerInteractionController layer;
    [SerializeField]
    private SteamVR_Action_Boolean interactAction;
    [SerializeField]
    private SteamVR_Input_Sources actionSource;

    public float GradientScaleFactor = 1;

    public bool Interacting => interactAction.GetState(actionSource);

    private void Update()
    {
        byte deviceID = (byte) actionSource;

        Debug.Log(Interacting);

        if (Interacting)
        {
            var interaction = new Interaction
            {
                GradientScaleFactor = GradientScaleFactor,
                InteractionType = (byte) InteractionType.NearestAtomUpdate,
                PlayerID = FiguringAppController.PlayerID,
                InputID = deviceID,
            };

            layer.AddLegacyInteraction(transform, interaction);
        }
        else
        {
            layer.DeleteInteraction(deviceID);
        }
    }
}
