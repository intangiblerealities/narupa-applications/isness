﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Narupa.Math
{
    /// <summary>
    /// Bundles position, rotation, and scale of a transformation.
    /// </summary>
    public struct Transformation
    {
        /// <summary>
        /// Construct a transformation from the translation, rotation, and 
        /// scale of a TRS matrix.
        /// </summary>
        public static Transformation FromMatrix(Matrix4x4 matrix)
        {
            return new Transformation(matrix.GetTranslation(),
                                      matrix.GetRotation(),
                                      matrix.GetScale());
        }

        /// <summary>
        /// Construct a transformation from the translation, rotation, and 
        /// scale of a Unity Transform.
        /// </summary>
        public static Transformation FromTransform(Transform transform)
        {
            return new Transformation(transform.position,
                                      transform.rotation,
                                      transform.lossyScale);
        }

        public static Transformation Identity
            => new Transformation(Vector3.zero, Quaternion.identity, Vector3.one);

        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;

        public Matrix4x4 Matrix => Matrix4x4.TRS(Position, Rotation, Scale);

        public Transformation(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }

        /// <summary>
        /// Set the transform's position, rotation, scale from this transformation.
        /// </summary>
        public void CopyToTransform(Transform transform)
        {
            Matrix.CopyTrsToTransform(transform);
        }
    }
}
