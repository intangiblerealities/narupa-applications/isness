﻿using UnityEngine;

namespace Narupa.Utility.Unity
{
    public static class MatrixExtensions
    {
        /// <summary>
        /// Extract the translation component of the given TRS matrix. This is
        /// the worldspace origin of matrix's coordinate space.
        /// </summary>
        public static Vector3 GetTranslation(this Matrix4x4 matrix)
        {
            Vector3 translate;
            translate.x = matrix.m03;
            translate.y = matrix.m13;
            translate.z = matrix.m23;
            return translate;
        }

        /// <summary>
        /// Extract the rotation component of the given TRS matrix. This is
        /// the quaternion that rotates worldspace forward, up, right vectors
        /// into localspace forward, up, right.
        /// </summary>
        public static Quaternion GetRotation(this Matrix4x4 matrix)
        {
            Vector3 forward;
            forward.x = matrix.m02;
            forward.y = matrix.m12;
            forward.z = matrix.m22;

            Vector3 upwards;
            upwards.x = matrix.m01;
            upwards.y = matrix.m11;
            upwards.z = matrix.m21;

            return Quaternion.LookRotation(forward, upwards);
        }

        /// <summary>
        /// Extract the scale component of the given TRS matrix. 
        /// </summary>
        public static Vector3 GetScale(this Matrix4x4 matrix)
        {
            Vector3 scale;
            scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
            scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
            scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;
            return scale;
        }

        /// <summary>
        /// Set this transform's position, rotation, scale from a given matrix. 
        /// Does not work with non-uniform scaling.
        /// </summary>
        public static void SetTRSFromMatrix(this Transform transform, Matrix4x4 matrix)
        {
            transform.position = matrix.GetTranslation();
            transform.rotation = matrix.GetRotation();
            transform.localScale = matrix.GetScale();
        }

        /// <summary>
        /// Get the matrix that transforms from one matrix to another.
        /// </summary>
        public static Matrix4x4 GetRelativeTransformMatrix(Matrix4x4 from, Matrix4x4 to)
        {
            return from.inverse * to;
        }
    }
}
