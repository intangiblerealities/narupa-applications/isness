﻿// Copyright (c) 2019 Mark Wonnacott. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

/// <summary>
/// A coordinate space that represents a shared physical space. Multiple XR clients
/// sharing a physical space will disagree about the Unity worldspace positions of 
/// physical locations, but will agree on the CalibratedSpace positions.
/// 
/// It is assumed that clients will agree on worldspace up, and worldspace scaling.
/// </summary>
public class PhysicallyCalibratedSpace
{
    private Matrix4x4 localToWorldMatrix;
    private Matrix4x4 worldToLocalMatrix;

    /// <summary>
    /// Calibrate the space using two known points. Situates the origin at A, points
    /// forward (+z) toward B, and leave worldspace up (+y) unaltered.
    /// </summary>
    public void CalibrateWithTwoPoints(Vector3 calibrationPointA,
                                       Vector3 calibrationPointB)
    {
        localToWorldMatrix = Matrix4x4.LookAt(from: calibrationPointA,
                                              to: calibrationPointB,
                                              up: Vector3.up);
        worldToLocalMatrix = localToWorldMatrix.inverse;
    }

    /// <summary>
    /// Orient a Unity Transform so that it's localspace matches this space.
    /// </summary>
    public void CopyToUnityTransform(Transform transform)
    {
        transform.position = TransformPointLocalToWorld(Vector3.zero);
        transform.rotation = TransformRotationLocalToWorld(Quaternion.identity);
        transform.localScale = Vector3.one;
    }

    /// <summary>
    /// Transform a point from this space into Unity worldspace.
    /// </summary>
    public Vector3 TransformPointLocalToWorld(Vector3 point)
    {
        return localToWorldMatrix.MultiplyPoint3x4(point);
    }

    /// <summary>
    /// Transform a point from this Unity worldspace into this space.
    /// </summary>
    public Vector3 TransformPointWorldToLocal(Vector3 point)
    {
        return worldToLocalMatrix.MultiplyPoint3x4(point);
    }

    /// <summary>
    /// Transform a rotation from this space into Unity worldspace.
    /// </summary>
    public Quaternion TransformRotationLocalToWorld(Quaternion rotation)
    {
        return localToWorldMatrix.rotation * rotation;
    }

    /// <summary>
    /// Transform a rotation from Unity worldspace into this space.
    /// </summary>
    public Quaternion TransformRotationWorldToLocal(Quaternion rotation)
    {
        return worldToLocalMatrix.rotation * rotation;
    }

    public Matrix4x4 TransformMatrixLocalToWorld(Matrix4x4 matrix)
    {
        return localToWorldMatrix * matrix;
    }

    public Matrix4x4 TransformMatrixWorldToLocal(Matrix4x4 matrix)
    {
        return worldToLocalMatrix * matrix;
    }
}
