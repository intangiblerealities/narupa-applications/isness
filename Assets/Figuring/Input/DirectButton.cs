﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.Frontend.Input
{
    /// <summary>
    /// An IButton that is pressed and released directly via code.
    /// </summary>
    public class DirectButton : IButton
    {
        public bool IsPressed { get; private set; }
        public event Action Pressed;
        public event Action Released;

        /// <summary>
        /// Put this button into the pressed state if it is not already 
        /// pressed.
        /// </summary>
        public void Press()
        {
            if (!IsPressed)
            {
                IsPressed = true;
                Pressed?.Invoke();
            }
        }

        /// <summary>
        /// Put this button into the released state if it is not already
        /// released.
        /// </summary>
        public void Release()
        {
            if (IsPressed)
            {
                IsPressed = false;
                Released?.Invoke();
            }
        }

        /// <summary>
        /// Toggle this button between the pressed and released states.
        /// </summary>
        public void Toggle()
        {
            if (IsPressed)
            {
                Release();
            }
            else
            {
                Press();
            }
        }
    }
}
