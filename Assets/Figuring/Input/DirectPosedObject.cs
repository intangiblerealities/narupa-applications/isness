﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using System;

using Narupa.Math;

namespace Narupa.Frontend.Input
{
    /// <summary>
    /// An IPosedObject whose pose is set directly from code.
    /// </summary>
    public class DirectPosedObject : IPosedObject
    {
        public Transformation? Pose { get; private set; }
        public event Action PoseChanged;

        /// <summary>
        /// Set the pose of this object. This invokes PoseChanged.
        /// </summary>
        public void SetPose(Transformation? pose)
        {
            Pose = pose;
            PoseChanged?.Invoke();
        }
    }
}
