﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.Frontend.Input
{ 
    /// <summary>
    /// Represents a button input that can be pressed, held, then released. Allows
    /// polling of the current state and listening to press and release events.
    /// </summary>
    public interface IButton
    {
        bool IsPressed { get; }
        event Action Pressed;
        event Action Released;
    }
}
