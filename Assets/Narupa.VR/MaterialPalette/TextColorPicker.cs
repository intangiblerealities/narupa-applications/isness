﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using TMPro;
using UnityEngine;

namespace Narupa.VR.MaterialPalette
{
	public class TextColorPicker : MaterialPaletteUser
	{
		[SerializeField] private bool useTextColor = true;

		[SerializeField] private bool useSecondaryColor = false;

		private TextMeshProUGUI text;

		protected override void Awake()
		{
			base.Awake();
			RefreshReferences();
		}

		private void RefreshReferences()
		{
			if(this.text == null)
				this.text = GetComponent<TextMeshProUGUI>();
		}

		public override void UpdateColors()
		{
			RefreshReferences();
			var palette = GetPalette();
			if (useTextColor)
				text.color = useSecondaryColor ? palette.SecondaryTextColor : palette.PrimaryTextColor;
			else
				text.color = useSecondaryColor ? palette.SecondaryColor : palette.PrimaryColor;

		}
	}
}
