﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;

namespace Narupa.VR.MaterialPalette
{
	public class GlobalMaterialPalette : MonoBehaviour
	{

		public MaterialPalette Palette;
		
		/// <summary>
		///     Indicates that the palette has been changed.
		/// </summary>
		public event EventHandler PaletteChanged;

		private void OnValidate()
		{
			if (PaletteChanged != null) PaletteChanged(this, null);

			var paletteUsers = FindObjectsOfType<MaterialPaletteUser>();
			foreach (var user in paletteUsers) user.UpdateColors();
		}
		
	}
}
