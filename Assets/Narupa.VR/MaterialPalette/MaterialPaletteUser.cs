﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.MaterialPalette
{
    /// <summary>
    ///     Base class for a material palette user.
    /// </summary>
    /// <remarks>
    ///     Any elements that use the <see cref="MaterialPalette" /> should implement this class, enabling updates to
    ///     colours from within the editor.
    /// </remarks>
    public abstract class MaterialPaletteUser : MonoBehaviour
    {
        private GlobalMaterialPalette materialPalette;

        protected virtual void Awake()
        {
            materialPalette = FindObjectOfType<GlobalMaterialPalette>();
        }
        
        /// <summary>
        /// Update the colors of this instance.
        /// </summary>
        public abstract void UpdateColors();
        
        public MaterialPalette GetPalette()
        {
            
            if (materialPalette == null)
                materialPalette = FindObjectOfType<GlobalMaterialPalette>();
            if (materialPalette == null) return null;
            return materialPalette.Palette;
        }
        
        private void Reset()
        {
            UpdateColors();
        }
       
        #if UNITY_EDITOR
        private void OnValidate()
        {
            UpdateColors();
        }
        #endif
    }
}