﻿using Narupa.Utility.Unity;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;

namespace Narupa.VR.Multiplayer
{
    /// <summary>
    /// Class for calibrating multiplayer Room-Scale SteamVR.
    /// </summary>
    public class MultiplayerSteamVrCalibration : MonoBehaviour
    {
        public readonly static PhysicallyCalibratedSpace PhysicalSpace = new PhysicallyCalibratedSpace();

        public static Matrix4x4 MultiplayerTRS { get; private set; } = Matrix4x4.Translate(Vector3.zero);

        public static Vector3 MultiplayerScale = Vector3.one;

        /// <summary>
        /// Transform of narupa root object.
        /// </summary>
        public Transform NarupaTransform;

        private bool calibrated;

        public static void SetBoxRelativeTRS(Matrix4x4 trsMatrix)
        {
            MultiplayerTRS = trsMatrix;
        }

        public void SetBoxWorldTRS(Matrix4x4 trsMatrix)
        {
            MultiplayerTRS = PhysicalSpace.TransformMatrixWorldToLocal(trsMatrix);
        }

        private List<XRNodeState> nodeStates = new List<XRNodeState>();

        /// <summary>
        /// Calibrate the PhysicalSpace with the two TrackingReference devices
        /// with the lowest uniqueIDs.
        /// </summary>
        private void CalibrateFromLighthouses()
        {
            InputTracking.GetNodeStates(nodeStates);

            var trackers = nodeStates.Where(s => s.nodeType == XRNode.TrackingReference)
                                     .OrderBy(s => s.uniqueID)
                                     .ToList();

            Vector3 trackerPosition0, trackerPosition1;

            if (trackers.Count >= 2
             && trackers[0].TryGetPosition(out trackerPosition0)
             && trackers[1].TryGetPosition(out trackerPosition1))
            {
                PhysicalSpace.CalibrateWithTwoPoints(trackerPosition0, trackerPosition1);
                calibrated = true;
            }
            else
            {
                Debug.LogWarning($"Couldn't calibrate from lighthouses: need two valid tracking positions.");
            }
        }

        /// <summary>
        /// Reposition the simulation box so it's forced to match the stored
        /// transform parameters.
        /// </summary>
        private void RepositionSimulationBox()
        {
            if (calibrated)
            {
                var trs = PhysicalSpace.TransformMatrixLocalToWorld(MultiplayerTRS);
                NarupaTransform.SetTRSFromMatrix(trs);
            }
        }

        private void Update()
        {
            CalibrateFromLighthouses();
            RepositionSimulationBox();
        }
    }
}
