﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Nano.Transport.Variables;
using Narupa.Client.Network;
using Narupa.VR.Player.Control;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Multiplayer
{
    /// <summary>
    /// Multiplayer SteamVR renderer.
    /// </summary>
    public class SteamVrObjectRenderer : MonoBehaviour
    {

        private readonly List<VRInteraction> controllerPositions = new List<VRInteraction>();
        private IndexedPool<Transform> controllers;

        [SerializeField]
        public Transform ControllerTemplate;

        private readonly List<VRInteraction> headsetPositions = new List<VRInteraction>();

        public IndexedPool<Transform> headsets;

        [SerializeField]
        public Transform HeadsetTemplate;

        private NetworkManager networkManager;
        private WorldLocalSpaceTransformer vrTransform;
        private float lerpSpeed = 2f;

        // Use this for initialization
        private void Start()
        {
            headsets = new IndexedPool<Transform>(HeadsetTemplate.transform, transform);
            controllers = new IndexedPool<Transform>(ControllerTemplate, transform);
            networkManager = FindObjectOfType<NetworkManager>();
            vrTransform = FindObjectOfType<WorldLocalSpaceTransformer>();
        }

        // Update is called once per frame
        private void Update()
        {
            networkManager.CurrentConnection?.UpdateVRPositions(headsetPositions, controllerPositions);
            
            headsets.SetActive(headsetPositions.Count);
            for (var i = 0; i < headsetPositions.Count; i++)
            {
                bool localHeadset = (headsetPositions[i].Interaction.PlayerID == FiguringAppController.PlayerID);

                headsets[i].gameObject.SetActive(!localHeadset);

                SetVrPosition(headsets[i], headsetPositions[i]);
            }

            controllers.SetActive(controllerPositions.Count);
            for (var i = 0; i < controllerPositions.Count; i++)
            {
                SetVrPosition(controllers[i], controllerPositions[i]);
            }
            
        }

        // Positions an MultiplayerObjectController according to a VRInteraction
        private void SetVrPosition(Transform obj, VRInteraction interaction)
        {
            var position = interaction.Interaction.Position;
            var rotation = interaction.Quaternion;
            var newPosition = vrTransform.TransformNarupaSpacePointToWorldSpace(new Vector3(position.X, position.Y, position.Z));
                    
            obj.transform.position =
                Vector3.Lerp(obj.transform.position, newPosition, lerpSpeed * Time.time);
                        
            var newRotation = vrTransform.TransformNarupaSpaceQuaternionToWorldSpace(new Quaternion(rotation.X, rotation.Y, rotation.Z,
                rotation.W));
            obj.transform.rotation =
                Quaternion.Lerp(obj.transform.rotation, newRotation, lerpSpeed * Time.time);
        }

    }
}