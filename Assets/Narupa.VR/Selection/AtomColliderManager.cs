﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using NSB.Examples.Player.Control;
using NSB.Interaction;
using NSB.MMD.Styles;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Selection
{
    /// <summary>
    ///     Class for spawning colliders for atoms.
    /// </summary>
    [Obsolete("No longer used due to poor performance, ")]
    public class AtomColliderManager : FrameFilter
    {
        [SerializeField] private Transform colliderParent;

        [SerializeField] private AtomColliderInfo colliderPrefab;

        private IndexedPool<AtomColliderInfo> colliders;

        [SerializeField] private float colliderScale;

        protected override void Awake()
        {
            base.Awake();
            colliders = new IndexedPool<AtomColliderInfo>(colliderPrefab, colliderParent);
        }

        private void Update()
        {
            if (AutoRefresh)
                RefreshColliders();
        }

        public void RefreshColliders()
        {
            UpdateFilter(frameSource.Component.Frame);
        }

        public void UpdateFilter(NSBFrame frame)
        {
            var scaling = Vector3.one * colliderScale;

            // TODO: this might be better replaced with ParticleBatches.UpdateBatches
            colliders.SetActive(frame.AtomCount);
            colliders.MapActive((i, collider) =>
            {
                int element = frame.AtomTypes[i];

                collider.transform.localPosition = frame.AtomPositions[i];
                collider.transform.localScale = scaling * ExampleMolecularStyle.ElementRadiuses[element];
                collider.AtomID = i;
            });
        }
    }
}