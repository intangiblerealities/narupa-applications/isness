﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Player.Control;
using Narupa.VR.Selection.Event;
using Narupa.VR.UI;
using UnityEngine;
using Valve.VR;

namespace Narupa.VR.Selection
{
    /// <summary>
    ///     Handles the selection of atoms via the <see cref="VrAtomSelectionPointer" />
    /// </summary>
    internal class VrAtomSelectionManager : MonoBehaviour
    {
        /// <summary>
        ///     Integer that controls how many layers up from the atom the touch interaction will trigger.
        /// </summary>
        public static int GroupSelection = 0;

        /// <summary>
        ///     Indicates whether the user is currently in selection mode or not.
        /// </summary>
        public static bool InSelectionMode;

        private uint controllerId;
        private VrAtomSelectionPointer controllerTouch;

        [SerializeField] private SteamVR_Action_Boolean actionTrigger;
        [SerializeField] private SteamVR_Action_Vibration actionHaptic;
        [SerializeField] private SteamVR_Input_Sources inputSource;
        private bool isTouching;
        private SelectionManager selectionManager;

        public delegate void SelectionModeChangedHandler(bool isSelectionMode);

        public event SelectionModeChangedHandler SelectionModeChanged;

        /// <summary>
        ///     Switches the user mode to Selection Mode, or Interactive Mode.
        /// </summary>
        /// <param name="switchToSelectionMode"></param>
        internal void SwitchToSelectionMode(bool switchToSelectionMode)
        {
            if (controllerTouch == null) Awake();

            if (switchToSelectionMode)
            {
                controllerTouch.TouchedAtom += OnControllerTouchInteractableObject;
                controllerTouch.UntouchedAtom += OnControllerUnTouchInteractableObject;
                //actionTrigger.AddOnChangeListener((_, __, ___) => OnTriggerChange(), this.inputSource);
                InSelectionMode = true;
            }
            else
            {
                controllerTouch.TouchedAtom -= OnControllerTouchInteractableObject;
                controllerTouch.UntouchedAtom -= OnControllerUnTouchInteractableObject;
                //actionTrigger.RemoveOnChangeListener(OnTriggerChange, this.inputSource);
                InSelectionMode = false;
            }

            SelectionModeChanged?.Invoke(switchToSelectionMode);
        }

        public void SetGroupSelectionMode(int mode)
        {
            GroupSelection = mode;
        }

        private void Awake()
        {
            controllerTouch = GetComponentInChildren<VrAtomSelectionPointer>();
        }

        private void OnControllerTouchInteractableObject(AtomColliderEventInfo atomInfo)
        {
            if (atomInfo == null) return;
            if (selectionManager == null) selectionManager = FindObjectOfType<SelectionManager>();
            selectionManager.ActiveSelection.OnTouchAtom(atomInfo, GroupSelection);
            isTouching = true;
            OnAtomHoverStart();
        }

        private void OnControllerUnTouchInteractableObject(AtomColliderEventInfo atomInfo)
        {
            isTouching = false;
            if (atomInfo == null) return;
            selectionManager.ActiveSelection.OnUntouchAtom(atomInfo);
            OnAtomHoverEnd();
        }

        public EventHandler<AtomHoverStartedEventArgs> AtomHoverStarted;
        
        public EventHandler<AtomHoverEndedEventArgs> AtomHoverEnded;

        
        private void OnAtomHoverStart()
        {
            AtomHoverStarted?.Invoke(this, new AtomHoverStartedEventArgs());
        }

        private void OnAtomHoverEnd()
        {
            AtomHoverEnded?.Invoke(this, new AtomHoverEndedEventArgs());
        }

        private void OnTriggerChange()
        {
            if (actionTrigger.GetStateDown(this.inputSource))
                OnTriggerPress();
        }

        private void OnTriggerPress()
        {
            controllerId = (uint) this.inputSource;
            if (isTouching)
            {
                selectionManager.ActiveSelection.ToggleHighlightedIntoSelection(controllerId);
                actionHaptic.Execute(0f, 1f, 1f, VrButtonBase.HapticFeedBackStrength, this.inputSource);
            }
        }

        private void Start()
        {
            selectionManager = FindObjectOfType<SelectionManager>();
        }

        private void Update()
        {
        }
    }
}