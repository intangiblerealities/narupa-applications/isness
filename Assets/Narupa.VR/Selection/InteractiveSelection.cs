﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using Nano.Transport.Variables.Interaction;
using Narupa.Client.Network;
using Narupa.VR.Player.Control;
using Narupa.VR.UI.Tooltips;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Simbox.Topology;
using UnityEngine;

namespace Narupa.VR.Selection
{
    /// <summary>
    ///     The properties that govern interacting with a given <see cref="InteractiveSelection" />.
    /// </summary>
    public class InteractionProperties
    {
        /// <summary>
        ///     How should interaction with this selection be handled.
        /// </summary>
        public InteractionType InteractionType = InteractionType.SingleAtom;

        /// <summary>
        ///     Indicates whether this layer is to be held in place with restraints.
        /// </summary>
        public bool RestraintLayer;
    }

    /// <summary>
    ///     Interface for making undoable actions.
    /// </summary>
    public interface IUndoable
    {
        /// <summary>
        ///     Whether this instance can perform another redo action.
        /// </summary>
        /// <returns></returns>
        bool CanRedo();

        /// <summary>
        ///     Whether this instance can perform another undo action.
        /// </summary>
        /// <returns></returns>
        bool CanUndo();

        /// <summary>
        ///     Get the name of the last action performed.
        /// </summary>
        /// <returns></returns>
        string GetLastActionName();

        /// <summary>
        ///     Get the name of the next action performed.
        /// </summary>
        /// <returns></returns>
        string GetNextActionName();

        /// <summary>
        ///     Redo the next action.
        /// </summary>
        void Redo();

        /// <summary>
        ///     Undo the previous action.
        /// </summary>
        void Undo();
    }

    internal class InteractiveSelectionInfo
    {
        public string Name;
        public string Renderer;
        public string Style;
        public InteractionProperties InteractionProperties;
        public List<int> SelectedAtoms = new List<int>();
        public bool IsVisible; 
        
        internal InteractiveSelectionInfo()
        {
            
        }
        
        internal InteractiveSelectionInfo(InteractiveSelection selection)
        {
            Name = selection.Name;
            Renderer = selection.RendererRoot.ActiveRenderer.name.ToLower();
            Style = selection.RendererRoot.Style.Name.ToLower();
            InteractionProperties = selection.InteractionProperties;
            SelectedAtoms.AddRange(selection.Selection);
            IsVisible = selection.RendererRoot.gameObject.activeInHierarchy;
        }
    }
    
    /// <summary>
    ///     Class representing a non-volatile selection, or layer.
    /// </summary>
    public partial class InteractiveSelection : MonoBehaviour, ISelectionSource
    {
        private AtomColliderEventInfo atomLastTouched;
        private readonly Dictionary<uint, HashSet<int>> atomsToHighlight = new Dictionary<uint, HashSet<int>>();
        private VrAtomSelectionPointer controllerTouch;
        internal string Name;

        /// <summary>
        ///     How this interactive layer will be interacted with.
        /// </summary>
        public InteractionProperties InteractionProperties = new InteractionProperties();

        private NetworkManager networkManager;
        private VrPlaybackRenderer playback;

        /// <summary>
        ///     The renderer associated with this selection.
        /// </summary>
        [SerializeField] public RendererManager RendererRoot;

        /// <summary>
        ///     The highlighter used for showing what will be selected.
        /// </summary>
        [HideInInspector] public SelectionHighlighter SelectionHighlighter;

        private uint toolTipId;
        private AtomToolTipManager tooltipSpawner;

        /// <summary>
        ///     The selection associated with this instance.
        /// </summary>
        public NSBSelection Selection { get; } = new NSBSelection();

        public event Action SelectedAtomsChanged;

        /// <summary>
        ///     What happens when one touches an atom while in selection mode.
        /// </summary>
        /// <param name="atomInfo"></param>
        /// <param name="selectParents"></param>
        public void OnTouchAtom(AtomColliderEventInfo atomInfo, int selectParents = 0)
        {
            if (atomInfo == null) return;
            var atomIndex = atomInfo.AtomId;

            HashSet<int> atoms;
            if (atomsToHighlight.ContainsKey(atomInfo.ColliderId))
            {
                atoms = atomsToHighlight[atomInfo.ColliderId];
                atoms.Clear();
            }
            else
            {
                atoms = new HashSet<int>();
                atomsToHighlight[atomInfo.ColliderId] = atoms;
            }

            var topology = networkManager.CurrentConnection?.Topology;
            
            if (topology != null)
                topology.UpdateVisibleAtomMap(playback.Frame.VisibleAtomMap);
            else
                return;

            atoms = topology.GetAtoms(atomIndex, selectParents, atoms);
            SelectionHighlighter.AddAtomsToHighlight(atoms);

            if (atomLastTouched == null || atomLastTouched != atomInfo)
            {
                var atom = topology.VisibleAtomMap[atomIndex];
                if (atom != null) toolTipId = tooltipSpawner.DisplayAtomTooltip(atom);
            }

            atomLastTouched = atomInfo;
        }

        /// <summary>
        ///     What happens when one deselects an atom in selection mode.
        /// </summary>
        /// <param name="atomInfo"></param>
        public void OnUntouchAtom(AtomColliderEventInfo atomInfo)
        {
            if (atomInfo == null) return;

            if (atomsToHighlight.ContainsKey(atomInfo.ColliderId))

            {
                SelectionHighlighter.RemoveAtomsFromHighlight(atomsToHighlight[atomInfo.ColliderId]);
                atomsToHighlight.Remove(atomInfo.ColliderId);
                //preserve tooltip until next atom is highlighted.
                //DestroyPreviousTooltip();
            }
        }

        /// <summary>
        ///     Toggles the currently highlighted atoms, distinguished by selectionID into the selection if they're not currently
        ///     in it, and out of it if they are.
        /// </summary>
        /// <param name="selectionId"> The ID associated with the selection, usually the device ID. </param>
        public void ToggleHighlightedIntoSelection(uint selectionId)
        {
            if (atomsToHighlight.ContainsKey(selectionId) == false)
                return;

            var highlightedAtoms = atomsToHighlight[selectionId];
            var selectionContainsAtoms = true;
            foreach (var atom in highlightedAtoms)
                if (Selection.Contains(atom) == false)
                {
                    selectionContainsAtoms = false;
                    break;
                }

            if (selectionContainsAtoms)
                RemoveHighlightedFromSelection(highlightedAtoms);
            else
                AddHighlightedToSelection(highlightedAtoms);
            SelectedAtomsChanged?.Invoke();
        }

        private void AddHighlightedToSelection(HashSet<int> highlightedAtoms, bool isUndo = false)
        {
            var atomsAdded = new HashSet<int>();
            foreach (var atom in highlightedAtoms)
            {
                if (Selection.Contains(atom) == false) atomsAdded.Add(atom);
                Selection.Add(atom);
            }

        }

        private void Awake()
        {
            tooltipSpawner = FindObjectOfType<AtomToolTipManager>();
            playback = FindObjectOfType<VrPlaybackRenderer>();
            networkManager = FindObjectOfType<NetworkManager>();
        }

        private void DestroyPreviousTooltip()
        {
            tooltipSpawner.HideTooltip(toolTipId);
        }

        private void RemoveHighlightedFromSelection(HashSet<int> highlightedAtoms, bool isUndo = false)
        {
            var atomsRemoved = new HashSet<int>();
            foreach (var atom in highlightedAtoms)
            {
                if (Selection.Contains(atom)) atomsRemoved.Add(atom);
                Selection.Remove(atom);
            }

        }
    }
}