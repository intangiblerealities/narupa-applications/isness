﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Transport.Variables.Interaction;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Renderers.Interaction
{
    /// <summary>
    /// Renders interaction forces.
    /// </summary>
    public class InteractionRenderer : InstanceView<InteractionForceInfo>
    {
        private Vector3 center;

        private Gradient defaultColorGradient;

        [SerializeField] private Gradient forceGradient;

        /// <summary>
        /// The magnitude of the force.
        /// </summary>
        public float ForceMagnitude = 1f;
        /// <summary>
        /// The frequency of sine wave oscillations.
        /// </summary>
        public float Frequency = 2f;

        /// <summary>
        /// The height of sine wave oscillations.
        /// </summary>
        public float Height = 0.2f;

        private float lastTime;
        private Gradient lerpKeys;

        private LineRenderer lineRenderer;
        private Vector3 lineVector;

        /// <summary>
        /// The maximum observed force magnitude.
        /// </summary>
        public float MaxForceMagnitude = 1000;

        [SerializeField] private float maxLineWidth = 0.1f;

        [SerializeField] private float minLineWidth = 0.01f;

        /// <summary>
        /// The number of trailrenderer sections to apply per nanometer.
        /// </summary>
        public float SectionsPerNm = 200f;
        /// <summary>
        /// Speed of sine wave oscillations.
        /// </summary>
        public float SpeedMultiplier = 5f;

        /// <summary>
        ///     This is a work around for instance pools, to reset the maxForce if it's been a sufficiently long time since the
        ///     last interaction.
        /// </summary>
        [SerializeField] private float timeBeforeReset = 0.5f;

        private WorldLocalSpaceTransformer worldSpaceTransformer;

        private void Start()
        {
            lineRenderer = gameObject.GetComponent<LineRenderer>();
            defaultColorGradient = lineRenderer.colorGradient;
            lineRenderer.useWorldSpace = true;
            worldSpaceTransformer = FindObjectOfType<WorldLocalSpaceTransformer>();
        }

        private void CheckForReset()
        {
            var time = Time.time;
            if (time - lastTime > timeBeforeReset) MaxForceMagnitude = 100000f;
            lastTime = time;
        }

        private void UpdateLineColors()
        {
            if (ForceMagnitude > MaxForceMagnitude) MaxForceMagnitude = 1.2f * ForceMagnitude;
            var gradient = lineRenderer.colorGradient;
            var colorKeys = gradient.colorKeys;
            for (var i = 0; i < defaultColorGradient.colorKeys.Length; i++)
            {
                var lerpValue = Mathf.Clamp01(ForceMagnitude / MaxForceMagnitude);
                colorKeys[i].color = Color.Lerp(defaultColorGradient.colorKeys[i].color,
                    forceGradient.colorKeys[i].color, lerpValue);
            }

            gradient.SetKeys(colorKeys, gradient.alphaKeys);
            lineRenderer.colorGradient = gradient;
        }

        private void UpdateLinePositions()
        {
            lineRenderer.positionCount = (int)Mathf.Clamp(lineVector.magnitude / SectionsPerNm, 2f, 50f);
            for (var i = 0; i < lineRenderer.positionCount; i++)
            {
                var posOnLine = center + (float)i / lineRenderer.positionCount * lineVector;
                var sineGoodness = Height * Mathf.Sin(Frequency * i + SpeedMultiplier * Time.time);
                posOnLine.y += sineGoodness;
                lineRenderer.SetPosition(i, posOnLine);
            }
        }

        private void Update()
        {
            //Hack to reset maximum force upon new interaction.
            CheckForReset();

            //Get the relevant information from the interaction.
            var atomCentre = worldSpaceTransformer.TransformNarupaSpacePointToWorldSpace(ToUnityVec3(config.SelectedAtomsCentre)) ;
            var cursorPosition = worldSpaceTransformer.TransformNarupaSpacePointToWorldSpace(ToUnityVec3(config.Position));
            center = atomCentre;
            lineVector = cursorPosition - atomCentre;
            
            
            ForceMagnitude = config.Force.LengthSquared;

            //Lerp the line width
            lineRenderer.widthMultiplier = Mathf.Max(minLineWidth,
                Mathf.Min(ForceMagnitude / MaxForceMagnitude * maxLineWidth, maxLineWidth));

            UpdateLineColors();

            UpdateLinePositions();
        }

        public static Vector3 ToUnityVec3(SlimMath.Vector3 v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }
    }
}