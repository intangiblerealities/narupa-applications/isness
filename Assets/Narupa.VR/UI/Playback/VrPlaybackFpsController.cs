﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Player.Control;
using Narupa.VR.UI.Radial;
using UnityEngine;

namespace Narupa.VR.UI.Playback
{
	public class VrPlaybackFpsController : MonoBehaviour {

		private RadialSlider radialSlider;

		private VrPlaybackRenderer playbackRenderer;

		// Use this for initialization
		void Start () {
			radialSlider = GetComponent<RadialSlider>();
			radialSlider.ValueChanged += RadialSliderOnValueChanged;
			playbackRenderer = FindObjectOfType<VrPlaybackRenderer>();
			radialSlider.SetValue(playbackRenderer.PlaybackFps);
		}

		private void RadialSliderOnValueChanged(object sender, RadialSlider.SliderValueChangeEventArgs e)
		{
			playbackRenderer.SetFPS(e.Value);
		}
	}
}
