﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.VR.UI.Selection
{
    public class SelectionSwitchManagerButton : VrButtonBase
    {
        private SelectionSwitchManager switchManager;

        protected void Start()
        {
            switchManager = GetComponentInParent<SelectionCanvasManager>().SelectionSwitchManager;
            if (switchManager == null)
                throw new NullReferenceException(
                    "SelectionSwitchManager could not be found in scene, this button will not work!");
        }

        protected override void OnClick(bool userActivated = true)
        {
            base.OnClick(userActivated);
            switchManager.gameObject.SetActive(true);
        }
    }
}