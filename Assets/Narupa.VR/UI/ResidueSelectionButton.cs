﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Player.Control;

namespace Narupa.VR.UI
{
    public class ResidueSelectionButton : VrToggleBase
    {
        private VrAppController player;
        private ResidueButtonManager residueButtonManager;
        private ResidueSelectionController residueSelectionController;
        public int ButtonId { get; set; }

        protected override void Awake()
        {
            base.Awake();
            ToggleChangeValue += OnToggleChangeValue;
            if (player == null) player = FindObjectOfType<VrAppController>();
            residueButtonManager = transform.parent.parent.parent.GetComponent<ResidueButtonManager>();
        }

        private void OnToggleChangeValue(object obj, EventArgs e = null)
        {
            var toggle = (bool) obj;
            //Trigger method in ResidueButtonManager
            residueButtonManager.ToggleResidue(ButtonId, toggle);
        }
    }
}