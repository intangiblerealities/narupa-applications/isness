﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using Valve.VR;

namespace Narupa.VR.UI.Radial
{
    /// <summary>
    ///     Manages input to a UiRadialMenu, sending messages such as hover and click
    /// </summary>
    [RequireComponent(typeof(UiRadialMenu))]
    public class UiRadialMenuController : MonoBehaviour
    {
      
        protected UiRadialMenu menu;
        protected float currentAngle; //Keep track of angle for when we click
        protected bool touchpadTouched;

        public SteamVR_Action_Vector2 ActionPosition;
        public SteamVR_Action_Boolean ActionTouch;
        public SteamVR_Action_Boolean ActionClick;
        public SteamVR_Input_Sources InputSource;

        protected virtual void Awake()
        {
            menu = GetComponent<UiRadialMenu>(); 
        }

        protected virtual void OnEnable()
        {
            ActionClick.AddOnChangeListener(OnTrackpadClickChange, InputSource);
            ActionTouch.AddOnChangeListener(OnTrackpadTouchChange, InputSource);
            ActionPosition.AddOnChangeListener(OnTrackpadAxisChange, InputSource);
        }
        
        protected virtual void OnDisable()
        {
            ActionClick.RemoveOnChangeListener(OnTrackpadClickChange, InputSource);
            ActionTouch.RemoveOnChangeListener(OnTrackpadTouchChange, InputSource);
            ActionPosition.RemoveOnChangeListener(OnTrackpadAxisChange, InputSource);
        }
    
        protected void OnTrackpadClickChange(SteamVR_Action_Boolean action,
                                             SteamVR_Input_Sources source,
                                             bool state) {
            if(ActionClick.GetStateDown(InputSource))
                OnTrackpadClickDown();
            else if(ActionClick.GetStateUp(InputSource))
                OnTrackpadClickUp();
        }
        
        protected void OnTrackpadTouchChange(SteamVR_Action_Boolean action,
                                    SteamVR_Input_Sources source,
                                    bool state) {
            if(ActionTouch.GetStateDown(InputSource))
                OnTrackpadTouchDown();
            else if(ActionTouch.GetStateUp(InputSource))
                OnTrackpadTouchUp();
        }

        protected virtual void OnTrackpadClickDown(object sender = null) 
        {
            menu.ClickDown(currentAngle);
        }

        protected virtual void OnTrackpadClickUp(object sender = null)
        {
            menu.ClickUp(currentAngle);
        }

        protected virtual void OnShowMenu(float initialAngle, object sender = null)
        {
            menu.ShowMenu();
            UpdateAngle(initialAngle); // Needed to register initial touch position before the touchpad axis actually changes
        }

        protected virtual void OnHideMenu(bool force, object sender = null)
        {
            menu.EndHover();
            menu.HideMenu(force);
        }

        protected virtual void UpdateAngle(float angle, object sender = null)
        {
            currentAngle = angle;
            menu.UpdateHover(currentAngle);
        }

        protected virtual void OnTrackpadTouchDown()
        {
            touchpadTouched = true;
            OnShowMenu(CalculateAngle());
        }

        protected virtual void OnTrackpadTouchUp()
        {
            touchpadTouched = false;
            menu.EndHover();
            OnHideMenu(false);
        }

        //Touchpad finger moved position
        protected virtual void OnTrackpadAxisChange(SteamVR_Action_Vector2 action,
                                    SteamVR_Input_Sources source,
                                    Vector2 state,
                                    Vector2 delta)
        {
            if (touchpadTouched && ActionPosition.GetAxis(InputSource).sqrMagnitude > 0.01f)
            {
                UpdateAngle(CalculateAngle());
            }
        }

        protected virtual float CalculateAngle()
        {
            var touchpadAxis = ActionPosition.GetAxis(InputSource);
            return 270 - Mathf.Atan2(-touchpadAxis.y, touchpadAxis.x) * Mathf.Rad2Deg;
        }
    }
}