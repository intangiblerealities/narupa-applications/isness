﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;

namespace Narupa.VR.UI.Radial
{
    /// <summary>
    /// Represents a radial UI slider on a VR controller.
    /// </summary>
    [RequireComponent(typeof(UiRadialMenu))]
    public class RadialSlider : MonoBehaviour
    {
        public class SliderValueChangeEventArgs : EventArgs
        {
            public float Value;
            public float Difference;
            public int Steps;

            public SliderValueChangeEventArgs(float value, float difference, int steps)
            {
                this.Value = value;
                this.Difference = difference;
                this.Steps = steps;
            }
        }

        public event EventHandler<SliderValueChangeEventArgs> ValueChanged;
        
        [SerializeField] private float currentValue;

        public float CurrentValue
        {
            get { return currentValue; }
        }
        public float Interval = 0.05f;
        public float MaximumValue = 5f;
        public float MinimumValue;
        private UiRadialMenu radialMenu;

        private void Awake()
        {
            radialMenu = GetComponent<UiRadialMenu>();
            radialMenu.HoverChange += RadialMenuOnHoverChange;
        }

        private void RadialMenuOnHoverChange(object sender, UiRadialMenu.RadialButtonChangeEventArgs args)
        {  
            int numberOfButtons = radialMenu.ButtonCount;
            var buttonStart = args.NewButtonIndex;
            var buttonEnd = args.OldButtonIndex;
            
            var steps = buttonEnd - buttonStart;
            //If we've skipped over more than half the buttons, assume we've actually gone anticlockwise.
            if (Mathf.Abs(steps) > numberOfButtons / 2)
            {
                if (buttonEnd > buttonStart)
                    steps = buttonStart - numberOfButtons + buttonEnd;
                else
                    steps = numberOfButtons - buttonStart + buttonEnd;
            }

            UpdateValue(steps);
        }


        private static float Clamp(float val, float min, float max)
        {
            if (val.CompareTo(min) < 0) return min;
            if (val.CompareTo(max) > 0) return max;
            return val;
        }

        private void UpdateValue(int steps)
        {
            var target = currentValue + steps * Interval;
            SetValue(target, steps);
        }

        public void SetValue(float value, int steps = 0)
        {
            currentValue = Clamp(value, MinimumValue, MaximumValue);
            ValueChanged?.Invoke(this, new SliderValueChangeEventArgs(currentValue, steps * Interval, steps));
        }
    }
}