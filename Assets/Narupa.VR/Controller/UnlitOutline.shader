﻿Shader "Unlit/Outline"
{
    Properties
    {
        // Color property for material inspector, default to white
        _Color ("Main Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        
         ZWrite Off // don't write to depth buffer 
            // in order not to occlude other objects

         Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
         
        Pass
        {
            CGPROGRAM
            
            
            
            #pragma vertex vert
            #pragma fragment frag
            // include file that contains UnityObjectToWorldNormal helper function
            #include "UnityCG.cginc"

            struct v2f {
                // we'll output world space normal as one of regular ("texcoord") interpolators
                float4 pos : SV_POSITION;
                float3 worldViewDir : TEXCOORD1;
                float3 worldNormal : TEXCOORD2;
            };

            // vertex shader: takes object space normal as input too
            v2f vert (float4 vertex : POSITION, float3 normal : NORMAL)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(vertex);
                // compute world space position of the vertex
                float3 worldPos = mul(unity_ObjectToWorld, vertex).xyz;
                // compute world space view direction
                o.worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
                // UnityCG.cginc file contains function to transform
                // normal from object to world space, use that
                o.worldNormal = UnityObjectToWorldNormal(normal);
                return o;
            }
            
            fixed4 _Color;
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 c = _Color;
                // 1 in middle, 0 on outside
                float p = saturate(dot(i.worldViewDir, i.worldNormal));
                p = 1 - p;
                p = saturate(p * p * p * p * 5);
                c.a = saturate(p);
                return c;
            }
            ENDCG
        }
    }
}