﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using Nano.Transport.Variables;
using NSB.MMD.Styles;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Audio;

namespace Narupa.VR.Sonification
{
    public class mixerControl : FrameFilter
    {
        //public FrameSource FrameSource;
        public NSBSelection Touching = new NSBSelection();
        NSBSelection Selection = new NSBSelection();
        private List<Vector3> debugLines = new List<Vector3>();
        private List<Vector2Int> contacts = new List<Vector2Int>();
        private List<float> distances = new List<float>();
        private List<byte> bondCounts = new List<byte>();

        private List<float> secondCounters = new List<float>();
        private List<float> timers = new List<float>();
        private List<float> triggerPause = new List<float>();

        public AudioMixer mixer;

        int numSynthVoices = 16;

        public float threshold = 0.1f;
        public int jumpThreshold = 1;
        public float speed;

        // Use this for initialization
        void Start ()
        {
            if (mixer != null)
            {
                for (int v = 1; v <= numSynthVoices; v++)
                {
                    String tempFreqID = "freq" + v;
                    String tempFdbkID = "fdbk" + v;
                    String tempTrigID = "trig" + v;
                    mixer.SetFloat(tempFdbkID, 0.999f);
                    mixer.SetFloat(tempFreqID, 50);
                    mixer.SetFloat(tempTrigID, 0f);
                }
            }

            for (int i = 0; i < numSynthVoices; i++)
            {
                secondCounters.Add(0);
                timers.Add(0);
                triggerPause.Add(0);
            }

            System.Random randy = new System.Random();

            for (int i = 0; i < numSynthVoices; i++)
            {
                float waitTime = randy.Next(0, 100) * 0.01f;
                waitTime = (waitTime * 0.5f) + 0.2f;
                // start the timer that will make the voice available to be reset after a given time
                StartCoroutine(StartVoiceTimer(i, waitTime));
            }
        }

        // Update is called once per frame
        public override void Refresh()
        {
            var frame = frameSource.Component.Frame;
            
            if (frame == null)
                return;

            //----------------------------------------------------------------------------------------------------

     
            bondCounts.Resize(frame.AtomCount);

            for (int id = 0; id < frame.AtomCount; ++id)
            {
                bondCounts[id] = 0;
            }


            int carbon = (int)Nano.Science.Element.Carbon;

            for (int i = 0; i < frame.BondCount; ++i)
            {
                var bond = frame.BondPairs[i];

                if (frame.AtomTypes[bond.A] == carbon
                    && frame.AtomTypes[bond.B] == carbon)
                {
                    bondCounts[bond.A] += 1;
                    bondCounts[bond.B] += 1;
                }
            }

            for (int id = 0; id < frame.AtomCount; ++id)
            {
                if (bondCounts[id] >= 2)
                {
                    Selection.Add(id);
                }
            }

            //Debug.Log("Selection.Count: " + Selection.Count);

            var contact = Vector2Int.zero;

            for (int i = 0; i < Selection.Count; ++i)
            {
                int a = Selection[i];

                for (int j = i + 1; j < Selection.Count; ++j)
                {
                    int b = Selection[j];

                    float d = Vector3.Distance(frame.AtomPositions[a],
                        frame.AtomPositions[b]);

                    if (d < threshold && Mathf.Abs(a - b) >= jumpThreshold)
                    {
                        debugLines.Add(frame.AtomPositions[a]);
                        debugLines.Add(frame.AtomPositions[b]);

                        Touching.Add(a);
                        Touching.Add(b);

                        contact.x = a;
                        contact.y = b;
                        contacts.Add(contact);

                        distances.Add(d);
                    }
                }
            }

            for (int i = 0; i < numSynthVoices; i++)
            {
                secondCounters[i] += Time.deltaTime;
            }
       
            if(mixer != null)
            {
                mixer.SetFloat("fdbk1", 0.999f);

                System.Random randy = new System.Random();

                for (int i = 0; i < contacts.Count && i < numSynthVoices; i++)
                {
                    if (timers[i] == 0.0)
                    {
                        String freqID = "freq" + (i + 1);
                        String trigID = "trig" + (i + 1);

                        float r = randy.Next(0, 100) * 0.01f;
                        r *= 0.3f;

                        float waitTime = randy.Next(0, 100) * 0.01f;
                        waitTime = (waitTime * 0.5f) + 0.2f;

                        // first calculate the frequency corresponding to the distance of the contact
                        float freqVal = Mathf.Clamp01(distances[i] * 0.4f + r);
                        // set the voice to that frequency
                        mixer.SetFloat(freqID, freqVal);
                        // set the trigger to 1
                        mixer.SetFloat(trigID, 1.0f);
                        // and then start the timer that will reset it 0 after a period
                        StartCoroutine(ResetTriggerAfterTime(i, 1f));
                        // start the timer that will make the voice available to be reset after a given time
                        //StartCoroutine(StartVoiceTimer(i, waitTime));
                        StartCoroutine(StartVoiceTimer(i, 0.3f));
                    }
                }
            }

            contacts.Clear();
            Touching.Clear();
            distances.Clear();
        }

        public IEnumerator StartVoiceTimer(int index, float speed)
        {
            while (timers[index] < 1.0f)
            {
                timers[index] += (Time.deltaTime * speed);
                yield return null;
            }
            Debug.Log("Reset: " + index);
            timers[index] = 0.0f;
        }

        public IEnumerator ResetTriggerAfterTime(int index, float speed)
        {
            while (triggerPause[index] < 0.5)
            {
                triggerPause[index] += Time.deltaTime * speed;
                yield return null;
            }
            String id = "trig" + (index + 1);
            mixer.SetFloat(id, 0.0f);
            triggerPause[index] = 0;
        }
    }
}
