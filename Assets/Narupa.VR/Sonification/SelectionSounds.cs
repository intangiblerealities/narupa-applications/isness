﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using Valve.VR;

namespace Narupa.VR.Sonification
{
    public class SelectionSounds : MonoBehaviour
    {
        public SteamVR_Action_Boolean actionTrigger;

        public AudioSource audioSource;
        
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
      
            if (actionTrigger != null)
            {
                actionTrigger.AddOnChangeListener(TriggerClicked, SteamVR_Input_Sources.Any);
            }
        }

        private void TriggerClicked(SteamVR_Action_Boolean action, 
                                    SteamVR_Input_Sources source,
                                    bool state)
        {
            if (state)
            {
                Debug.Log("trigger!");
                audioSource.Play();
            }
        }
    }
}
