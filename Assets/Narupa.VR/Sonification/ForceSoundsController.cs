﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Nano.Transport.Variables.Interaction;
using Narupa.VR.Player.Control;
using NSB.MMD.Styles;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Sonification
{
    public class ForceSoundsController : FrameFilter
    {
        //public FrameSource frameSource;

        private InstancePool<InteractionForceInfo> interactions;
        private List<InteractionForceInfo> fadingInteractions = new List<InteractionForceInfo>();
        private HashSet<InteractionForceInfo> allActiveInteractions = new HashSet<InteractionForceInfo>();
        [SerializeField] private InstancePoolSetup interactionSetup;

        private InteractionForceInfoComparer comparer = new InteractionForceInfoComparer();

        HashSet<InteractionForceInfo> liveIFIs;
        HashSet<InteractionForceInfo> interactionInfoIncoming;
        //private Dictionary<InteractionForceInfo, ForceInteractionSoundsGen> instanceMap = new Dictionary<InteractionForceInfo, ForceInteractionSoundsGen>();

        void Start()
        {
            liveIFIs = new HashSet<InteractionForceInfo>(comparer);
            interactionInfoIncoming = new HashSet<InteractionForceInfo>(comparer);

            //interactions = interactionSetup.FinaliseAudio<InteractionForceInfo>(false, comparer);
        }

        void Update()
        {
            var frame = frameSource.Component.Frame;
            if (frame == null)
                return;

            var infos = frame.InteractionForceInfo;

            if (infos == null)
            {
                interactions.Clear();
            }
            else
            {
                // @review -  
                // Convert the incoming array of structs (infos) into a HashSet
                HashSet<InteractionForceInfo> incomingHash = new HashSet<InteractionForceInfo>(infos, comparer);
                HashSet<InteractionForceInfo> incomingHashDiff = new HashSet<InteractionForceInfo>(infos, comparer);
                HashSet<InteractionForceInfo> fadingMatchesIncoming = new HashSet<InteractionForceInfo>(infos, comparer);
                // If there is an interaction and it has the same ID as an existing interaction that is currently fading, stop it fading and respond to the new interaction
                if (incomingHash.Count > 0)
                {
                    // get those that are in common for incoming and fading
                    fadingMatchesIncoming.IntersectWith(fadingInteractions);
                    foreach (InteractionForceInfo info in fadingMatchesIncoming)
                    {
                        var instance = interactions.Get(info);
                        var soundGen = instance.GetComponent<ForceInteractionSoundsGen>();
                        soundGen.StopFade();
                        fadingInteractions.Remove(info);
                    }
                }

                // new IFI Gained
                // Incoming.ExceptWith(live ifis not including fading)
                incomingHashDiff.ExceptWith(interactionInfoIncoming);
                // incomingHashDiff now contains those IFIs that are in the current frame but were not in the previous frame
                if (incomingHashDiff.Count > 0)
                {
                    foreach (var interaction in incomingHashDiff)
                    {
                        interactions.Get(interaction);
                        allActiveInteractions.Add(interaction);
                    }
                }
                interactionInfoIncoming = incomingHash;

                // IFI Lost 
                // find the elements in liveIFIs that are not the same as any of those in incomingHash   
                // liveIFIs now contains the IFIs that were in the previous frame but are not in the current frame (i.e. those that have gone)
                liveIFIs.ExceptWith(incomingHash);
                //if interactionInfoRef.Count > 0, then start fading coroutine and add them all to fadingInteractions.
                if (liveIFIs.Count > 0)
                {
                    foreach (var interaction in liveIFIs)
                    {
                        var instance = interactions.Get(interaction);
                        var soundGen = instance.GetComponent<ForceInteractionSoundsGen>();
                        allActiveInteractions.Add(interaction);
                        soundGen.StartFade();
                    }
                    fadingInteractions.AddRange(liveIFIs);
                }
                liveIFIs = incomingHash;

                // check if any of the fading interactions have finished and if they have, remove them from fadingInteractions
                List<InteractionForceInfo> infosToRemove = new List<InteractionForceInfo>();
            
                foreach (var fading in fadingInteractions)
                {
                    var instance = interactions.Get(fading);
                    var soundGen = instance.GetComponent<ForceInteractionSoundsGen>();
                    if (soundGen.fadeFinished)
                    {
                        //add to list to be removed
                        infosToRemove.Add(fading);
                        interactions.Discard(fading);
                    }
                }
                // remove infosToRemove from fadingInteractions
                foreach (var info in infosToRemove) fadingInteractions.Remove(info);

                // for all the interactions that are live and not fading, update their config with the current inputFrame 
                // all the interactions that are live and not fading --> referenced by prevInteractions
            
                int count = 0;
                foreach (var info in incomingHash)
                {
                    var instance = interactions.Get(info);
                    var soundGen = instance.GetComponent<ForceInteractionSoundsGen>();
                    instance.SetConfig(info);
                    count++;
                    if (count == 1)
                    {
                        soundGen.pitch = 1f;
                        soundGen.pan = -1f; 
                    }
                    else if (count == 2)
                    {
                        soundGen.pitch = 1.5f;
                        soundGen.pan = 1f;
                    }
                }
            }
        }
    }

    internal class InteractionForceInfoComparer : EqualityComparer<InteractionForceInfo>
    {
        /// <summary>
        /// Checks if the hash code for this instance is the same as the hash code of the input.
        /// </summary>
        public override bool Equals(InteractionForceInfo this_, InteractionForceInfo other)
        {
            // check equlaity of player ids & device ids
            if (this_.PlayerID != other.PlayerID) return false;
            if (this_.InputID != other.InputID) return false;
            //if (other.GetHashCode() != this.GetHashCode()) return false;
            return true;
        }

        /// </summary>
        /// <returns></returns>
        public override int GetHashCode(InteractionForceInfo ifi)
        {
            int a = ifi.PlayerID;
            int b = ifi.InputID;
            return a + b;
        }

    }
}