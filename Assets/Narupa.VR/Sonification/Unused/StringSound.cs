﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using UnityEngine;

namespace Narupa.VR.Sonification.Unused
{
    public class DelayBuffer
    {
        List<float> buffer;
        float writePos = 0;
        float readPos = 0;
        int bufferSize = 1;
        public float delTimeInMs;
        private int sampleRate;
        float delTimeInSamples;

        public DelayBuffer(int bufferSize_)
        {
            delTimeInMs = 50;
            bufferSize = bufferSize_;
            buffer = new List<float>(bufferSize);
            sampleRate = AudioSettings.outputSampleRate;

            for (int i = 0; i < bufferSize; i++)
            {
                buffer.Add(0f);
            }
        }

        public void Write(float valueToAdd)
        {
            writePos++;
            if (writePos >= (bufferSize - 1))
            {
                writePos = 0;
            }

            buffer.Insert((int)writePos, valueToAdd);
        }

        public void SetDelTime(float delTimeInMs)
        {
            float delTimeInSec = delTimeInMs / 1000f;
            delTimeInSamples = delTimeInSec * sampleRate;
            if (delTimeInSamples > bufferSize)
            {
                delTimeInSamples = bufferSize;
            }
        }

        public float Read()
        {
            readPos = writePos - delTimeInSamples;
            if (readPos < 0)
            {
                readPos += bufferSize;
            }
            if (readPos > (bufferSize - 1))
            {
                readPos = bufferSize - 1;
            }
            return buffer[(int)readPos];
        }
    }

    [RequireComponent(typeof(AudioSource))]
    public class StringSound : MonoBehaviour
    {
        [SerializeField]
        public float Gain = 0.1F;

        private DelayBuffer delayBuffer;

        private double sampleRate = 0.0F;
        private bool running = false;

        float phaseInc;
        float newFreq;
        float phasePos;
        float frequency = 150;

        // Use this for initialization
        void Start ()
        {
            phasePos = 0;
            sampleRate = AudioSettings.GetConfiguration().sampleRate;
            phaseInc = (2 * Mathf.PI * frequency) / (float)sampleRate;

            delayBuffer = new DelayBuffer(4410);
            delayBuffer.SetDelTime(95);
            running = true;
            Gain = 0.1f;
            phasePos = 0;
        }
	
        // Update is called once per frame
        void Update ()
        {
	    	
        }

        public void OnAudioFilterRead(float[] data, int channels)
        {
            if (!running)
                return;
            int datalen = data.Length / channels;

            int n = 0;
            while (n < datalen)
            {
                int i = 0;
                while (i < channels)
                {
                    if (i == 0)
                    {
                        float wetSignal = delayBuffer.Read();

                        data[n * channels + i] = wetSignal;

                        float nextSinSample = Mathf.Sin(phasePos) * Gain;

                        delayBuffer.Write((nextSinSample * Gain) + (wetSignal * 0.87f));

                        phasePos += phaseInc;

                        if (phasePos >= (Mathf.PI * 2.0f))
                        {
                            phasePos = 0;
                        }
                    }
                    i++;
                }
                n++;
            }
        }
    }
}