﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using UnityEngine;
using UnityEngine.Audio;

namespace Narupa.VR.Sonification.Unused
{
    class ParamSmooth
    {
        public void initialise(float coeff)
        {
            a = coeff;
            b = 1.0f - a;
        }
        public float process(float input)
        {
            return previous = (a * input) + (b * previous);
        }
        private float a, b;
        private float previous = 0;
    };

    public class BuckyballControl : MonoBehaviour
    {
        public AudioMixer mixer;
        public FrameSource FrameSource;
        List<ParamSmooth> smoothers = new List<ParamSmooth>();

        void Start()
        {
            for (int i = 0; i < 30; i++)
            {
                ParamSmooth smooth = new ParamSmooth();
                smooth.initialise(0.1f);
                smoothers.Add(smooth);
            }
        }

        void Update()
        {
            if (FrameSource == null)
                return;
            if (FrameSource.Component.Frame != null)
            {
                NSBFrame frame = FrameSource.Component.Frame;
                if (frame == null || frame.AtomCount == 0) return;

                Vector3 centroid = new Vector3();
                for (int i = 0; i < 60; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        centroid[j] += frame.AtomPositions[i][j];
                    }
                }
                for (int j = 0; j < 3; j++)
                {
                    centroid[j] = centroid[j] / 60.0f;
                }

                float magnitudeDiffSum = 0;

                for (int i = 0; i < 60; i++)
                {
                    Vector3 diff = frame.AtomPositions[i] - centroid;
                    magnitudeDiffSum += diff.sqrMagnitude;
                }

                float radOfGyr = (1.0f / 60.0f) * magnitudeDiffSum;

                for (int i = 0; i < 60; i++)
                {
                    Vector3 diff = frame.AtomPositions[i] - centroid;
                    if (i < 30)
                    {
                        float valToSet = diff.sqrMagnitude;
                        if (diff.sqrMagnitude < radOfGyr)
                        {
                            valToSet *= -1.0f;
                        }
                        string val = "V" + i;
                        valToSet *= 2.0f;
                        mixer.SetFloat(val, smoothers[i].process(valToSet));
                    }
                }

                radOfGyr *= 30.0f;
                float freq = radOfGyr * 100.0f;

                //if (freq < -1.0f)
                //{
                //    freq *= -1.0f;
                //}

                float freq1 = freq;
                float freq2 = freq * 2.0f;
                float freq3 = freq * 1.5f;
                float freq4 = freq * 0.5f; 

                mixer.SetFloat("Freq1", freq1);
                mixer.SetFloat("Freq2", freq2);
                mixer.SetFloat("Freq3", freq3);
                mixer.SetFloat("Freq4", freq4);

                mixer.SetFloat("ModFreq1", freq1 * 0.5f);
                mixer.SetFloat("ModFreq2", freq2 * 0.5f);
                mixer.SetFloat("ModFreq3", freq3 * 0.5f);
                mixer.SetFloat("ModFreq4", freq4 * 0.5f);
            }
        }
    }
}