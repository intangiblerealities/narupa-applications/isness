﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Nano.Transport.Variables;
using NSB.MMD.Styles;
using UnityEngine;

namespace Narupa.VR.Sonification
{
//-------------------------------------------------------------------------------------------
    public class AbsValAverage
    {
        float[] buffer;
        int length = 256;
        int bufferWritePos;
        float runningTotal;

        public AbsValAverage(int length_)
        {
            buffer = new float[length_];
            length=length_;
            bufferWritePos = 0;
            runningTotal = 0;
        }

        public float update(float input)
        {
            if (bufferWritePos++ == (length-1))
            {
                bufferWritePos = 0;
            }

            runningTotal -= buffer[bufferWritePos];
            buffer[bufferWritePos] = input * input;
            runningTotal += buffer[bufferWritePos];

            if (runningTotal < 0) runningTotal = 0;

            return Mathf.Sqrt(runningTotal/length);
        }
    }

//public class CircularBuffer
//{
//    Queue<float> buffer;
//    int size;
//    int counter = 0;
//    float currentMax;
//    float currentMin;

//    public CircularBuffer(int size_)
//    {
//        currentMax = Mathf.NegativeInfinity;
//        currentMin = Mathf.Infinity;
//        size = size_;
//        buffer = new Queue<float>(size_);
//    }

//    public bool Write(float valueToAdd)
//    {
//        if (valueToAdd > currentMax) { currentMax = valueToAdd; }
//        if (valueToAdd < currentMin) { currentMin = valueToAdd; }
//        if (buffer.Count == size)
//        {
//            buffer.Dequeue();
//            buffer.Enqueue(valueToAdd);
//        }
//        else
//        {
//            buffer.Enqueue(valueToAdd);
//        }
//        if (counter++ == size-1)
//        {
//            counter = 0;
//            return true;
//        }
//        else
//        {
//            return false;
//        }
//    }
//    public float Read()
//    {
//        return buffer.Dequeue();
//    }
//    public float getCurrentMax() { return currentMax; }
//    public float getCurrentMin() { return currentMin; }
//}

    public class EnergyMapping : FrameFilter
    {
        public AbsValAverage envelopeFollower;
        public AudioSource audioSource;
        public AudioLowPassFilter lpf;
        //public NSB.VR.Player.Control.VrPlaybackRenderer playback;
        //public FrameSource source;

        [SerializeField]
        [Range(0.0f, 10f)]
        private float cutoffRampSpeedFine;

        [SerializeField]
        [Range(0.0f, 10f)]
        private float cutoffRampSpeedCoarse;

        [SerializeField]
        [Range(0.0f, 200.0f)]
        private float volumeRampSpeedFine;

        [SerializeField]
        [Range(0.0f, 200.0f)]
        private float volumeRampSpeedCoarse;

        [SerializeField]
        [Range(0.0f, 300.0f)]
        private float cuttoffMult;

        [SerializeField]
        [Range(0.01f, 3.0f)]
        private float volumeMult;

        //=============================================================================

        public float centrePointLO;
        public float rangeLO;

        public float centrePointMID;
        public float rangeMID;

        private float centrePointHI;
        private float rangeHI;

        private float cutoffCoarse;
        private float cutoffFine;

        private float currentCutoffFine;
        private float currentCutoffCoarse;

        private float destinationCutoffFine;
        private float destinationCutoffCoarse;

        private float currentVolumeFine;
        private float currentVolumeCoarse;

        private float destinationVolumeFine;
        private float destinationVolumeCoarse;

        private float cutoffSum;

        //=============================================================================

        //private CircularBuffer circularBuffer;
    
        private float previousEnergy;
        private float deltaEnergy;
        private float energy;

        void Start()
        {
            //circularBuffer = new CircularBuffer(200);

            audioSource = GetComponent<AudioSource>();
            lpf = GetComponent<AudioLowPassFilter>();
       
            previousEnergy = deltaEnergy = destinationCutoffFine = destinationCutoffCoarse = 0;
            
            cutoffRampSpeedFine = 0.92f;
            cutoffRampSpeedCoarse = 0.66f;
            volumeRampSpeedFine = 0.9f;
            volumeRampSpeedCoarse = 20.0f;
            cuttoffMult = 2.0f;
            currentCutoffFine = currentCutoffCoarse = cutoffSum = 0;
            volumeMult = 0.75f;

            centrePointLO = 0;
            rangeLO = 200;
            centrePointMID = 1000;
            rangeMID = 1400;
        }
        public override void Refresh()
        {
            var frame = frameSource?.Component.Frame;
            //var frame = playback.ne

            energy = 0;

            if (frame == null)
                return;

            try
            {
                energy = frame.GetVariable<float>(VariableName.KineticEnergy);
            }
            catch (Exception)
            {

            }

            //if ( circularBuffer.Write(currentPotEnergy))
            //{
            //    maxPotEnergy = circularBuffer.getCurrentMax();
            //    minPotEnergy = circularBuffer.getCurrentMin();
            //    potEnergyRange = (maxPotEnergy - minPotEnergy);
            //}
       
            deltaEnergy = energy - previousEnergy;
            if (deltaEnergy == 0f) return;
            previousEnergy = energy;

            float deltaEnergyAbs = Mathf.Abs(deltaEnergy);

            // Use a different mapping according to the size of deltaEnergy
            if (deltaEnergyAbs > 0 && deltaEnergyAbs <= 50.0f)
            {
                float deltaEnergyNorm = deltaEnergyAbs / 50.0f;
                destinationCutoffFine = (deltaEnergyNorm * rangeLO) + centrePointLO;
            }
            else if (deltaEnergyAbs > 50 && deltaEnergyAbs < 500)
            {
                float deltaEnergyNorm = (deltaEnergyAbs - 50) / 450.0f;
                destinationCutoffCoarse = (deltaEnergyNorm * rangeMID) + centrePointMID;
            }
       
            if (currentCutoffFine != destinationCutoffFine)
            {
                currentCutoffFine = Mathf.Lerp(currentCutoffFine, destinationCutoffFine, Time.deltaTime * cutoffRampSpeedFine);
            }
            if (currentCutoffCoarse != destinationCutoffCoarse)
            {
                currentCutoffCoarse = Mathf.Lerp(currentCutoffCoarse, destinationCutoffCoarse, Time.deltaTime * cutoffRampSpeedCoarse);
            }

            cutoffSum = currentCutoffCoarse + currentCutoffFine;

            if (cutoffSum < 0)
            {
                cutoffSum = 0;
            }
            if (cutoffSum > 20000)
            {
                cutoffSum = 20000;
            }

            lpf.cutoffFrequency = cutoffSum * cuttoffMult;

            if (deltaEnergyAbs > 0 && deltaEnergyAbs <= 50.0f)
            {
                float deltaEnergyNorm = deltaEnergyAbs / 50.0f;
                destinationVolumeFine = deltaEnergyNorm;
                // Debug.Log("1deltaEnergyNorm: " + deltaEnergyNorm);
            }
            else if (deltaEnergyAbs > 50 && deltaEnergyAbs < 500)
            {
                float deltaEnergyNorm = (deltaEnergyAbs - 50) / 450.0f;
                destinationVolumeCoarse = deltaEnergyNorm;
                // Debug.Log("2deltaEnergyNorm: " + deltaEnergyNorm);
            }
            else
            {
                //Debug.Log("3deltaEnergyAbs: " + deltaEnergyAbs);
            }
        
            if (destinationVolumeFine != currentVolumeFine)
            {
                currentVolumeFine = Mathf.Lerp(currentVolumeFine, destinationVolumeFine, Time.deltaTime * volumeRampSpeedFine);
            }
            if (destinationVolumeCoarse != currentVolumeCoarse)
            {
                currentVolumeCoarse = Mathf.Lerp(currentVolumeCoarse, destinationVolumeCoarse, Time.deltaTime * volumeRampSpeedCoarse);
            }

            float volumeSum = currentVolumeCoarse + currentVolumeFine;
            volumeSum *= volumeMult;
            volumeSum *= volumeSum;

            audioSource.volume = Mathf.Clamp01(volumeSum);
        }

        //void OnAudioFilterRead(float[] data, int channels)
        //{
        //    if (!running)
        //        return;
        //    int datalen = data.Length / channels;

        //    float newFreq = (normPotEnergy * 300.0f) + 200.0f;
        //    phaseInc = newFreq / sampleRate;
        //    phasePos[0] = phasePos[1] = 0;

        //    int n = 0;
        //    while (n < datalen)
        //    {
        //        int ch = 0;
        //        while (ch < channels)
        //        {
        //            currentAmp = (destinationAmp * 0.01f) + (currentAmp * 0.99f);
        //            phasePos[ch] += phaseInc;

        //            if (phasePos[ch] >= (Mathf.PI * 2.0f))
        //            {
        //                phasePos[ch] = 0.0f;
        //            }

        //            data[n * channels + ch] += (Mathf.Sin(phasePos[ch]) * currentAmp);
        //            ch++;
        //        }
        //        n++;
        //    }
        //}
    }
}