﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using Narupa.Client.Network;
using Narupa.VR.Multiplayer;
using Narupa.VR.Player.Control;
using UnityEngine;

namespace Narupa.VR
{
    /// <summary>
    ///     Handles transformations and scaling of the simulation box in world space.
    /// </summary>
    /// <remarks>
    ///     Should be placed in a child game object of the simulation playback game object.
    /// </remarks>
    public class WorldLocalSpaceTransformer : MonoBehaviour
    {
        /// <summary>
        ///     Whether running in local multiplayer mode.
        /// </summary>
        public static bool LocalMultiplayerMode = false;

        private readonly Vector3?[] transformPoints = new Vector3?[2];
        private readonly Vector3?[] transformPointsInitial = new Vector3?[2];
        private readonly Quaternion?[] transformQuaternions = new Quaternion?[2];
        private readonly Quaternion?[] transformQuaternionsInitial = new Quaternion?[2];
        private Transform parentTransform;

        private bool scaleUpdated;
        private Vector3 startingCenter;
        private Vector3 startingCenterOffset;
        private float startingDistance;

        private Quaternion startingRotation;
        private Quaternion startingRotationOffsetInverse;
        private Vector3 startingScale;

        private new Transform transform;

        /// <summary>
        ///     Event triggered when the scale changes in multiplayer.
        /// </summary>
        public event EventHandler MultiplayerScaleChange;

        private NetworkManager networkManager;

        private void Awake()
        {
            transform = base.transform;

            parentTransform = transform.parent;
            networkManager = FindObjectOfType<NetworkManager>();
        }
        
        /// <summary>
        ///     Transforms a point from world space (left-handed) into narupa space (right-handed).
        /// </summary>
        /// <param name="worldSpacePosition"></param>
        /// <returns></returns>
        public Vector3 TransformWorldSpacePointToLocalSpace(Vector3 worldSpacePosition)
        {
            // World Space (Left-Handed) to Simulation Space (Left-Handed)
            return transform.InverseTransformPoint(worldSpacePosition);
        }

        /// <summary>
        ///     Transforms a point from world space (left-handed) into narupa space (right-handed).
        /// </summary>
        /// <param name="worldSpacePosition"></param>
        /// <returns></returns>
        public Vector3 TransformWorldSpacePointToNarupaSpace(Vector3 worldSpacePosition)
        {
            // World Space (Left-Handed) to Simulation Space (Left-Handed)
            var pt = transform.InverseTransformPoint(worldSpacePosition);
            // Simulation Space (Left-Handed) to Narupa Space (Right-Handed)
            pt.z = -pt.z;
            return pt;
        }

        /// <summary>
        ///     Transforms a point from narupa space into world space.
        /// </summary>
        /// <param name="narupaSpacePosition"></param>
        /// <returns></returns>
        public Vector3 TransformNarupaSpacePointToWorldSpace(Vector3 narupaSpacePosition)
        {
            var pt = narupaSpacePosition;
            // Narupa Space (Right-Handed) to Simulation Space (Left-Handed)
            pt.z = -pt.z;
            // Simulation Space (Left-Handed) to World Space (Left-Handed)
            pt = transform.TransformPoint(pt);
            return pt;
        }
        
        /// <summary>
        ///     Transforms a point from narupa space into world space.
        /// </summary>
        /// <param name="narupaSpaceDirection"></param>
        /// <returns></returns>
        public Vector3 TransformNarupaSpaceDirectionToWorldSpace(Vector3 narupaSpaceDirection)
        {
            var direction = narupaSpaceDirection;
            // Narupa Space (Right-Handed) to Simulation Space (Left-Handed)
            direction.z = -direction.z;
            // Simulation Space (Left-Handed) to World Space (Left-Handed)
            direction = transform.TransformDirection(direction);
            return direction;
        }

        /// <summary>
        ///     Transforms a quaternion from wolrd space into narupa space.
        /// </summary>
        /// <param name="worldSpaceQuaternion"></param>
        /// <returns></returns>
        public Quaternion TransformWorldSpaceQuaternionToNarupaSpace(Quaternion worldSpaceQuaternion)
        {
            // World Space (Left-Handed) to Simulation Space (Left-Handed)
            var rot = Quaternion.Inverse(transform.rotation) * worldSpaceQuaternion;
            // Simulation Space (Left-Handed) to Narupa Space (Right-Handed)
            //rot.z = -rot.z;
            //rot.w = -rot.w;
            return rot;
        }

        /// <summary>
        ///     Transforms a quaternion from narupa space into world space.
        /// </summary>
        /// <param name="narupaSpaceQuaternion"></param>
        /// <returns></returns>
        public Quaternion TransformNarupaSpaceQuaternionToWorldSpace(Quaternion narupaSpaceQuaternion)
        {
            var rot = narupaSpaceQuaternion;
            // Narupa Space (Right-Handed) to Simulation Space (Left-Handed)
            //rot.z = -rot.z;
            //rot.w = -rot.w;
            // Simulation Space (Left-Handed) to World Space (Left-Handed)
            rot = transform.rotation * rot;
            return rot;
        }

        /// <summary>
        ///     Registers a point for performing transformations of the Narupa scene scaling.
        /// </summary>
        /// <param name="worldSpacePosition"></param>
        /// <param name="worldRotation"></param>
        /// <returns></returns>
        public int RegisterTransformPoint(Vector3 worldSpacePosition, Quaternion worldRotation)
        {
            var index = 0;
            for (; index < transformPoints.Length; index++)
            {
                if (transformPoints[index].HasValue) continue;

                transformPoints[index] = worldSpacePosition;
                transformPointsInitial[index] = worldSpacePosition;

                transformQuaternions[index] = worldRotation;
                transformQuaternionsInitial[index] = worldRotation;

                break;
            }

            if (index < transformPoints.Length)
            {
                var vectors = new Vector3[2];
                var v = 0;

                foreach (var point in transformPoints)
                {
                    if (point.HasValue == false) continue;

                    vectors[v++] = point.Value;

                    if (v == 2) break;
                }

                if (v == 2)
                {
                    startingRotation = parentTransform.rotation;

                    var baseRotation = CalculateRotation(vectors[0], vectors[1]);

                    // get the inverse so we can subtract it from the new rotations
                    startingRotationOffsetInverse = Quaternion.Inverse(baseRotation);

                    startingDistance = Vector3.Distance(vectors[0], vectors[1]);
                    startingScale = parentTransform.localScale;

                    startingCenter = parentTransform.position;
                    startingCenterOffset = (vectors[0] + vectors[1]) * 0.5f;
                }
                else if (v == 1)
                {
                    startingCenter = parentTransform.position;
                    //startingRotation = parentTransform.rotation;

                    //startingRotationOffsetInverse = Quaternion.Inverse(worldRotation);
                }

                return index;
            }

            throw new Exception("No index available");
        }

        /// <summary>
        ///     Updates a transform point.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="worldSpacePosition"></param>
        /// <param name="worldSpaceRotation"></param>
        public void UpdateTransformPoint(int index, Vector3 worldSpacePosition, Quaternion worldSpaceRotation)
        {
            transformPoints[index] = worldSpacePosition;
            transformQuaternions[index] = worldSpaceRotation;
        }

        /// <summary>
        ///     Releases a previously added transform point.
        /// </summary>
        /// <param name="index"></param>
        public void ReleaseTransformPoint(int index)
        {
            transformPoints[index] = null;
            transformPointsInitial[index] = null;

            transformQuaternions[index] = null;
            transformQuaternionsInitial[index] = null;

            startingCenter = parentTransform.position;

            for (var i = 0; i < transformPoints.Length; i++)
            {
                if (transformPoints[i].HasValue == false) continue;

                transformPointsInitial[i] = transformPoints[i];
            }
        }


        private void LateUpdate()
        {
            var points = new List<Vector3>();
            var pointsInitial = new List<Vector3>();

            for (var index = 0; index < transformPoints.Length; index++)
            {
                if (transformPoints[index].HasValue == false) continue;

                points.Add(transformPoints[index].Value);
                if (transformPointsInitial?[index] != null) pointsInitial.Add(transformPointsInitial[index].Value);
            }

            if (points.Count == 1 && !LocalMultiplayerMode)
            {
                parentTransform.position = startingCenter + (points[0] - pointsInitial[0]);
            }
            else if (points.Count == 2)
            {
                var center = (points[0] + points[1]) * 0.5f;

                var rotation = CalculateRotation(points[0], points[1]);

                var distance = Vector3.Distance(points[0], points[1]);

                var ratio = startingScale.x * (distance / startingDistance);

                if (!LocalMultiplayerMode)
                {
                    parentTransform.position = startingCenter + (center - startingCenterOffset);
                    parentTransform.rotation = startingRotation * rotation * startingRotationOffsetInverse;
                    parentTransform.localScale = new Vector3(ratio, ratio, ratio);
                }
                else
                {
                    //Set scale flag to indicate that box has changed scale, but we won't send the message until it's finished being rescaled
                    //scaleUpdated = true;
                    //MultiplayerSteamVrCalibration.MultiplayerScale = new Vector3(ratio, ratio, ratio);
                }
            }
            //If not still resizing and we have resized, trigger event to transmit new scale to other players. 
            else if (scaleUpdated)
            {
                //scaleUpdated = false;
                //networkManager?.CurrentConnection.OnSimulationScaleChange(MultiplayerSteamVrCalibration.MultiplayerScale[0]);
                //MultiplayerScaleChange?.Invoke(MultiplayerSteamVrCalibration.MultiplayerScale[0], null);
            }
        }

        private static Quaternion CalculateRotation(Vector3 point1, Vector3 point2)
        {
            var diff = point1 - point2;

            var diffY = new Vector2(diff.x, diff.z);

            var angley = Mathf.Atan2(diffY.x, diffY.y) * Mathf.Rad2Deg;

            var rotation = Quaternion.Euler(0, angley, 0);

            return rotation;
        }
    }
}