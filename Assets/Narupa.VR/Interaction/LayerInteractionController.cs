﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Nano.Transport.Variables.Interaction;
using Narupa.Client.Network;
using Narupa.VR.Multiplayer;
using Narupa.VR.Player.Control;
using Narupa.VR.Selection;
using Narupa.VR.UI.Selection;
using NSB.Processing;
using NSB.Simbox;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Interaction
{
    /// <summary>
    ///     Manages the instantiation and updates to interactions with atoms in the simulation.
    /// </summary>
    [RequireComponent(typeof(WorldLocalSpaceTransformer))]
    public class LayerInteractionController : MonoBehaviour
    {
        
        /// <summary>
        ///     A set of atoms to select for each interaction.
        ///     TODO refactor once streams have variable lengths.
        /// </summary>
        private readonly List<IList<int>> atomsToSelectTransmit = new List<IList<int>>();

        /// <summary>
        ///     Interactions to transmit this frame.
        /// </summary>
        private readonly List<Nano.Transport.Variables.Interaction.Interaction> interactionsToTransmit =
            new List<Nano.Transport.Variables.Interaction.Interaction>();

        private readonly Dictionary<ushort, LayerInteraction> activeInteractions =
            new Dictionary<ushort, LayerInteraction>();

        // for input streams...
        private NetworkManager networkManager;

        private SelectionManager selectionManager;
        private WorldLocalSpaceTransformer narupaTransformer;

        private void Start()
        {
            networkManager = FindObjectOfType<NetworkManager>();
            selectionManager = FindObjectOfType<SelectionManager>();
            narupaTransformer = FindObjectOfType<WorldLocalSpaceTransformer>();
        }

        /// <summary>
        ///     Adds an interaction to be used as a force location in Narupa.
        /// </summary>
        /// <param name="tform">Transform to use as interaction location.</param>
        /// <param name="interaction">Interaction settings.</param>
        public void AddLegacyInteraction(Transform tform, Nano.Transport.Variables.Interaction.Interaction interaction)
        {
            var position = tform.position;
            AddLegacyInteraction(position, interaction);
        }

        public void AddLegacyInteraction(Vector3 pointInWorldSpace,
            Nano.Transport.Variables.Interaction.Interaction interaction, NSBSelection atomsToInteract = null)
        {
            var vec = pointInWorldSpace;
            vec = narupaTransformer.TransformWorldSpacePointToNarupaSpace(pointInWorldSpace);
            interaction.Position = new SlimMath.Vector3(vec.x, vec.y, vec.z);
            interactionsToTransmit.Add(interaction);

            if (atomsToInteract == null)
                atomsToSelectTransmit.Add(new IndexedSet<int>());
            else
                atomsToSelectTransmit.Add(atomsToInteract);
        }

        /// <summary>
        ///     Adds an interaction to the simulation.
        /// </summary>
        /// <param name="interactionPosition">Position in world space of the interaction.</param>
        /// <param name="frame">Frame to gather data from.</param>
        /// <param name="deviceId">Device ID to use to identify interaction.</param>
        /// <param name="gradientScaleFactor">Gradient scale factor to use in interaction.</param>
        public void AddInteraction(Vector3 interactionPosition, NSBFrame frame, ushort deviceId,
            float gradientScaleFactor)
        {
            //First, find the nearest atom.
            float distanceSqr;
            var localPosition = narupaTransformer.TransformWorldSpacePointToLocalSpace(interactionPosition);
            var narupaPosition = narupaTransformer.TransformWorldSpacePointToNarupaSpace(interactionPosition);
            var nearestAtomId = GetNearestAtom(localPosition, frame, out distanceSqr);
            if (nearestAtomId == -1)
                return;

            //Figure out which atoms we're going to be interacting with.
            var atomsSelected = GetAtomsToInteractWith(nearestAtomId);

            //Stash which type of interaction this is going to be based on selection.
            //Note that we still create an "interaction" for NoInteraction, just so we save resources.
            InteractionType interactionType;
            if (atomsSelected == null)
                interactionType = InteractionType.NoInteraction;
            else if (atomsSelected.Count == 1)
                interactionType = InteractionType.SingleAtom;
            else
                interactionType = InteractionType.Group;

            //Create an interaction with appropriate data.
            var interaction =
                new Nano.Transport.Variables.Interaction.Interaction
                {
                    GradientScaleFactor = gradientScaleFactor,
                    InputID = (byte) deviceId,
                    InteractionType = (byte) interactionType,
                    PlayerID = SteamVrObjectSender.PlayerId,
                    Position = new SlimMath.Vector3(narupaPosition.x, narupaPosition.y, narupaPosition.z)
                };

            //Store this interaction so it can be updated instead of recreated constantly.
            activeInteractions[deviceId] = new LayerInteraction
            {
                Interaction = interaction,
                AtomsToInteract = atomsSelected
            };

            if (interactionType == InteractionType.NoInteraction) return;

            //Add interaction to queue to be transmitted.
            AddLegacyInteraction(interactionPosition, interaction, atomsSelected);
        }

        /// <summary>
        /// Given a device ID and position, updates an active interaction on that device.
        /// </summary>
        /// <param name="deviceId">The device ID to update.</param>
        /// <param name="interactionPosition">The position of the interaction.</param>
        public void UpdateInteractionPosition(ushort deviceId, Vector3 interactionPosition)
        {
            if (activeInteractions.ContainsKey(deviceId) == false)
                return;

            var interaction = activeInteractions[deviceId];

            if ((InteractionType) interaction.Interaction.InteractionType == InteractionType.NoInteraction)
                return;
            AddLegacyInteraction(interactionPosition, interaction.Interaction, interaction.AtomsToInteract);
        }

        /// <summary>
        /// Deletes an interaction associated with the given device ID.
        /// </summary>
        /// <param name="deviceId">The device ID.</param>
        public void DeleteInteraction(ushort deviceId)
        {
            activeInteractions.Remove(deviceId);
        }

        /// <summary>
        /// Clears all active interactions.
        /// </summary>
        public void ClearInteractions()
        {
            activeInteractions.Clear();
        }

        private NSBSelection GetAtomsToInteractWith(int nearestAtomId)
        {
            InteractiveSelection selectionToInteractWith = null;
            if (selectionManager != null)
            {
                //Prioritise the atoms to interact with by the active layer, then those in groups, then single atom.
                if (selectionManager.ActiveSelection != null &&
                    selectionManager.ActiveSelection.Selection.Contains(nearestAtomId))
                    selectionToInteractWith = selectionManager.ActiveSelection;
                else
                    for (var selectionIndex = selectionManager.Selections.Count - 1;
                        selectionIndex >= 0;
                        selectionIndex--)
                        if (selectionManager.Selections[selectionIndex].InteractionProperties.InteractionType !=
                            InteractionType.NoInteraction && selectionManager.Selections[selectionIndex].Selection
                                .Contains(nearestAtomId))
                        {
                            selectionToInteractWith = selectionManager.Selections[selectionIndex];
                            break;
                        }
            }

            if (selectionToInteractWith == null)
            {
                //If base layer is non-interactable, return null.
                //TODO put base layer into a proper selection, this is bullshit.
                if (ActiveSelectionTemplate.InteractWithBaseLayer == false)
                    return null;

                var atomsInteracting = new NSBSelection();
                atomsInteracting.Add(nearestAtomId);
                return atomsInteracting;
            }

            //Either return the selection atoms, a new set with just the single atom in it, or null depending on interaciton type.
            switch (selectionToInteractWith.InteractionProperties.InteractionType)
            {
                case InteractionType.Group:
                    return selectionToInteractWith.Selection;

                case InteractionType.NoInteraction:
                    return null;

                case InteractionType.SingleAtom:
                    var atomsInteracting = new NSBSelection();
                    atomsInteracting.Add(nearestAtomId);
                    return atomsInteracting;

                default:
                    return null;
            }
        }

        private bool IsAtomVisible(int atom)
        {
            //If the base layer is visible, then all atoms are visible.
            if (ActiveSelectionTemplate.BaseLayerVisible) return true;

            if (selectionManager == null) return false;

            foreach (var selection in selectionManager.Selections)
                if (selection.Selection.Contains(atom))
                    return true;
            return false;
        }

        /// <summary>
        /// </summary>
        /// <param name="interactionPosition">Position in Narupa space.</param>
        /// <param name="frame"></param>
        /// <param name="nearestDistanceSqr"></param>
        /// <returns></returns>
        private int GetNearestAtom(Vector3 interactionPosition, NSBFrame frame, out float nearestDistanceSqr)
        {
            float distSqr;
            nearestDistanceSqr = float.MaxValue;
            var nearestAtom = -1;
            for (var i = 0; i < frame.AtomCount; i++)
            {
                if (IsInteractableAtom(i) == false)
                    continue;
                
                distSqr = FasterMath.Sub(frame.AtomPositions[i], interactionPosition).sqrMagnitude;
                //distSqr = Vector3.Distance(transformPos, frame.AtomPositions[i]);
                if (distSqr < nearestDistanceSqr)
                {
                    nearestAtom = i;
                    nearestDistanceSqr = distSqr;
                }
            }

            return nearestAtom;
        }

        private bool IsInteractableAtom(int i)
        {
            if (IsAtomVisible(i) == false) 
                return false;
            if (ActiveSelectionTemplate.InteractWithBaseLayer == true)
                return true; 
            
            //if we can't interact with the base layer, we have to check that the atom is in a selection which is interactable. 
            foreach (var selection in selectionManager.Selections)
            {
                if(selection.InteractionProperties.InteractionType == InteractionType.NoInteraction)
                    continue;
                if (selection.Selection.Contains(i))
                    return true;
            }

            return false;

        }
        private void LateUpdate()
        {
            networkManager.CurrentConnection?.ClearInputs();

            CopyInteractions();
        }

        /// <summary>
        ///     Called once per frame, copies interactionsToTransmit into the <see cref="NSBFrameReader" /> to be sent to the
        ///     server.
        /// </summary>
        private void CopyInteractions()
        {
            networkManager.CurrentConnection?.CopyInteractions(interactionsToTransmit, atomsToSelectTransmit);
            interactionsToTransmit.Clear();
            atomsToSelectTransmit.Clear();
        }

        private class LayerInteraction
        {
            public NSBSelection AtomsToInteract;
            public Nano.Transport.Variables.Interaction.Interaction Interaction;
        }
    }
}