﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Nano.Client;
using Nano.Rainbow.v1_0;
using Nano.Transport.Variables;
using Narupa.Client.Network;
using Narupa.Client.Network.Event;
using Narupa.Client.Playback;
using Narupa.Trajectory;
using Narupa.VR.UI;
using NSB.Debugging;
using NSB.Examples.Player.Control;
using NSB.Interaction;
using NSB.MMD;
using NSB.MMD.RenderingTypes;
using NSB.MMD.Renders;
using NSB.Processing;
using NSB.Simbox;
using NSB.Simbox.Debugging;
using SlimMath;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;
using Topology = NSB.Simbox.Topology.Topology;
using Vector2 = UnityEngine.Vector2;

namespace Narupa.VR.Player.Control
{
    /// <summary>
    ///     A copy of the <see cref="PlaybackRenderer" /> with a view tweaks for VR.
    /// </summary>
    public class VrPlaybackRenderer : MonoBehaviour,
        IFrameSource
    {
        [SerializeField] private BoundingBoxRenderer boundsRenderer;

        [SerializeField] private new Camera camera;

        [Range(1, 32)] public float DarkenNumber;

        /// <summary>
        /// The current interpolated frame.
        /// </summary>
        public NSBFrame InterpolatedFrame = new NSBFrame();

        private NetworkManager networkManager;

        [SerializeField] private Image loadingImage;

        [Header("Loading Icon")] [SerializeField]
        private GameObject loadingObject;

        private SimulationMetadata loadingSim;
        private string loadingSim2;

        /// <summary>
        /// Whether to loop when playback finishes.
        /// </summary>
        public bool Looping => frameProvider.IsFixedHistory;

        public NSBFrame NextFrame;

        [Header("Rendering")] [Header("Rendering")] [SerializeField]
        private NSBRepresentation nsbRenderer;

        /// <summary>
        /// The target FPS.
        /// </summary>
        public float PlaybackFps = 30;

        /// <summary>
        ///     Is playback active
        /// </summary>
        private bool playbackPlayMode = true;

        /// <summary>
        ///     Should playback play forwards or backwards
        /// </summary>
        private bool playbackReverse = false;

        public void TogglePlaybackReverse()
        {
            playbackReverse = !playbackReverse;
        }

        public bool PlaybackPlayMode
        {
            get { return playbackPlayMode; }
            set
            {
                if (this.playbackPlayMode == value) return;
                this.playbackPlayMode = value;
                PlaybackPlayModeChanged?.Invoke(this.playbackPlayMode);
            }
        }

        public void PlaybackPlay()
        {
            this.PlaybackPlayMode = true;
        }

        public void PlaybackPause()
        {
            this.PlaybackPlayMode = false;
        }

        public void PlaybackGotoFrame(float frame)
        {
            JumpToFrame(frame);
        }

        public void PlaybackNextFrame()
        {
            MoveFrames(1);
        }

        public void PlaybackReset()
        {
            JumpToFrame(0);
        }

        public void PlaybackPreviousFrame()
        {
            MoveFrames(-1);
        }

        public void MoveFrames(float offset)
        {
            JumpToFrame(CurrentFrame + offset);
        }

        public void JumpToFrame(float newFrame)
        {
            if (!Ready)
                return;
            if (this.History.Count > 0)
            {
                if (Looping)
                {
                    if (newFrame > History.FinalFrame)
                        newFrame = History.FirstFrame;
                    else if (newFrame < History.FirstFrame) newFrame = History.FinalFrame;
                }
                else
                {
                    if (newFrame > History.FinalFrame)
                        newFrame = History.FinalFrame;
                    else if (newFrame < History.FirstFrame) newFrame = History.FirstFrame;
                }

                SetFrame(newFrame);
            }
        }


        public class TrajectoryPlaybackStartedEventArgs : EventArgs
        {

        }

        public event EventHandler<TrajectoryPlaybackStartedEventArgs> TrajectoryPlaybackStarted;

        public class SimulationStartedEventArgs : EventArgs
        {

        }

        public event EventHandler<SimulationStartedEventArgs> SimulationStarted;


        public delegate void PlaybackPlayModeChangedHandler(bool play);

        public event PlaybackPlayModeChangedHandler PlaybackPlayModeChanged;


        private NSBFrame prevFrame;

        /// <summary>
        /// The frame reader that records simulation frames from the server.
        /// </summary>
        private IFrameProvider frameProvider;

        public bool Ready => frameProvider?.IsFrameHistoryReady ?? false;

        public float Scaling = 1f / 3f;

        private readonly float scalingTarget = 1f / 3f;
        private float scalingVelocity;
        /// <summary>
        /// The atoms selected in the simulation.
        /// </summary>
        public InspectorSelection SelectedAtoms;

        private float selectionLerp;
        private float selectionVelocity;

        [Header("Interaction")] [SerializeField]
        private RaycastFrameAtoms touchPickingAtom;

        /// <summary>
        /// The visible atoms.
        /// </summary>
        public InspectorSelection VisibleAtoms;

        /// <summary>
        /// Name of the current simulation.
        /// </summary>
        public string SimulationName;

        /// <summary>
        /// Triggered when the server sends us a new simulation name that can added to the <see cref="LocalSimulationSelectionPanel"/>."/>
        /// </summary>
        public event EventHandler OnNewSimulationAvailable;

        /// <summary>
        /// The current frame index.
        /// </summary>
        public float CurrentFrame { get; private set; }

        public NSBFrame Frame => InterpolatedFrame;
        public NSBFrame LastFullFrame => this.History[(int) this.CurrentFrame];

        public IFrameHistory<NSBFrame> History => this.frameProvider.History;

        public event Action OnReady;

        public NetworkManager NetworkManager => networkManager == null ? FindObjectOfType<NetworkManager>() : networkManager;
        public NarupaServerConnection CurrentConnection => networkManager.CurrentConnection;

        public void SetFrameProvider(IFrameProvider producer)
        {
            this.frameProvider = producer;
        }

        private bool ready = false;

        private void Awake()
        {
            this.networkManager = FindObjectOfType<NetworkManager>();
            this.networkManager.ServerConnectionInitialised += ServerConnectionInitialised;
            Clear();
        }

        private void ServerConnectionInitialised(object sender, ServerConnectionInitialisedEventArgs args)
        {
            SetFrameProvider(args.ServerConnection as IFrameProvider);
            SimulationStarted?.Invoke(this, new SimulationStartedEventArgs());
        }

        private void Start()
        {
            if (touchPickingAtom == null) touchPickingAtom = GetComponentInChildren<RaycastFrameAtoms>();
            if (camera == null) camera = Camera.main;
        }

        /// <summary>
        /// Enters the given recording.
        /// </summary>
        /// <param name="path">The trajectory file path.</param>
        public void EnterRecording(string path)
        {
            TrajectoryPlaybackStarted?.Invoke(this, new TrajectoryPlaybackStartedEventArgs());
        }

        /// <summary>
        /// Enters the given recording.
        /// </summary>
        /// <param name="bytes">The bytes representing the recording.</param>
        public void EnterRecording(byte[] bytes)
        {
            TrajectoryPlaybackStarted?.Invoke(this, new TrajectoryPlaybackStartedEventArgs());
        }

        private void OnRecordingReady(string path)
        {
            loadingObject.SetActive(false);
            EnterRecording(path);
        }

        private void OnDownloadProgress(float progress)
        {
            loadingImage.fillAmount = progress;
        }

        /// <summary>
        /// Clears the playback.
        /// </summary>
        public void Clear()
        {
            loadingObject.SetActive(false);
            CurrentFrame = 0;
            NSBFrame.Clear(InterpolatedFrame);
            VisibleAtoms.Selection.Clear();
            SelectedAtoms.Selection.Clear();
            boundsRenderer.gameObject.SetActive(false);
        }

        public bool InvertZ
        {
            get
            {
                var narupa = this.frameProvider as NarupaServerConnection;
                if (narupa != null)
                    return narupa.InvertZ;
                return false;
            }
        }

        public Topology Topology =>
            frameProvider is NarupaServerConnection ? (frameProvider as NarupaServerConnection).Topology : null;

        private void Update()
        {
            if (frameProvider == null)
                return;

            if (!frameProvider.IsFrameHistoryReady)
                ready = false;

            if (!ready && frameProvider.IsFrameHistoryReady)
            {
                ready = true;
                SetFrame(History.FirstFrame);
                OnReady?.Invoke();
            }

            float target = SelectedAtoms.Selection.Count > 0 ? 1 : 0;
            selectionLerp = Mathf.SmoothDamp(selectionLerp, target, ref selectionVelocity, 0.25f);

            ConsoleReporter.LogToUnityConsole = false;

            frameProvider.Update();

            if (PlaybackPlayMode)
            {
                MoveFrames((playbackReverse ? -1f : 1f) * Time.deltaTime * PlaybackFps);

                // Ensure that a client does not fall behind
                if (!Looping)
                {
                    var finalFrame = this.History.FinalFrame;
                    if((finalFrame - CurrentFrame) > 2f)
                    {
                        SetFrame(this.History.FinalFrame);
                    }
                }
            }

            Scaling = Mathf.SmoothDamp(Scaling, scalingTarget, ref scalingVelocity, .25f);
        }

        /// <summary>
        /// Sets the playback renderer to the given frame.
        /// </summary>
        /// <param name="frame">Index of frame.</param>
        public void SetFrame(float frame)
        {
            if (History == null)
                return;

            Profiler.BeginSample("Gather Playback Frame");

            lock (History)
            {
                CurrentFrame = Mathf.Clamp(frame,
                    History.FirstFrame,
                    History.FinalFrame);

                if (History.Count == 0) return;

                var prev = Mathf.Clamp(Mathf.FloorToInt(CurrentFrame), History.FirstFrame, History.FinalFrame);
                var next = Mathf.Clamp(Mathf.CeilToInt(CurrentFrame), History.FirstFrame, History.FinalFrame);

                prevFrame = History[prev];
                NextFrame = History[next];
            }

            Profiler.EndSample();

            Refresh();
        }

        /// <summary>
        /// Refreshes the frame.
        /// </summary>
        public void Refresh()
        {
            Profiler.BeginSample("PlaybackRenderer Refresh");

            NSBFrame.Interpolate(prevFrame, NextFrame, InterpolatedFrame, CurrentFrame % 1);

            VisibleAtoms.Selection.SetSourceAtomCount(InterpolatedFrame.AtomCount);
            SelectedAtoms.Selection.SetSourceAtomCount(InterpolatedFrame.AtomCount);

            prevFrame.GetVariable<BoundingBox>(VariableName.SimBoundingBox);

            boundsRenderer.gameObject.SetActive(true);

            // only > 0 during live simulations?
            if (InterpolatedFrame.SelectedAtomIndicies != null
                && InterpolatedFrame.SelectedAtomIndicies.Length > 0)
            {
                SelectedAtoms.Selection.Clear();

                for (var i = 0; i < InterpolatedFrame.SelectedAtomIndicies.Length; ++i)
                    SelectedAtoms.Selection.Add(InterpolatedFrame.SelectedAtomIndicies[i]);
            }
            else if (InterpolatedFrame.SelectedAtomIndicies != null)
            {
                SelectedAtoms.Selection.Clear();
            }

            Profiler.EndSample();
        }

        /// <summary>
        /// Sends play to server.
        /// </summary>
        public void MediaPlay()
        {
            networkManager.CurrentConnection?.SendPlay();
        }

        /// <summary>
        /// Sends pause to server.
        /// </summary>
        public void MediaPause()
        {
            networkManager.CurrentConnection?.SendPause();
        }

        /// <summary>
        /// Sends step to server.
        /// </summary>
        public void MediaStep()
        {
            networkManager.CurrentConnection?.SendStep();
        }

        /// <summary>
        /// Sends reset to server.
        /// </summary>
        public void MediaReset()
        {
            networkManager.CurrentConnection?.SendReset();
        }

        /// <summary>
        /// Picks an atom from screen.
        /// </summary>
        /// <param name="screen">Position on screen.</param>
        /// <param name="id">Atom index.</param>
        /// <returns></returns>
        public bool PickAtom(Vector2 screen, out int id)
        {
            if (!Ready)
            {
                id = 0;
                return false;
            }

            if (touchPickingAtom == null)
            {
                Debug.LogWarning("Missing RaycastFramesAtom, picking atoms from screen is not possible.");
                id = 0;
                return false;
            }

            if (camera == null)
            {
                camera = Camera.main;
                if (camera == null)
                {
                    Debug.LogWarning(
                        "Camera not set in inspector, and there is no main camera. Picking atoms from screen is not possible.");
                    id = 0;
                    return false;
                }
            }

            return touchPickingAtom.RaycastScreenPoint(screen, out id, camera);
        }

        public void SetFPS(float fps)
        {
            this.PlaybackFps = fps;
        }

        public void ResumePlay()
        {
            this.MediaPlay();
        }

        public void StopPlay()
        {
            this.MediaPause();
        }
    }
}
