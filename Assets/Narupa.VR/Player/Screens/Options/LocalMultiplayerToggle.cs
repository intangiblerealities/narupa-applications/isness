﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Multiplayer;
using UnityEngine;

namespace Narupa.VR.Player.Screens.Options

{
    /// <summary>
    /// Toggle for enabling local multiplayer.
    /// </summary>
    [RequireComponent(typeof(OptionUiCycler))]
    public class LocalMultiplayerToggle : MonoBehaviour
    {
        private MultiplayerSteamVrCalibration calibrationParameters;

        [SerializeField] private bool localMultiplayerEnabled = true;

        private OptionUiCycler optionCycler;

        [SerializeField] private string optionLabel = "Local Multiplayer";

        private void Awake()

        {
            optionCycler = GetComponent<OptionUiCycler>();

            optionCycler.OptionChanged += EnableLocalMultiplayer;

            calibrationParameters = FindObjectOfType<MultiplayerSteamVrCalibration>();

            if (PlayerPrefs.HasKey(optionLabel))

            {
                var multInt = PlayerPrefs.GetInt(optionLabel);
                
                localMultiplayerEnabled = multInt == 1;
            }

            EnableLocalMultiplayer(localMultiplayerEnabled);

            var option = localMultiplayerEnabled;

            optionCycler.RegisterBooleanOption(optionLabel);

            optionCycler.SetOption(OptionUiCycler.GetBoolOptionString(option));
        }

        private void Start()

        {
        }

        /// <summary>
        /// Enables local multiplayer mode.
        /// </summary>
        /// <param name="enable">Enable/disable.</param>
        private void EnableLocalMultiplayer(bool enable)

        {
            //calibrationParameters.enabled = enable;

            //calibrationParameters.Activated = enable;

            WorldLocalSpaceTransformer.LocalMultiplayerMode = enable;

            PlayerPrefs.SetInt(optionLabel, enable ? 1 : 0);
        }

        private void EnableLocalMultiplayer(object sender, EventArgs e = null)

        {
            var optionVal = sender as string;

            if (optionVal == null) return;

            var enable = OptionUiCycler.GetBoolFromOptionString(optionVal);

            EnableLocalMultiplayer(enable);

            localMultiplayerEnabled = enable;
        }
    }
}