﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Client;
using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;
using Text = TMPro.TextMeshProUGUI;

namespace Narupa.VR.Player.Screens.Server
{
    /// <summary>
    /// Represents a server in the VR UI.
    /// </summary>
    public class VrServerElement : InstanceView<SimboxConnectionInfo>
    {
        [SerializeField] private Text addressText;

        [SerializeField] private Text descriptionText;

        [SerializeField] private VrServerSelectionPanel panel;

        [SerializeField] private Button selectButton;

        private void Awake()
        {
            selectButton.onClick.AddListener(() => panel.SelectServer(config));
        }

        /// <inheritdoc />
        protected override void Configure()
        {
            descriptionText.text = config.Descriptor;
            addressText.text = string.Format("{0}:{1}", config.Address, config.Port);
        }
    }
}