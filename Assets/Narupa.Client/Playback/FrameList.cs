﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Narupa.Client.Playback;
using NSB.Processing;
using UnityEngine;

/// <summary>
/// Default implementation of an IFrameHistory, simply a standard C# list
/// </summary>
public class FrameList<TFrame> : List<TFrame>, IFrameHistory<TFrame>
{
    public int FirstFrame => 0;
    public int FinalFrame => this.Count - 1;
}