// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Client.Config;
using Narupa.Client.Render.Utility;
using Narupa.Client.Utility.Extensions;
using NSB.MMD.Base;
using NSB.MMD.MeshGeneration;
using NSB.MMD.RenderingTypes;
using NSB.Utility;
using UnityEngine;

namespace Narupa.Client.Render.Renderer
{
    /// <inheritdoc />
    /// <summary>
    ///     Renders edges as billboard sprites
    /// </summary>
    public class EdgeBillboardRenderer : EdgeRenderer
    {
        private readonly ParticleBatchSet batches = new ParticleBatchSet();
        private readonly ParticleBatchRenderer particlesRenderer = new ParticleBatchRenderer();
        private int sharpness, scaling;
        private Mesh bondQuad;

        [Header("Setup")] [Tooltip("The position to orient billboards towards when using particles")]
        public Transform EyeTransform;

        private Vector3 facingTargetPosition;

        public bool ForceParticles;


        private Material linesMaterial;
        private MeshFilter linesMeshFilter;

        private MeshRenderer linesMeshRenderer;

        [Header("Shaders")] [SerializeField] [Tooltip("Shader for rendering a mesh of colored lines as lines")]
        private Shader linesShader;

        private Material linesToBillboardsMaterial;

        [SerializeField]
        [Tooltip("Shader for rendering a mesh of colored lines as billboarded lines (using a geometry shader)")]
        private Shader linesToBillboardsShader;

        private MeshTool mesh;
        private Material quadsMaterial;

        [SerializeField] [Tooltip("Shader for rendering billboarded quads via a Unity particle system")]
        private Shader quadsShader;

        public Config Settings = new Config();
        public Texture2D Texture;

        private void Awake()
        {
            AddMesh(ref linesMeshFilter, ref linesMeshRenderer);

            mesh = new MeshTool(new Mesh(), MeshTopology.Lines);
            linesMeshFilter.sharedMesh = mesh.mesh;

            linesMaterial = new Material(linesShader);
            linesToBillboardsMaterial = new Material(linesToBillboardsShader);
            quadsMaterial = new Material(quadsShader);

            scaling = Shader.PropertyToID("_Scaling");
            sharpness = Shader.PropertyToID("_Sharpness");

            bondQuad = new Mesh();
            MeshGeneration.GenerateBondQuad(bondQuad);
            particlesRenderer.Initialise(transform);
            particlesRenderer.SetMesh(bondQuad);
            particlesRenderer.SetMaterial(quadsMaterial);
            batches.SetBatchSize(bondQuad);

            EyeTransform = Camera.main?.transform;
        }

        public override void Refresh()
        {
            var useMesh = true;

            if (Settings.Width.Value > 0 && linesToBillboardsMaterial.shader.isSupported && !ForceParticles)
            {
                linesMeshRenderer.enabled = true;
                linesMeshRenderer.sharedMaterial = linesToBillboardsMaterial;
                // TODO: use model scaling in the geometry shader...
                linesToBillboardsMaterial.SetFloat(scaling, Settings.Width.Value * 0.5f * transform.lossyScale.x);
                linesToBillboardsMaterial.SetFloat(sharpness, Settings.Sharpness.Value);
            }
            else if (Settings.Width.Value > 0)
            {
                useMesh = false;
                linesMeshRenderer.enabled = false;
                quadsMaterial.SetTexture("_Palette", null); // todo: readd palette
            }
            else
            {
                linesMeshRenderer.enabled = true;
                linesMeshRenderer.sharedMaterial = linesMaterial;
            }

            if (useMesh)
            {
                particlesRenderer.Clear();

                MeshUtility.GenerateBondLines(mesh, bonds, bondColor, atomPosition);
            }
            else
            {
                quadsMaterial.SetKeyword("GRADIENT", atomPaletteIndex.Count > 0);
                quadsMaterial.SetTexture("_MainTex", Texture);
                quadsMaterial.SetFloat(sharpness, Settings.Sharpness.Value);
                facingTargetPosition = transform.InverseTransformPoint(EyeTransform.transform.position);

                batches.GenerateParticles(this, bonds.Count, GenerateParticle);
                particlesRenderer.RenderBatchSet(batches);
            }
        }

        private static void GenerateParticle(EdgeBillboardRenderer data,
            int index,
            ref ParticleSystem.Particle particle)
        {
            var bond = data.bonds[index];
            Color color = data.Settings.UseOverrideColor ? (Color) data.Settings.OverrideColor : data.GetEdgeColor(index, bond);

            particle.startColor = color;

            var posA = data.atomPosition[bond.A];
            var posB = data.atomPosition[bond.B];
            var dir = FasterMath.Sub(posB, posA);

            MMDTools.BillboardBondParticle(ref particle, posA, posB, data.facingTargetPosition);

            Vector3 scale;
            scale.y = data.Settings.Width.Value;
            scale.z = dir.magnitude;
            scale.x = data.Settings.Width.Value;

            particle.startSize3D = scale;
        }

        [Serializable]
        public class Config : BaseConfig
        {
            public FloatParameter Sharpness = 1f;
            public FloatParameter Width = 0.025f;
            public BoolParameter UseOverrideColor = false;
            public ColorParameter OverrideColor = Color.black;

            protected override void SetupParameters()
            {
                Width.SetRange(0, .1f);
                Sharpness.SetRange(0, 1);
            }
        }
    }
}