// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

namespace Narupa.Client.Render.Property
{
    public class ValueProperty<T>
    {
        private T value;

        public void Set(T val)
        {
            value = val;
        }

        public static implicit operator T(ValueProperty<T> val)
        {
            return val.value;
        }
    }
}