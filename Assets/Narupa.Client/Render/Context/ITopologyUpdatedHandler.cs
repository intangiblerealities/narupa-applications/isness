// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine.EventSystems;

namespace Narupa.Client.Render.Context
{
    /// <summary>
    ///     Interface for components which should act when the topology is updated
    /// </summary>
    public interface ITopologyUpdatedHandler : IEventSystemHandler
    {
        /// <summary>
        ///     Called when the topology of the system is changed
        /// </summary>
        void TopologyUpdated();
    }
}