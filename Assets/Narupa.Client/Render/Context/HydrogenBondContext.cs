// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Nano.Client;
using Nano.Science;
using Narupa.Client.Render.Property;
using NSB.Processing;
using NSB.Simbox.Topology;
using UnityEngine;

namespace Narupa.Client.Render.Context
{
    /// <inheritdoc />
    /// <summary>
    ///     Custom frame render context which overrides bonds with a set of calculated bonds
    /// </summary>
    public class HydrogenBondContext : FrameRenderContext
    {
        private readonly Dictionary<int, Residue> residueMap = new Dictionary<int, Residue>();


        [SerializeField] private float angleCutoff = 20f;

        [RenderProperty("EDGES")] private IndexedProperty<BondPair> bonds;

        [RenderProperty("EDGE.HBOND")] private IndexedProperty<int> bondType;

        [SerializeField] private float distanceCutoff = 3f;

        protected override void OnTopologyChanged()
        {
            CalculateHydrogenBonds(Frame, Frame.Topology);
        }

        protected override void OnRefresh()
        {
            CalculateBonds(Frame);
        }

        private void CalculateBonds(NSBFrame frame)
        {
            var calculatedBonds = new List<BondPair>();
            var types = new List<int>();
            foreach (var res1 in residueMap.Values)
            foreach (var res2 in residueMap.Values)
            {
                var type = Mathf.Abs(res2.Index - res1.Index);
                foreach (var donor in res1.Donors)
                foreach (var acceptor in res2.Acceptors)
                    if (CheckHydrogenBond(donor, acceptor, frame, calculatedBonds))
                        types.Add(type);
            }

            bonds.Set(calculatedBonds);
            bondType.Set(types);
        }

        private bool CheckHydrogenBond(IReadOnlyList<int> dh, int a, NSBFrame frame, ICollection<BondPair> bondPairs)
        {
            var donor = frame.AtomPositions[dh[0]];
            var hydrogen = frame.AtomPositions[dh[1]];
            var acceptor = frame.AtomPositions[a];
            var d = Vector3.Distance(donor, acceptor);
            if (d > distanceCutoff)
                return false;
            var ang = Vector3.Angle(acceptor - hydrogen, donor - hydrogen);
            if (ang < 180f - angleCutoff)
                return false;
            bondPairs.Add(new BondPair(dh[1], a));
            return true;
        }

        private void CalculateHydrogenBonds(NSBFrame frame, Topology topology)
        {
            foreach (var atom in topology.AtomsByAbsoluteIndex)
            {
                var res = atom.Parent.Id;
                var residue = GetResidue(res);
                residue.Indices.Add(atom.Index);
                var element = (Element) frame.AtomTypes[atom.Index];
                switch (element)
                {
                    case Element.Oxygen:
                        residue.Oxygens.Add(atom.Index);
                        break;
                    case Element.Hydrogen:
                        residue.Hydrogens.Add(atom.Index);
                        break;
                    case Element.Nitrogen:
                        residue.Nitrogens.Add(atom.Index);
                        break;
                }
            }

            foreach (var residue in residueMap.Values)
            {
                foreach (var nitrogen in residue.Nitrogens)
                foreach (var hydrogen in residue.Hydrogens)
                    if (Bonded(frame, nitrogen, hydrogen))
                        residue.Donors.Add(new[] {nitrogen, hydrogen});
                foreach (var oxygen in residue.Oxygens)
                foreach (var hydrogen in residue.Hydrogens)
                    if (Bonded(frame, oxygen, hydrogen))
                        residue.Donors.Add(new[] {oxygen, hydrogen});
                residue.Acceptors.AddRange(residue.Nitrogens);
                residue.Acceptors.AddRange(residue.Oxygens);
            }
        }

        private static bool Bonded(NSBFrame frame, int index1, int index2)
        {
            foreach (var bond in frame.BondPairs)
            {
                if (bond.A == index1 && bond.B == index2)
                    return true;
                if (bond.A == index2 && bond.B == index1)
                    return true;
            }

            return false;
        }

        private Residue GetResidue(int res)
        {
            if (residueMap.ContainsKey(res))
                return residueMap[res];
            var residue = new Residue {Index = res};
            residueMap.Add(res, residue);
            return residue;
        }

        private class Residue
        {
            public readonly List<int> Acceptors = new List<int>();
            public readonly List<int[]> Donors = new List<int[]>();
            public readonly List<int> Hydrogens = new List<int>();
            public readonly List<int> Indices = new List<int>();
            public readonly List<int> Nitrogens = new List<int>();
            public readonly List<int> Oxygens = new List<int>();
            public int Index;
        }
    }
}