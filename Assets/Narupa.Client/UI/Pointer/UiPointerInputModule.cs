﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;

namespace Narupa.Client.UI.Pointer
{
    /// <summary>
    ///     An implementation of an InputModule that uses the SteamVR input system and can use a pointer in world space as
    ///     a raycaster to interact with the standard unity UI.
    ///     Based on the gaze based input module at https://ritchielozada.com/2016/01/01/part-8-creating-a-gaze-based-input-module-for-unity/
    /// </summary>
    public class UiPointerInputModule : PointerInputModule
    {
        private readonly MouseState mouseState = new MouseState();

        public bool forceModuleActive = false;

        [Header("SteamVR Input")] public SteamVR_Action_Boolean actionLeftClick;
        public SteamVR_Action_Boolean actionRightClick;
        public SteamVR_Input_Sources inputSource;

        [Header("Pointer State")] public bool isPointerActive = false;
        public Vector3 pointerTarget = Vector3.zero;

        private List<IUiPointerOverride> pointerOverrides = new List<IUiPointerOverride>();

        public void AddPointerOverride(IUiPointerOverride @override)
        {
            pointerOverrides.Add(@override);
        }

        protected PointerEventData.FramePressState GetLeftClickState()
        {
            return GetButtonState(actionLeftClick, inputSource);
        }

        protected PointerEventData.FramePressState GetRightClickState()
        {
            return GetButtonState(actionRightClick, inputSource);
        }

        protected static PointerEventData.FramePressState GetButtonState(SteamVR_Action_Boolean action,
            SteamVR_Input_Sources inputSource)
        {
            var pressed = action.GetStateDown(inputSource);
            var released = action.GetStateUp(inputSource);
            if (pressed && released)
                return PointerEventData.FramePressState.PressedAndReleased;
            if (pressed)
                return PointerEventData.FramePressState.Pressed;
            if (released)
                return PointerEventData.FramePressState.Released;
            return PointerEventData.FramePressState.NotChanged;
        }

        public override void Process()
        {
            var pointerData = CreatePointerEvent(0);

            var leftPressData = pointerData.GetButtonState(PointerEventData.InputButton.Left).eventData;

            var raycast = leftPressData.buttonData.pointerCurrentRaycast;
            this.isPointerActive = raycast.gameObject != null;
            this.pointerTarget = raycast.worldPosition;

            foreach (var @override in this.pointerOverrides)
                this.isPointerActive = this.isPointerActive && @override.IsUiPointerActive;

            if (this.isPointerActive)
            {
                ProcessPress(leftPressData.buttonData, leftPressData.PressedThisFrame(),
                    leftPressData.ReleasedThisFrame());
                ProcessMove(leftPressData.buttonData);

                if (actionLeftClick.GetState(inputSource))
                {
                    ProcessDrag(leftPressData.buttonData);
                }
            }
        }

        private void ProcessPress(PointerEventData pointerEvent, bool pressed, bool released)
        {
            var currentOverGo = pointerEvent.pointerCurrentRaycast.gameObject;

            if (pressed)
            {
                pointerEvent.eligibleForClick = true;
                pointerEvent.delta = Vector2.zero;
                pointerEvent.dragging = false;
                pointerEvent.useDragThreshold = true;
                pointerEvent.pressPosition = pointerEvent.position;
                pointerEvent.pointerPressRaycast = pointerEvent.pointerCurrentRaycast;

                DeselectIfSelectionChanged(currentOverGo, pointerEvent);

                if (pointerEvent.pointerEnter != currentOverGo)
                {
                    HandlePointerExitAndEnter(pointerEvent, currentOverGo);
                    pointerEvent.pointerEnter = currentOverGo;
                }

                var newPressed =
                    ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.pointerDownHandler);

                if (newPressed == null)
                {
                    newPressed = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGo);
                }

                float time = Time.unscaledTime;

                if (newPressed == pointerEvent.lastPress)
                {
                    var diffTime = time - pointerEvent.clickTime;
                    if (diffTime < 0.3f)
                        ++pointerEvent.clickCount;
                    else
                        pointerEvent.clickCount = 1;

                    pointerEvent.clickTime = time;
                }
                else
                {
                    pointerEvent.clickCount = 1;
                }

                pointerEvent.pointerPress = newPressed;
                pointerEvent.rawPointerPress = currentOverGo;

                pointerEvent.clickTime = time;

                pointerEvent.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(currentOverGo);

                if (pointerEvent.pointerDrag != null)
                    ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent,
                        ExecuteEvents.initializePotentialDrag);

                if (pointerEvent.eligibleForClick)
                {
                    ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerClickHandler);
                }
            }


            if (released)
            {
                ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerUpHandler);

                if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
                {
                    ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.dropHandler);
                }

                pointerEvent.eligibleForClick = false;
                pointerEvent.pointerPress = null;
                pointerEvent.rawPointerPress = null;

                if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
                    ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);

                pointerEvent.dragging = false;
                pointerEvent.pointerDrag = null;

                if (pointerEvent.pointerDrag != null)
                    ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);

                pointerEvent.pointerDrag = null;

                ExecuteEvents.ExecuteHierarchy(pointerEvent.pointerEnter, pointerEvent,
                    ExecuteEvents.pointerExitHandler);
                pointerEvent.pointerEnter = null;
            }
        }

        public Transform currentPointer;

        protected MouseState CreatePointerEvent(int id)
        {
            PointerEventData leftData;
            GetPointerData(kMouseLeftId, out leftData, true);
            Vector2 pos = new Vector2(Screen.width / 2f, Screen.height / 2f);

            leftData.Reset();
            leftData.delta = Vector2.zero;
            leftData.position = pos;
            leftData.scrollDelta = Vector2.zero;
            leftData.button = PointerEventData.InputButton.Left;

            // Override the raycast such that it uses the currentPointer's position and facing. This is set automatically
            // by the VRPointer MonoBehaviour
            var raycastNew = leftData.pointerCurrentRaycast;
            if (currentPointer != null)
            {
                raycastNew.worldPosition = currentPointer.position;
                raycastNew.worldNormal = currentPointer.forward;
            }

            leftData.pointerCurrentRaycast = raycastNew;

            eventSystem.RaycastAll(leftData, m_RaycastResultCache);
            var raycast = FindFirstRaycast(m_RaycastResultCache);

            leftData.pointerCurrentRaycast = raycast;
            m_RaycastResultCache.Clear();

            PointerEventData rightData;
            GetPointerData(kMouseRightId, out rightData, true);
            CopyFromTo(leftData, rightData);
            rightData.button = PointerEventData.InputButton.Right;

            mouseState.SetButtonState(
                PointerEventData.InputButton.Left,
                GetLeftClickState(),
                leftData);
            mouseState.SetButtonState(
                PointerEventData.InputButton.Right,
                GetRightClickState(),
                rightData);

            return mouseState;
        }

        public override bool IsModuleSupported()
        {
            return forceModuleActive;
        }

        public override bool ShouldActivateModule()
        {
            if (!base.ShouldActivateModule())
                return false;

            return forceModuleActive;
        }

        public override void DeactivateModule()
        {
            base.DeactivateModule();
            ClearSelection();
        }

        public override void ActivateModule()
        {
            base.ActivateModule();
        }
    }
}