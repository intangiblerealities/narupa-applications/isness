﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.Client.Network
{
	
	/// <summary>
	/// General interface that wraps receiving messages from the server
	/// </summary>
	public interface IMessageReceiver
	{

		/// <summary>
		/// Subscribe to a message which matches address
		/// </summary>
		void SubscribeMessage(string address, Action<string, object[]> callback);

		/// <summary>
		/// Unsubscribe to a message which matches address
		/// </summary>
		void UnsubscribeMessage(string address, Action<string, object[]> callback);

	}
}
