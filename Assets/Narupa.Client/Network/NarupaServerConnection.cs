﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Nano;
using Nano.Client;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using Narupa.Client.FileSystem;
using Narupa.Client.Network.Event;
using Narupa.VR.Multiplayer;
using NSB.Processing;
using NSB.Simbox;
using NSB.Simbox.Debugging;
using NSB.Simbox.Topology;
using Rug.Osc;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;

namespace Narupa.Client.Network
{
    /// <summary>
    /// Represents a connection to a Narupa server, managing all communication between the server and client
    /// </summary>
    public partial class NarupaServerConnection : IServerConnection, IFrameProvider, IMessageReceiver, IMessageSender
    {
        public enum ServerConnectionState
        {
            Closed,
            Connecting,
            Connected
        }

        public SimboxConnectionInfo ServerInfo;
        
        private ServerConnectionState connectionState;

        public ServerConnectionState Connection => connectionState;

        private ConnectionType connectionType;
        private string sessionToken;
        
        public NarupaServerConnection(SimboxConnectionInfo info, ConnectionType connectionType = ConnectionType.Tcp,
            string sessionToken = null)
        {
            ServerInfo = info;
            this.connectionType = connectionType;
            this.sessionToken = sessionToken;
        }

        public void Initialise()
        {
            ClearConnection();
            BeginConnection(ServerInfo, connectionType, sessionToken);
        }

        private void BeginConnection(SimboxConnectionInfo info, ConnectionType connectionType, string sessionToken)
        {
           connectionState = ServerConnectionState.Connecting;

            transport = new TransportClient(info.ConnectionString, sessionToken, statistics, reporter);
            client = new NSB.Simbox.Client(transport, statistics, reporter);

            ClientCreated?.Invoke(this, new ClientCreatedEventArgs(this, client));
            
            AttachClientToStreams();

            transport.Connected += (s, a) => client.Start();
            transport.AttachReceiver(client);
            transport.Start();
        }

        private void AttachClientToStreams()
        {
            frameStream.Attach(client);
            atomPositions.Attach(client);
            atomTypes.Attach(client);
            atomVelocities.Attach(client);
            atomKineticEnergies.Attach(client);
            atomBondPairs.Attach(client);
            selectedAtoms.Attach(client);
            visibleAtomMap.Attach(client);
            interactionForceInfo.Attach(client);
            VRPositions.Attach(client);
            topologyUpdateCount.Attach(client);
            frameStream.Updated += OnFrameStreamUpdated;

            client.SubscribeMessage("//*", OnNetworkMessage);
        }

        private void RemoveClientsFromStreams()
        {
            frameStream.Detach(client);
            atomPositions.Detach(client);
            atomTypes.Detach(client);
            atomVelocities.Detach(client);
            atomKineticEnergies.Detach(client);
            atomBondPairs.Detach(client);
            visibleAtomMap.Detach(client);
            interactionForceInfo.Detach(client);
            selectedAtoms.Detach(client);
            VRPositions.Detach(client);
            topologyUpdateCount.Detach(client);
            frameStream.Updated -= OnFrameStreamUpdated;

            client.UnsubscribeMessage("//*", OnNetworkMessage);
        }

        public event EventHandler<ClientCreatedEventArgs> ClientCreated;  
        public event Action ConnectionSucceeded;

        public event Action ConnectionFailed;

        public event Action ConnectionLost;
        
        public event Action ConnectionReset;

        public event Action FramesReady;

        public bool Connected => connectionState == ServerConnectionState.Connected;
        private bool Connecting => connectionState == ServerConnectionState.Connecting;

        public event Action ReconnectSignalReceived;

        private void OnConnectionSucceeded()
        {
            SubscribeToMessages(client);
            ConnectionSucceeded?.Invoke();
        }

        private void OnConnectionFailed()
        {
            ConnectionFailed?.Invoke();
        }

        private void OnConnectionLost()
        {
            ConnectionLost?.Invoke();
        }

        private void OnConnectionReset()
        {
            ConnectionReset?.Invoke();
        }
        
        protected TransportClient transport;

        public NSB.Simbox.Client client;

        private static PacketStatistics statistics = new PacketStatistics();

        private IReporter reporter = new ConsoleReporter();


        //address for multiplayer scale change
        private readonly string boxScaleChangeAddress = "/server/setMultiplayerBoxScale";

        private void SubscribeToMessages(NSB.Simbox.Client client)
        {
            client.SubscribeMessage(boxScaleChangeAddress, OnBoxScaleChange);
            client.SubscribeMessage("/reconnect", OnReconnectReceived);
        }

        private void OnReconnectReceived(string address, params object[] parameters)
        {
            Debug.Log("Reconnect");
            ReconnectSignalReceived?.Invoke();
        }

        private void OnBoxScaleChange(string address, params object[] parameters)
        {
            try
            {
                //var scale = (float) parameters[0];
                //MultiplayerSteamVrCalibration.MultiplayerScale = new Vector3(scale, scale, scale);
            }
            catch
            {
                //Debug.Log("Invalid box scale received!");
            }
        }


        //TODO locker for topology, needs streams v2 to be good. 
        private object locker;
        public Topology Topology;


        public Action<string> SimulationNameRecieved;

        public Action<string> SimulationAvailable;


        /// <summary>
        /// Add an atom of given id to a named group.
        /// </summary>
        private void AddAtomToGroup(string name, ushort atom)
        {
            List<ushort> atoms;

            if (!Groups.TryGetValue(name, out atoms))
            {
                atoms = new List<ushort>();
                Groups[name] = atoms;
            }

            atoms.Add(atom);
        }

        /// <summary>
        /// When the frame stream is updated, that means the previous frame must
        /// be complete, so we should copy all the existing data into a new frame
        /// in the history
        /// </summary>
        private void OnFrameStreamUpdated(ISimboxSyncBuffer<long> buffer)
        {
            RecordFrame();
        }


        public bool InvertZ = true;

        public Dictionary<string, List<ushort>> Groups = new Dictionary<string, List<ushort>>();

        private List<BondPair> filteredBonds = new List<BondPair>();
        /// <summary>
        /// Copy all stream data into a new frame (data that hasn't changed will
        /// reuse the arrays from the last frame)
        /// </summary>
        public void RecordFrame()
        {
            var invert = InvertZ ? Vector3ArrayInvertZ : (Action<Vector3[]>) null;

            var frame = new NSBFrame
            {
                VisibleAtomMap = GetDataCopy(visibleAtomMap),

                FrameNumber = frameStream.Data[0],
                AtomCount = atomTypes.Count,
                BondCount = atomBondPairs.Count,

                AtomTypes = GetDataCopy(atomTypes),
                AtomPositions = GetDataCopy(atomPositions),
                AtomVelocities = GetDataCopy(atomVelocities),
                AtomVelocitiesValid = atomVelocities.Count == atomPositions.Count,
                AtomKineticEnergies = GetDataCopy(atomKineticEnergies),
                AtomKineticEnergiesValid = atomKineticEnergies.Count == atomPositions.Count,
                BondPairs = GetDataCopy(atomBondPairs),

                CollidedAtomIndicies = GetDataCopy(atomCollisions),
                SelectedAtomIndicies = GetDataCopy(selectedAtoms),
                InteractionForceInfo = GetDataCopy(interactionForceInfo),

                TopologyUpdateCount = topologyUpdateCount.Data[0],
                Topology = Topology,
                Variables = GetVariableValues(),
                VRInteractions = GetDataCopy(VRPositions),
            };

            //hack to deal with bond lengths.
            frame.BondPairs = SanityCheckBonds(frame.BondPairs, frame.AtomPositions);
            frame.BondCount = frame.BondPairs.Length;
            
            invert(frame.AtomPositions);

            frame.InteractionForceInfo = GetDataCopy(interactionForceInfo);

            AddFrameToHistory(frame);

            this.hasRecievedFrames = true;
        }

        private BondPair[] SanityCheckBonds(BondPair[] bonds, Vector3[] atomPositions)
        {
            filteredBonds.Clear();
            filteredBonds.Capacity = bonds.Length;
            float cutoff = 0.3f * 0.3f;
            foreach (var bond in bonds)
            {
                if(Vector3.SqrMagnitude(atomPositions[bond.A] - atomPositions[bond.B]) < cutoff)
                    filteredBonds.Add(bond);
            }

            return filteredBonds.ToArray();
        }
        private bool hasRecievedFrames = false;

        public void Vector3ArrayInvertZ(Vector3[] vectors)
        {
            for (int i = 0; i < vectors.Length; ++i)
            {
                vectors[i].z *= -1;
            }
        }

        /// <summary>
        /// Return the current data from a buffer - if that data hasn't changed
        /// since the last copy we got, return a reference to that instead of
        /// making a new one
        /// </summary>
        private T[] GetDataCopy<T>(ISimboxSyncBuffer<T> buffer,
            Action<T[]> postprocess = null)
        {
            T[] copy;

            // no data to copy yet
            if (buffer.Data == null)
            {
                copy = Enumerable.Empty<T>().ToArray();

                bufferCopies[buffer] = copy;
            }
            // data changed since previous copy or there is no previous copy
            else if (buffer.IsDirty || !bufferCopies.ContainsKey(buffer))
            {
                buffer.IsDirty = false;

                copy = buffer.Data.Take(buffer.Count).ToArray();

                bufferCopies[buffer] = copy;
            }
            // use previous copy
            else
            {
                copy = (T[]) bufferCopies[buffer];
            }

            return copy;
        }


        private Dictionary<object, object> bufferCopies = new Dictionary<object, object>();

        public Dictionary<VariableName, object> GetVariableValues()
        {
            var values = new Dictionary<VariableName, object>();

            foreach (var pair in client.VariablePortal.Variables)
            {
                values[pair.Key] = pair.Value.GetValue();
            }

            return values;
        }

        public bool HasVariable(VariableName name)
        {
            return client.VariablePortal.Variables.ContainsKey(name);
        }

        public IVariable GetVariable(VariableName name)
        {
            return client.VariablePortal.Variables[name];
        }

        public IVariable<T> GetVariable<T>(VariableName name)
        {
            return client.VariablePortal.Variables[name] as IVariable<T>;
        }

        public IVariable<T> GetVariable<T>(NSBVariable<T> variable)
        {
            return GetVariable<T>(variable.VariableName);
        }

        // does the simulation support the core variables?
        public bool Variables => HasVariable(VariableName.Temperature)
                                 && HasVariable(VariableName.SimBoundingBox)
                                 && HasVariable(VariableName.Pressure);

        public bool TransportIsConnected => transport != null && transport.State == ConnectionState.Connected;


        public bool TransportIsConnecting => transport != null && (transport.State == ConnectionState.Connecting
                                             || transport.State == ConnectionState.Handshake);

        public bool TransportIsDisconnected => transport == null || transport.State == ConnectionState.Disconnecting
                                               || transport.State == ConnectionState.Disconnected
                                               || transport.State == ConnectionState.ForcedDisconnection
                                               || transport.State == ConnectionState.NotConnected;

        public ConnectionState TransportConnection => transport.State;

        public void Update()
        {
            if (Connecting && TransportIsConnected)
            {
                connectionState = ServerConnectionState.Connected;
                OnConnectionSucceeded();
            }
            else if (Connecting && !TransportIsConnecting)
            {
                ClearConnection();
                OnConnectionFailed();
            }
            else if (TransportIsDisconnected)
            {
                ClearConnection();
                OnConnectionLost();
            }

            if (historyDirty)
            {
                historyDirty = false;
                FrameHistoryUpdated?.Invoke();
            }
            
            if (this.Connected)
            {
                SendInputFields();

                if (!this.Ready && hasRecievedFrames)
                {
                    Ready = true;
                    FramesReady?.Invoke();
                    this.SendPlay();
                }
            }
        }

        private void SendInputFields()
        {
            if (client == null)
                return;

            SendInteractions();
            SendVrDevicePositions();
        }

        public bool Ready { get; private set; }

        public void ClearConnection()
        {
            connectionState = ServerConnectionState.Closed;
            Ready = false;

            bufferCopies.Clear();
            Topology = new Topology();
            Groups = new Dictionary<string, List<ushort>>();

            if (client != null)
            {
                client.Dispose();
                RemoveClientsFromStreams();
                client = null;
            }

            if (transport != null)
            {
                transport.Dispose();
                transport = null;
            }

            lock (history)
            {
                history.Reset();
            }
            
            OnConnectionReset();
        }

        public List<Vector3> Inputs = new List<Vector3>();


        /// <summary>
        /// The atoms associated with the Interactions list. Note that the order of the lists needs to be maintained.
        /// TODO Refactor once better streams are available.
        /// </summary>
        public List<IList<int>> AtomsToSelect = new List<IList<int>>();


        private void OnNewSimulationAvailable(string simulationName)
        {
            if (simulationName == null) return;
            SimulationAvailable?.Invoke(simulationName);
            fileSystem.AddFile(simulationName);
        }

        private DiscoverableFileSystem fileSystem = new DiscoverableFileSystem();


        public IEnumerable<string> AvailableSimulations => fileSystem.GetFiles("");

        public void OpenSimulation(string path)
        {
            client.SendSimulationPath(path);
        }

    }
}