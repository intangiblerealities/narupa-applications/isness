// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Nano.Client;

namespace Narupa.Client.Network.Event
{
    public class ServerConnectionInfoEventArgs : EventArgs
    {
        public ServerConnectionInfoEventArgs(SimboxConnectionInfo connection)
        {
            ServerConnectionInfo = connection;
        }

        public SimboxConnectionInfo ServerConnectionInfo { get; }
    }
}