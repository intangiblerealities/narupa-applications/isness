﻿using System;
using NSB.MMD.Base;
using NSB.MMD.Editor;
using UnityEditor;
using UnityEngine;

namespace Narupa.Client.Config.Editor
{
    public abstract class ParameterDrawer<T> : PropertyDrawer where T : IEquatable<T>
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            var param = EditorHelper.GetTargetObjectOfProperty(property) as Parameter<T>;
            if (param == null)
                return;

            var value = property.FindPropertyRelative("value");
            var prev = param.Value;

            EditorGUI.indentLevel = 0;

            if (typeof(T) == typeof(int))
            {
                var range = param as IntParameter;
                if (range != null)
                    EditorGUI.IntSlider(position, value, range.Min, range.Max,
                        new GUIContent(property.displayName));
            }
            else if (typeof(T) == typeof(float))
            {
                var range = param as FloatParameter;
                if (range != null)
                    EditorGUI.Slider(position, value, range.Min, range.Max,
                        new GUIContent(property.displayName));
            }
            else
            {
                EditorGUI.PropertyField(position, value, new GUIContent(property.displayName));
            }

            EditorGUI.EndProperty();

            property.serializedObject.ApplyModifiedProperties();

            if (!param.Value.Equals(prev)) param.TriggerValueChanged(prev);
        }
    }

    [CustomPropertyDrawer(typeof(IntParameter))]
    public class IntParameterDrawer : ParameterDrawer<int>
    {
    }

    [CustomPropertyDrawer(typeof(FloatParameter))]
    public class FloatParameterDrawer : ParameterDrawer<float>
    {
    }

    [CustomPropertyDrawer(typeof(BoolParameter))]
    public class BoolParameterDrawer : ParameterDrawer<bool>
    {
    }
    
    [CustomPropertyDrawer(typeof(ColorParameter))]
    public class ColorParameterDrawer : ParameterDrawer<Color>
    {
    }
}