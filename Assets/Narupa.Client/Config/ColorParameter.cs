using System;
using UnityEngine;

namespace Narupa.Client.Config
{
    [Serializable]
    public class ColorParameter : Parameter<Color>
    {
        public ColorParameter(string name, Color value) : base(name, value)
        {
        }
        
        public static implicit operator ColorParameter(Color d)
        {
            return new ColorParameter(null, d);
        }
    }
}