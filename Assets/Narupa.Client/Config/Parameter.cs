using System;
using UnityEngine;

namespace Narupa.Client.Config
{
    /// <summary>
    /// Represents a general 'parameter', with an event which is fired when the Parameter is modified.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public abstract class Parameter<T> : ICopyable<Parameter<T>>
    {
        protected Parameter()
        {
        }

        protected Parameter(string name, T value)
        {
            this.name = name;
            this.value = value;
        }

        [SerializeField] private string name;

        /// <summary>
        /// The name of the parameter
        /// </summary>
        public string Name => name;

        [SerializeField] private T value;

        public T Value
        {
            get { return value; }
            set
            {
                if (!value.Equals(this.value))
                    TriggerValueChanged(this.value);
                this.value = value;
            }
        }

        public virtual void CopyFrom(Parameter<T> parameter)
        {
            name = parameter.Name;
            value = parameter.Value;
        }
        
        public static implicit operator T(Parameter<T> d)
        {
            return d.Value;
        }

        public event EventHandler<ParameterChangedEventArgs<T>> ValueChanged;

        public void TriggerValueChanged(T oldVal)
        {
            ValueChanged?.Invoke(this, new ParameterChangedEventArgs<T>(oldVal, Value));
        }
    }

    public class ParameterChangedEventArgs<T>
    {
        public T NewValue { get; }
        public T OldValue { get; }

        public ParameterChangedEventArgs(T oldValue, T newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}