﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Examples.Player.Control;
using NSB.Examples.Utility;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;

// update sphere colliders on-demand so we can raycast against atoms (for e.g
// seeing which atom a user clicked on the screen)
namespace NSB.Interaction
{
    public class RaycastFrameAtoms : MonoBehaviour
    {
        [SerializeField]
        private AtomColliderInfo colliderPrefab;
        [SerializeField]
        private Transform colliderParent;
        [Tooltip("Root object for simbox scaling")]
        [SerializeField]
        private GameObject simboxSceneScaling;

        [SerializeField]
        private FrameSource frameSource;

        private IndexedPool<AtomColliderInfo> colliders;
        public NSBStyle Style;

        [Header("Config")]
        [SerializeField, UnityLayer]
        private int layer;
        [Range(0, 1)]
        public float Scaling = 1;
        public bool UseUnityColliders = true;
        public bool LazyUpdates;

        public NSBFrame Frame;
        private bool dirty;

        private void Awake()
        {
            colliders = new IndexedPool<AtomColliderInfo>(colliderPrefab, colliderParent);
            if (simboxSceneScaling == null)
                GameObject.Find ("Scene Scaling");
        }

        private void Update()
        {
            Frame = frameSource.Component.Frame;
        
            if (!LazyUpdates)
            {
                Rebuild();
            }
            else
            {
                dirty = true;
            }
        }

        public void Rebuild()
        {
            dirty = false;

            // TODO: this might be better replaced with ParticleBatches.UpdateBatches
            colliders.SetActive(UseUnityColliders ? Frame.AtomCount : 0);

            if (UseUnityColliders)
            {
                var scaling = FasterMath.Uniform(Scaling);

                colliders.MapActive((i, collider) =>
                {
                    int element = Frame.AtomTypes[i];

                    collider.transform.localPosition = Frame.AtomPositions[i];
                    collider.transform.localScale = scaling * ExampleMolecularStyle.ElementRadiuses[element];
                    collider.AtomID = i;
                });
            }
        }

        public bool Raycast(Ray ray, out RaycastHit hit)
        {
            return Physics.Raycast(ray, out hit, layer);
        }

        public bool Raycast(Ray ray, out int id)
        {
            if (LazyUpdates && dirty)
            {
                Rebuild();
            }

            if (UseUnityColliders)
            {
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, layer))
                {
                    var info = hit.collider.GetComponent<AtomColliderInfo>();

                    if (info != null)
                    {
                        id = info.AtomID;
                        return true;
                    }
                    else
                    {
                        id = 0;
                        return false;
                    }
                }
                else
                {
                    id = 0;
                    return false;
                }
            }
            else
            {
                Sphere sphere;
                float t;
                float tmin = Mathf.Infinity;
                bool hit = false;
                id = 0;

                Vector3 scenePosition = Vector3.zero;
                float sceneScaling = 1f; 
                if (simboxSceneScaling != null) 
                {
                    sceneScaling = simboxSceneScaling.transform.localScale[0];
                    scenePosition = simboxSceneScaling.transform.localPosition;
                }

                for (int i = 0; i < Frame.AtomCount; ++i)
                {
                    sphere.position = sceneScaling * Frame.AtomPositions[i] + simboxSceneScaling.transform.localPosition;
                    sphere.radius = ExampleMolecularStyle.ElementRadiuses[Frame.AtomTypes[i]] * Scaling;

                    if (Style != null)
                    {
                        sphere.radius = Style.AtomRadiuses[i] * Scaling;
                    }

                    if (sphere.Raycast(ray, out t) && t < tmin && t > 0)
                    {
                        id = i;
                        tmin = t;
                        hit = true;
                    }
                }

                return hit;
            }
        }

        public bool RaycastScreenPoint(Vector2 screen, 
            out int id,
            UnityEngine.Camera camera = null)
        {
            camera = camera == null ? UnityEngine.Camera.main : camera;

            Ray ray = camera.ScreenPointToRay(screen);

            return Raycast(ray, out id);
        }

        public void RefreshColliders()
        {
            Update();
        }

        private void OnDrawGizmosSelected()
        {
            Sphere sphere;

            for (int i = 0; i < Frame.AtomCount; ++i)
            {
                sphere.position = Frame.AtomPositions[i];
                sphere.radius = ExampleMolecularStyle.ElementRadiuses[Frame.AtomTypes[i]] * Scaling;

                if (Style != null)
                {
                    sphere.radius = Style.AtomRadiuses[i];
                }

                Gizmos.DrawSphere(sphere.position, sphere.radius * Scaling);
            }
        }

        public static bool RaycastRenderer(IMMDRenderer renderer,
            Ray ray,
            out int hitID)
        {
            Sphere sphere;
            float t;
            float tmin = Mathf.Infinity;
            bool hit = false;
            hitID = 0;

            var selection = renderer.Selection;
            var frame = renderer.Frame;

            for (int i = 0; i < selection.Count; ++i)
            {
                int id = selection[i];

                sphere.position = frame.AtomPositions[id];
                sphere.radius = renderer.GetAtomBoundingRadius(id);

                if (sphere.Raycast(ray, out t) && t < tmin && t > 0)
                {
                    hitID = id;
                    tmin = t;
                    hit = true;
                }
            }

            return hit;
        }

        public static bool RaycastContext(NSBFrame frame,
            NSBSelection selection,
            NSBStyle style,
            Ray ray,
            out int hitID,
            float scaling = 1f)
        {
            Sphere sphere;
            float t;
            float tmin = Mathf.Infinity;
            bool hit = false;
            hitID = 0;

            for (int i = 0; i < selection.Count; ++i)
            {
                int id = selection[i];

                sphere.position = frame.AtomPositions[id];
                sphere.radius = style.AtomRadiuses[id] * scaling;

                if (sphere.Raycast(ray, out t) && t < tmin && t > 0)
                {
                    hitID = id;
                    tmin = t;
                    hit = true;
                }
            }

            return hit;
        }
    }
}
