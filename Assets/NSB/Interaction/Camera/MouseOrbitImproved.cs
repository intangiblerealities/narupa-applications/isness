﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Interaction.Camera
{
    [AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
    class MouseOrbitImproved : IInteraction
    {
        public float distance = 5.0f;
        public float xSpeed = 1.2f;
        public float ySpeed = 1.2f;

        public float yMinLimit = -90f;
        public float yMaxLimit = 90f;

        public float distanceMin = .5f;
        public float distanceMax = 2000f;

        public float zoomSpeed = 15f;

        float x;
        float y;

        // Use this for initialization
        void Start()
        {
            Vector3 angles = transform.eulerAngles;

            x = angles.y;
            y = angles.x;
        }

        public static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360F)
            {
                angle += 360F;
            }

            if (angle > 360F)
            {
                angle -= 360F;
            }

            return Mathf.Clamp(angle, min, max);
        }

        void LateUpdate()
        {
            /*
        Quaternion rotation = Camera.main.transform.rotation; //  Quaternion.Euler(y, x, 0);

        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, distanceMin, distanceMax);

        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + target.position;

        Camera.main.transform.rotation = rotation;
        Camera.main.transform.position = position;
        */ 
        }

        public override bool UpdateInteractions()
        {
            bool consumed = false; 

            if (Input.GetMouseButton(0))
            {
                x += Input.GetAxis("Mouse X") * xSpeed;
                y -= Input.GetAxis("Mouse Y") * ySpeed;

                y = ClampAngle(y, yMinLimit, yMaxLimit);

                consumed = true; 
            }
            else
            {
                consumed = false; 
            }

            return consumed; 
        }
    }
}