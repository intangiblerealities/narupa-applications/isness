﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Interaction
{
    public class AtomColliderInfo : MonoBehaviour
    {
        /// <summary>
        /// ID of the atom.
        /// </summary>
        public int AtomID;

        /// <summary>
        /// ID of the object that collided with the atom.
        /// </summary>
        public uint ColliderID;
    }
}