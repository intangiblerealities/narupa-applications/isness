﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano.Client;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using NSB.Simbox.Topology;
using UnityEngine;

namespace NSB.Processing
{
    public class NSBFrame
    {
        /// <summary>
        /// Map from atom indices in frontend-frame to the underlying absolute
        /// atom indices in the simulation.
        /// </summary>
        public int[] VisibleAtomMap;

        public long FrameNumber;
        public int AtomCount;
        public int BondCount;

        public int TopologyUpdateCount;

        public ushort[] AtomTypes;
        public Vector3[] AtomPositions;
        public bool AtomVelocitiesValid;
        public Vector3[] AtomVelocities;
        public bool AtomKineticEnergiesValid;
        public float[] AtomKineticEnergies;
        
        public BondPair[] BondPairs;

        public ushort[] SelectedAtomIndicies;
        public ushort[] CollidedAtomIndicies;

        public Topology Topology;
        public Dictionary<VariableName, object> Variables;
        public VRInteraction[] VRInteractions;
        public InteractionForceInfo[] InteractionForceInfo;

        public List<Rug.Osc.OscMessage> Messages = new List<Rug.Osc.OscMessage>();

        public static void Clear(NSBFrame frame)
        {
            frame.AtomCount = 0;
            frame.BondCount = 0;

            frame.AtomTypes = null;
            frame.AtomPositions = null;
            frame.BondPairs = null;
            frame.AtomKineticEnergies = null;

            frame.SelectedAtomIndicies = null;
            frame.Variables = null;
            frame.InteractionForceInfo = null;
            frame.VRInteractions = null;

            frame.TopologyUpdateCount = -1;
        }

        public static void Interpolate(NSBFrame prevFrame,
            NSBFrame nextFrame,
            NSBFrame resultFrame,
            float u)
        {
            resultFrame.VisibleAtomMap = prevFrame.VisibleAtomMap;
            resultFrame.TopologyUpdateCount = prevFrame.TopologyUpdateCount;

            resultFrame.AtomCount = Mathf.Min(prevFrame.AtomCount, nextFrame.AtomCount);
            resultFrame.BondCount = Mathf.Min(prevFrame.BondCount, nextFrame.BondCount);

            resultFrame.AtomTypes = prevFrame.AtomTypes;
            resultFrame.AtomKineticEnergies = prevFrame.AtomKineticEnergies;
            resultFrame.BondPairs = prevFrame.BondPairs;
            resultFrame.VisibleAtomMap = prevFrame.VisibleAtomMap;

            resultFrame.SelectedAtomIndicies = prevFrame.SelectedAtomIndicies;
            resultFrame.InteractionForceInfo = prevFrame.InteractionForceInfo;
            resultFrame.Topology = prevFrame.Topology;
            resultFrame.Variables = prevFrame.Variables;

            resultFrame.SelectedAtomIndicies = prevFrame.SelectedAtomIndicies;
            if (resultFrame.AtomPositions == null
                || resultFrame.AtomPositions.Length < resultFrame.AtomCount)
            {
                resultFrame.AtomPositions = new Vector3[resultFrame.AtomCount];
            }

            resultFrame.AtomVelocitiesValid = prevFrame.AtomVelocitiesValid
                                              && nextFrame.AtomVelocitiesValid;

            if (resultFrame.AtomVelocitiesValid)
            {
                if (resultFrame.AtomVelocities == null
                    || resultFrame.AtomVelocities.Length < resultFrame.AtomCount)
                {
                    resultFrame.AtomVelocities = new Vector3[resultFrame.AtomCount];
                }

                for (int id = 0; id < resultFrame.AtomCount; ++id)
                {
                    Vector3 prevVel = prevFrame.AtomVelocities[id];
                    Vector3 nextVel = nextFrame.AtomVelocities[id];

                    nextVel.x = prevVel.x * (1 - u) + nextVel.x * u;
                    nextVel.y = prevVel.y * (1 - u) + nextVel.y * u;
                    nextVel.z = prevVel.z * (1 - u) + nextVel.z * u;

                    resultFrame.AtomVelocities[id] = nextVel;
                }
            }

            for (int id = 0; id < resultFrame.AtomCount; ++id)
            {
                Vector3 prevPos = prevFrame.AtomPositions[id];
                Vector3 nextPos = nextFrame.AtomPositions[id];

                nextPos.x = prevPos.x * (1 - u) + nextPos.x * u;
                nextPos.y = prevPos.y * (1 - u) + nextPos.y * u;
                nextPos.z = prevPos.z * (1 - u) + nextPos.z * u;

                resultFrame.AtomPositions[id] = nextPos;
            }

            resultFrame.AtomKineticEnergiesValid =
                prevFrame.AtomKineticEnergiesValid && nextFrame.AtomKineticEnergiesValid;

            if (resultFrame.AtomKineticEnergiesValid)
            {
                if (resultFrame.AtomKineticEnergies == null ||
                    resultFrame.AtomKineticEnergies.Length < resultFrame.AtomCount)
                {
                    resultFrame.AtomKineticEnergies = new float[resultFrame.AtomCount];
                }
                
                for (int id = 0; id < resultFrame.AtomCount; ++id)
                {
                    var prev = prevFrame.AtomKineticEnergies[id];
                    var next = nextFrame.AtomKineticEnergies[id];

                    var interp = prev * (1 - u) + next * u;

                    resultFrame.AtomKineticEnergies[id] = interp;
                }
            }

        }

        public T GetVariable<T>(VariableName name)
        {
            object value;

            if (Variables.TryGetValue(name, out value))
            {
                return (T)value;
            }
            else
            {
                return default(T);
            }
        }
    }
}