﻿Shader "NSB/Lines To Billboards"
{
    Properties
    {
		_Scaling ("Scaling", Range(0, .05)) = 0.02
		_Sharpness ("Sharpness", Range(0, 1)) = .5
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Opaque"
            "PreviewType" = "Plane"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest Off
		Blend One One

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma geometry geom
			#pragma multi_compile_fog
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float4 color : COLOR;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
				float4 color : COLOR;
				float4 tex0 : TEXCOORD0;

				//UNITY_FOG_COORDS(1)
            };

			float _Scaling;
			float _Sharpness;

			v2f vert(appdata v)
			{
				v2f o;

				o.pos =  mul(unity_ObjectToWorld, v.vertex);
				o.color = v.color;

				//UNITY_TRANSFER_FOG(o, UnityObjectToClipPos(v.vertex));

				return o;
			}

            [maxvertexcount(4)]
            void geom(line v2f p[2], inout TriangleStream<v2f> triStream)
            {
				float3 mid = (p[0].pos + p[1].pos) * .5;

				float3 ybasis = normalize(p[1].pos - p[0].pos);
				float3 zbasis = normalize(_WorldSpaceCameraPos - mid);
				float3 xbasis = normalize(cross(ybasis, zbasis));
    
                float4 v0 = float4(p[0].pos + xbasis * _Scaling, 1.0);
                float4 v1 = float4(p[0].pos - xbasis * _Scaling, 1.0);
                float4 v2 = float4(p[1].pos + xbasis * _Scaling, 1.0);
                float4 v3 = float4(p[1].pos - xbasis * _Scaling, 1.0);
 
 				v0 = mul(unity_WorldToObject, v0);
				v1 = mul(unity_WorldToObject, v1);
				v2 = mul(unity_WorldToObject, v2);
				v3 = mul(unity_WorldToObject, v3);

				v2f pIn = p[0];
                pIn.pos = UnityObjectToClipPos(v0);
                pIn.color = p[0].color;
				pIn.tex0 = float4(p[1].color.xyz, 0);
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v1);
                pIn.color = p[0].color;
				pIn.tex0 = float4(p[1].color.xyz, 0);
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v2);
				pIn.color = p[0].color;
                pIn.tex0 = float4(p[1].color.xyz, 1);
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v3);
				pIn.color = p[0].color;
                pIn.tex0 = float4(p[1].color.xyz, 1);
                triStream.Append(pIn);
            }

            fixed4 frag(v2f i) : SV_Target
			{
				float u = smoothstep(_Sharpness * 0.5, 1 - _Sharpness * 0.5, i.tex0.w);
				fixed4 cl = lerp(i.color, i.tex0, u);
				cl.a = 1;

				//UNITY_APPLY_FOG(i.fogCoord, cl);
				//UNITY_OPAQUE_ALPHA(cl.a);

				return cl;
			}
            ENDCG
        }
    }
}
