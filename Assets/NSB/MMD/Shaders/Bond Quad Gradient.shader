﻿Shader "NSB/Bond Quad Gradient"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Palette ("Palette", 2D) = "white" {}
		_Sharpness ("Sharpness", Range(0, 1)) = .5
	}
	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"RenderType"="Opaque" 
			"PreviewType"="Plane"
		}

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest Always
		Blend One One

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#pragma multi_compile _ GRADIENT
			#include "UnityCG.cginc"
			#include "DrawingPalette.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 color : COLOR;

				UNITY_FOG_COORDS(1)
			};

			sampler2D _MainTex;
			sampler2D _Palette;
			float4 _MainTex_ST;
			fixed _Sharpness;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;

				UNITY_TRANSFER_FOG(o, UnityObjectToClipPos(v.vertex));

				return o;
			}

#ifdef GRADIENT
			fixed4 frag (v2f i) : SV_Target
			{
				half4 tex = tex2D(_MainTex, i.uv.xy);

				clip(tex.a - .9);

				fixed4 c1 = pal2Dindex(_Palette, i.color.r) * i.color.b;
				fixed4 c2 = pal2Dindex(_Palette, i.color.g) * i.color.a;

				//c2 = fixed4(.9, .4, .4, 1);
				//c1 = fixed4(.4, .4, .9, 1);

				float u = smoothstep(_Sharpness * 0.5, 1 - _Sharpness * 0.5, i.uv.y);
				fixed4 cl = lerp(c1, c2, u) * tex;

				UNITY_APPLY_FOG(i.fogCoord, cl);
				UNITY_OPAQUE_ALPHA(cl.a);

				return cl;
			}
#else
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 c = i.color;
				half4 tex = tex2D(_MainTex, i.uv.xy);

				clip(tex.a - .9);

				c *= tex;

				UNITY_APPLY_FOG(i.fogCoord, c);
				UNITY_OPAQUE_ALPHA(c.a);

				return c;
			}
#endif
			ENDCG
		}
	}
}
