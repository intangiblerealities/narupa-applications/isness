﻿Shader "NSB/MMD/Points-Billboard"
{
    Properties
    {
		_MainTex ("Texture", 2D) = "pink" {}
		_Transparency ("Transparency", float) = 1
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Geometry+12"
            "IgnoreProjector" = "True"
            "RenderType" = "Opaque"
            "PreviewType" = "Cube"
        }

        Cull Off
        Lighting Off
        ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_fog
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION; 
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
				float2 uv : TEXCOORD0;
				float4 screenPos : TEXCOORD1;
            };

			sampler2D _MainTex;

            v2f vert(appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = v.color;
				o.uv = v.texcoord;
				o.screenPos = ComputeScreenPos(o.vertex);

                return o;
            }

			float _Transparency;

            half4 frag(v2f i) : COLOR
            {
				half4 tex = tex2D(_MainTex, i.uv);

				// Screen-door transparency: Discard pixel if below threshold.
				float4x4 thresholdMatrix =
				{  1.0 / 17.0,  9.0 / 17.0,  3.0 / 17.0, 11.0 / 17.0,
				  13.0 / 17.0,  5.0 / 17.0, 15.0 / 17.0,  7.0 / 17.0,
				   4.0 / 17.0, 12.0 / 17.0,  2.0 / 17.0, 10.0 / 17.0,
				  16.0 / 17.0,  8.0 / 17.0, 14.0 / 17.0,  6.0 / 17.0
				};
				float4x4 _RowAccess = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };
				float2 pos = i.screenPos.xy / i.screenPos.w;
				pos *= _ScreenParams.xy; // pixel position
				clip(_Transparency * tex.a - thresholdMatrix[fmod(pos.x, 4)] * _RowAccess[fmod(pos.y, 4)]);

				//clip(tex.a - .9);

				half4 c = tex * i.color;

				UNITY_OPAQUE_ALPHA(c.a);

				return c;
            }
            ENDCG
        }
    }
}
