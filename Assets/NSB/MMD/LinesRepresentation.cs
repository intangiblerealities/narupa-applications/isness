﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano.Client;
using NSB.MMD.Base;
using NSB.MMD.MeshGeneration;
using NSB.MMD.RenderingTypes;
using NSB.Utility;
using UnityEngine;

namespace NSB.MMD
{
    public class LinesRepresentation : MMDRenderer
    {
        [Serializable]
        public class LinesConfig : RendererConfig
        {
            public FloatParameter Width;
            public FloatParameter Sharpness;

            protected override void Setup()
            {
                Width = AddFloat(nameof(Width), .025f, 0, .1f, dirtying: false);
                Sharpness = AddFloat(nameof(Sharpness), 1, 0, 1, dirtying: false);
            }
        }

        [Header("Setup")]
        [Tooltip("The position to orient billboards towards when using particles")]
        public Transform EyeTransform;
        public bool ForceParticles;
        public Texture2D Texture;

        [Header("Shaders")]
        [SerializeField]
        [Tooltip("Shader for rendering a mesh of colored lines as linse")]
        private Shader linesShader;
        [SerializeField]
        [Tooltip("Shader for rendering a mesh of colored lines as billboarded lines (using a geometry shader)")]
        private Shader linesToBillboardsShader;
        [SerializeField]
        [Tooltip("Shader for rendering billboarded quads via a Unity particle system")]
        private Shader quadsShader;

        private Material linesMaterial;
        private Material linesToBillboardsMaterial;
        private Material quadsMaterial;

        private int _sharpness, _scaling;

        public LinesConfig Settings = new LinesConfig();
        public override RendererConfig Config => Settings;

        private MeshRenderer linesMeshRenderer;
        private MeshFilter linesMeshFilter;
        private ParticleBatchRenderer particlesRenderer = new ParticleBatchRenderer();

        private MeshTool mesh;
        private ParticleBatchSet batches = new ParticleBatchSet();
        private Mesh bondQuad;
        private Vector3 facingTargetPosition;

        private void Awake()
        {
            AddMesh(ref linesMeshFilter, ref linesMeshRenderer);

            mesh = new MeshTool(new Mesh(), MeshTopology.Lines);
            linesMeshFilter.sharedMesh = mesh.mesh;

            linesMaterial = new Material(linesShader);
            linesToBillboardsMaterial = new Material(linesToBillboardsShader);
            quadsMaterial = new Material(quadsShader);

            _scaling = Shader.PropertyToID("_Scaling");
            _sharpness = Shader.PropertyToID("_Sharpness");

            bondQuad = new Mesh();
            MeshGeneration.MeshGeneration.GenerateBondQuad(bondQuad);
            particlesRenderer.Initialise(transform);
            particlesRenderer.SetMesh(bondQuad);
            particlesRenderer.SetMaterial(quadsMaterial);
            batches.SetBatchSize(bondQuad);
        }

        [ThreadStatic]
        private static List<BondPair> filteredBonds;
        public override void Refresh()
        {
            bool useMesh = true;

            if (linesToBillboardsMaterial.shader.isSupported && !ForceParticles)
            {
                linesMeshRenderer.enabled = true;
                linesMeshRenderer.sharedMaterial = linesToBillboardsMaterial;
                // TODO: use model scaling in the geometry shader...
                linesToBillboardsMaterial.SetFloat(_scaling, Settings.Width.Value * 0.5f * transform.lossyScale.x);
                linesToBillboardsMaterial.SetFloat(_sharpness, Settings.Sharpness.Value);
            }
            else
            {
                useMesh = false;
                linesMeshRenderer.enabled = false;
                Style.UsePalette(quadsMaterial);
            }

            if (!CheckValidity()) return;

            if (useMesh)
            {
                particlesRenderer.Clear();

                MeshGeneration.MeshGeneration.GenerateBondLines(mesh, Frame, Selection, Style);
            }
            else
            {
                if (Style.PaletteIsValid)
                {
                    quadsMaterial.EnableKeyword("GRADIENT");
                }
                else
                {
                    quadsMaterial.DisableKeyword("GRADIENT");
                }

                quadsMaterial.SetTexture("_MainTex", Texture);
                quadsMaterial.SetFloat(_sharpness, Settings.Sharpness.Value);

                filteredBonds = filteredBonds ?? new List<BondPair>();
                Selection.FilterBonds(Frame, filteredBonds);

                facingTargetPosition = transform.InverseTransformPoint(EyeTransform.transform.position);

                batches.GenerateParticles(this, filteredBonds, GenerateParticle);
                particlesRenderer.RenderBatchSet(batches);
            }
        }

        private static void GenerateParticle(LinesRepresentation data,
            BondPair bond,
            ref ParticleSystem.Particle particle)
        {
            Color32 color = default(Color32);

            if (data.Style.PaletteIsValid)
            {
                color.r = data.Style.AtomPaletteIndices[bond.A];
                color.g = data.Style.AtomPaletteIndices[bond.B];
                color.b = 255;
                color.a = 255;
            }
            else
            {
                color = Color32.LerpUnclamped(data.Style.AtomColors[bond.A],
                    data.Style.AtomColors[bond.B],
                    0.5f);
            }

            particle.startColor = color;

            Vector3 posA = data.Frame.AtomPositions[bond.A];
            Vector3 posB = data.Frame.AtomPositions[bond.B];
            Vector3 dir = FasterMath.Sub(posB, posA);

            MMDTools.BillboardBondParticle(ref particle, posA, posB, data.facingTargetPosition);

            Vector3 scale;
            scale.y = data.Settings.Width.Value;
            scale.z = dir.magnitude;
            scale.x = data.Settings.Width.Value;

            particle.startSize3D = scale;
        }
    }
}