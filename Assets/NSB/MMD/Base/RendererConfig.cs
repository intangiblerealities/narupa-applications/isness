﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NSB.MMD.Base
{
    [Serializable]
    public class RendererConfig : ISerializationCallbackReceiver
    {
        [Serializable]
        public class ConfigColor : IEquatable<ConfigColor>
        {
            public Color32 color;

            public bool Equals(ConfigColor other)
            {
                return color.r == other.color.r
                       && color.g == other.color.g
                       && color.b == other.color.b
                       && color.a == other.color.a;
            }

            public static implicit operator Color32(ConfigColor color)
            {
                return color.color;
            }

            public static implicit operator ConfigColor(Color32 color)
            {
                return new ConfigColor { color = color };
            }
        }

        [Serializable]
        public class IntParameter : Parameter<int> { }
        [Serializable]
        public class FloatParameter : Parameter<float> { }
        [Serializable]
        public class BoolParameter : Parameter<bool> { }
        [Serializable]
        public class ColorParameter : Parameter<ConfigColor> { }
    
        public class Parameter<T>
            where T : IEquatable<T>
        {
            [NonSerialized]
            public bool Dirtying;
            public string Name;
            [SerializeField]
            public T _value;
            [NonSerialized]
            public T Min, Max;
            [NonSerialized]
            public RendererConfig Config;

            public bool Constrained => !Min.Equals(Max);

            public T Value
            {
                get
                {
                    return _value;
                }

                set
                {
                    if (Dirtying && !value.Equals(_value))
                    {
                        Config.SetChanged();
                    }

                    _value = value;
                }
            }
        }

        [NonSerialized]
        public bool Dirty = true;
        [NonSerialized]
        public int Revision;
        [HideInInspector]
        public List<IntParameter> IntParameters = new List<IntParameter>();
        [HideInInspector]
        public List<FloatParameter> FloatParameters = new List<FloatParameter>();
        [HideInInspector]
        public List<BoolParameter> BoolParameters = new List<BoolParameter>();
        [HideInInspector]
        public List<ColorParameter> ColorParameters = new List<ColorParameter>();

        public RendererConfig()
        {
            Setup();
        }

        public void Reset()
        {
            IntParameters.Clear();
            FloatParameters.Clear();
            BoolParameters.Clear();
            ColorParameters.Clear();

            SetChanged();

            Setup();
        }

        protected virtual void Setup()
        {

        }

        public void SetChanged()
        {
            Dirty = true;
            Revision += 1;
        }

        public bool ResetDirty()
        {
            bool dirty = Dirty;
            Dirty = false;
            return dirty;
        }

        public void Set<T>(Parameter<T> parameter, T value)
            where T : IEquatable<T>
        {
            if (!parameter.Value.Equals(value) && parameter.Dirtying)
            {
                SetChanged();
            }

            parameter.Value = value;
        }

        public IntParameter AddInt(string name, 
            int value, 
            int min=0, 
            int max=0,
            bool dirtying=true)
        {
            var param = new IntParameter
            {
                Config = this,
                Name = name,
                Value = value,
                Min = min,
                Max = max,
                Dirtying = dirtying,
            };

            IntParameters.Add(param);

            return param;
        }

        public FloatParameter AddFloat(string name, 
            float value, 
            float min = 0, 
            float max = 0,
            bool dirtying=true)
        {
            var param = new FloatParameter
            {
                Config = this,
                Name = name,
                Value = value,
                Min = min,
                Max = max,
                Dirtying = dirtying,
            };

            FloatParameters.Add(param);

            return param;
        }

        public BoolParameter AddBool(string name,  
            bool value,
            bool dirtying=true)
        {
            var param = new BoolParameter
            {
                Config = this,
                Name = name,
                Value = value,
                Dirtying = dirtying,
            };

            BoolParameters.Add(param);

            return param;
        }

        public ColorParameter AddColor(string name,
            Color32 value,
            bool dirtying = true)
        {
            var param = new ColorParameter
            {
                Config = this,
                Name = name,
                Value = value,
                Dirtying = dirtying,
            };

            ColorParameters.Add(param);

            return param;
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
        
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            var ints = IntParameters.ToDictionary(p => p.Name, p => p.Value);
            var floats = FloatParameters.ToDictionary(p => p.Name, p => p.Value);
            var bools = BoolParameters.ToDictionary(p => p.Name, p => p.Value);
            var colors = ColorParameters.ToDictionary(p => p.Name, p => p.Value);

            Reset();

            foreach (var param in IntParameters)
            {
                int value;

                if (ints.TryGetValue(param.Name, out value))
                {
                    param.Value = value;
                }
            }

            foreach (var param in FloatParameters)
            {
                float value;

                if (floats.TryGetValue(param.Name, out value))
                {
                    param.Value = value;
                }
            }

            foreach (var param in BoolParameters)
            {
                bool value;

                if (bools.TryGetValue(param.Name, out value))
                {
                    param.Value = value;
                }
            }

            foreach (var param in ColorParameters)
            {
                ConfigColor value;

                if (colors.TryGetValue(param.Name, out value))
                {
                    param.Value = value;
                }
            }
        }
    }
}
