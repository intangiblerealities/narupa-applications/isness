﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.RenderingTypes;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace NSB.MMD.Base
{


    public class MMDInspectorConfig : MonoBehaviour 
    {
        [Header("Sources")]
        public FrameSource FrameSource;
        public SelectionSource SelectionSource;
        public StyleSource StyleSource;
        public bool IgnoreSources;

        public IMMDRenderer Renderer => GetComponent<IMMDRenderer>();
        public RendererConfig Config => Renderer.Config;

        private void Update()
        {
            if (IgnoreSources) return;

            if (    FrameSource.Linked) Renderer.Frame     =     FrameSource.Component.Frame;
            if (SelectionSource.Linked) Renderer.Selection = SelectionSource.Component.Selection;
            if (    StyleSource.Linked) Renderer.Style     =     StyleSource.Component.Style;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(MMDInspectorConfig))]
    public class MMDInspectorConfigEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var configurer = target as MMDInspectorConfig;
            var config = configurer?.Config;

            if (config != null)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField($"Configuration", EditorStyles.boldLabel);

                EditorGUI.BeginChangeCheck();

                foreach (var parameter in config.IntParameters)
                {
                    if (parameter.Constrained)
                    {
                        parameter.Value = EditorGUILayout.IntSlider(parameter.Name, parameter.Value, parameter.Min, parameter.Max);
                    }
                    else
                    {
                        parameter.Value = EditorGUILayout.IntField(parameter.Name, parameter.Value);
                    }
                }

                foreach (var parameter in config.FloatParameters)
                {
                    if (parameter.Constrained)
                    {
                        parameter.Value = EditorGUILayout.Slider(parameter.Name, parameter.Value, parameter.Min, parameter.Max);
                    }
                    else
                    {
                        parameter.Value = EditorGUILayout.FloatField(parameter.Name, parameter.Value);
                    }
                }

                foreach (var parameter in config.BoolParameters)
                {
                    parameter.Value = EditorGUILayout.Toggle(parameter.Name, parameter.Value);
                }

                if (EditorGUI.EndChangeCheck())
                {
                    EditorUtility.SetDirty(target);
                }

                if (GUILayout.Button("Reset"))
                {
                    config.Reset();

                    EditorUtility.SetDirty(target);
                }
            }
        }
    }
#endif
}