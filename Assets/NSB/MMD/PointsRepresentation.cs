﻿using NSB.MMD.Base;
using NSB.MMD.MeshGeneration;
using NSB.MMD.RenderingTypes;
using UnityEngine;

/// <summary>
/// MMDRenderer that renders the selected atoms as billboarded sprites.
/// 
/// Uses geometry shader where supported, otherwise falls back to a Unity particle
/// system. Draws a mesh of points in the special case of 0 scaling.
/// </summary>
public class PointsRepresentation : MMDRenderer
{
    [System.Serializable]
    public class PointsConfig : RendererConfig
    {
        public FloatParameter Scaling;

        protected override void Setup()
        {
            Scaling = AddFloat(nameof(Scaling), 1, dirtying: false);
        }
    }

    [Header("Shaders")]
    [SerializeField]
    [Tooltip("Shader for rendering a mesh of colored points as points")]
    private Shader pointsShader;
    [SerializeField]
    [Tooltip("Shader for rendering a mesh of colored points as billboarded sprites (using a geometry shader)")]
    private Shader pointsToBillboardsShader;
    [SerializeField]
    [Tooltip("Shader for rendering billboarded sprites via a Unity particle system")]
    private Shader quadsShader;
    [SerializeField]

    [Header("Setup")]
    [Tooltip("Texture to use as the billboarded sprite")]
    private Texture2D ParticleTexture;
    public bool ForceParticles;

    //[Section]
    public PointsConfig Settings = new PointsConfig();
    public override RendererConfig Config => Settings;

    private Material pointsMaterial;
    private Material pointsToBillboardsMaterial;
    private Material quadsMaterial;

    private MeshRenderer pointsMeshRenderer;
    private MeshFilter pointsMeshFilter;
    private ParticleBatchRenderer particlesRenderer = new ParticleBatchRenderer();

    private MeshTool mesh;
    private ParticleBatchSet batches = new ParticleBatchSet();

    private void Awake()
    {
        pointsMaterial = new Material(pointsShader);

        pointsToBillboardsMaterial = new Material(pointsToBillboardsShader);
        pointsToBillboardsMaterial.SetTexture("_MainTex", ParticleTexture);

        quadsMaterial = new Material(quadsShader);
        quadsMaterial.SetTexture("_MainTex", ParticleTexture);

        // points mesh
        AddMesh(ref pointsMeshFilter, ref pointsMeshRenderer);

        mesh = new MeshTool(new Mesh(), MeshTopology.Points);
        pointsMeshFilter.sharedMesh = mesh.mesh;

        // fallback particle system
        var quad = new Mesh(); 
        MeshGeneration.GenerateAtomQuad(quad, 1);

        particlesRenderer.Initialise(transform);
        particlesRenderer.SetMesh(quad, true);
        particlesRenderer.SetMaterial(quadsMaterial);

        batches.SetBatchSize(quad);
    }

    public override void Refresh()
    {
        bool useMesh = !ForceParticles;

        if (useMesh && Settings.Scaling.Value > 0 && pointsToBillboardsMaterial.shader.isSupported)
        {
            pointsMeshRenderer.sharedMaterial = pointsToBillboardsMaterial;
            // TODO: use model scaling in the geometry shader...
            pointsToBillboardsMaterial.SetFloat("_Scaling", Settings.Scaling.Value * transform.lossyScale.x);
        }
        else if (Settings.Scaling.Value > 0)
        {
            useMesh = false;
            quadsMaterial.SetFloat("_Scaling", Settings.Scaling.Value);
        }
        else
        {
            useMesh = true;
            pointsMeshRenderer.sharedMaterial = pointsMaterial;
        }

        if (!CheckValidity()) return;

        if (useMesh)
        {
            particlesRenderer.Clear();
            pointsMeshRenderer.enabled = true;

            MeshGeneration.GenerateAtomPoints(mesh, Frame, Selection, Style);

            // vertex per selected atom
            for (int i = 0; i < Selection.Count; ++i)
            {
                int id = Selection[i];

                Vector4 data = default(Vector4);
                data.x = Style.AtomRadiuses[id];

                mesh.uv0s[i] = data;
            }

            if (Selection.Count > 0) mesh.Apply(uv0s: true, colors: true);
        }
        else
        {
            pointsMeshRenderer.enabled = false;

            batches.GenerateParticles(this, Selection, GenerateParticle);
            particlesRenderer.RenderBatchSet(batches);
        }
    }

    private static void GenerateParticle(PointsRepresentation data,
                                         int id,
                                         ref ParticleSystem.Particle particle)
    {
        particle.position = data.Frame.AtomPositions[id];
        particle.startColor = data.Style.AtomColors[id];
        particle.startSize = data.Style.AtomRadiuses[id] * data.Settings.Scaling.Value;
    }

    public override float GetAtomBoundingRadius(int id)
    {
        if (Selection != null && !Selection.Contains(id)) return -1;

        return Style.AtomRadiuses[id] * Settings.Scaling.Value;
    }
}