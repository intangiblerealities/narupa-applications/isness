﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.Base;
using NSB.Utility;
using UnityEngine;

namespace NSB.MMD
{
    public class AtomTrailRenderer : MMDRenderer
    {
        [System.Serializable]
        public class TrailConfig : RendererConfig
        {
            public FloatParameter Scaling, Time, MinimumLength;

            protected override void Setup()
            {
                Scaling = AddFloat("Scaling", .333f, 0, 2, dirtying: false);
                Time = AddFloat("Time", .333f, 0, 15, dirtying: false);
                MinimumLength = AddFloat("Minimum Length", 1, 0.01f, 8, dirtying: false);
            }
        }

        [Header("Setup")]
        [SerializeField]
        private AnimationCurve widthCurve;
        [SerializeField]
        private TrailRenderer trailTemplate;
        private IndexedPool<TrailRenderer> trails;

        public TrailConfig Settings = new TrailConfig();
        public override RendererConfig Config => Settings;

        private static Gradient gradient = new Gradient();
        private static GradientColorKey[] gck = new GradientColorKey[2];
        private static GradientAlphaKey[] gak = new GradientAlphaKey[2];

        private void Awake()
        {
            trails = new IndexedPool<TrailRenderer>(trailTemplate, InstanceSetup: Setup);

            gck[0].time = 0.0F;
            gck[1].color = Color.black;
            gck[1].time = 1.0F;

            gak[0].alpha = 1.0F;
            gak[0].time = 0.0F;
            gak[1].alpha = 1.0f;
            gak[1].alpha = 0.0F;
            gak[1].time = 1.0F;
        }

        private void Setup(TrailRenderer trail)
        {
            trail.Clear();
        }

        public override void Refresh()
        {
            if (!gameObject.activeInHierarchy) return; 

            float limit = Settings.MinimumLength.Value;
            float scale = Settings.Scaling.Value;

            trails.SetActive(Selection.Count);

            var colors = Style.AtomColors;
            var positions = Frame.AtomPositions;
            float time = Settings.Time.Value;

            for (int i = 0; i < trails.count; ++i)
            {
                var trail = trails[i];

                float length = 0;
                int count = trail.positionCount;

                if (count >= 2)
                {
                    int step = Mathf.CeilToInt(count / 8f);
                    Vector3 prev = trail.GetPosition(0);

                    for (int n = 1; n < count; n += step)
                    {
                        Vector3 next = trail.GetPosition(n);

                        length += Vector3.Distance(prev, next);

                        prev = next;
                    }
                }

                float scale2 = scale * Mathf.InverseLerp(0, limit, Mathf.Clamp(length, 0, limit));

                Color32 color = colors[i];

                gck[0].color = color;
                gck[1].color = color;
                gradient.SetKeys(gck, gak);
                trail.colorGradient = gradient;

                trail.transform.localPosition = positions[Selection[i]];
                trail.widthMultiplier = scale2;
                trail.widthCurve = widthCurve;
                trail.time = time;
            }
        }
    }

}
