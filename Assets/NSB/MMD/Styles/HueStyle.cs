﻿using NSB.Examples.Player.Control;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD.Styles
{
    public class HueStyle : FrameFilter, IStyleSource
    {
        public float Scaling = 1f / 3f;
        [Range(0, 1)]
        public float Cycle;
        [Range(0, 1)]
        public float Saturation = 0.75f;
        [Range(0, 1)]
        public float Value = 1f;
        [Range(0, 1)]
        public float Variance;

        public NSBStyle Style = new NSBStyle();

        NSBStyle IStyleSource.Style => Style;

        private NSBFrame frame;

        [SerializeField]
        private SelectionSource highlightSource;

        private bool performForcedUpdate;

        public bool fixedHue;

        protected override void Awake()
        {
            base.Awake();

            Style.Name = name;

        }

        private void OnValidate()
        {
            performForcedUpdate = true;
        }

        public override void UpdateFilter(NSBFrame frame, bool topologyChanged = false)
        {
            Profiler.BeginSample("HueStyle.UpdateFilter");

            if (highlightSource.Linked)
            {
                Style.HighlightedAtoms = highlightSource.Component.Selection;
            }

            //if (topologyChanged || performForcedUpdate)
            {
                performForcedUpdate = false;
                Style.AtomColors.Resize(frame.AtomCount);
                Style.AtomPaletteIndices.Resize(frame.AtomCount);
                Style.AtomRadiuses.Resize(frame.AtomCount);

                this.frame = frame;

                //Parallel.For(0, frame.AtomCount, UpdateAtom);
                for (int i = 0; i < frame.AtomCount; ++i) UpdateAtom(i);

                Style.InitPalette();

                for (int i = 0; i < 256; ++i)
                {
                    Style.PaletteColors[i] = FasterMath.HSV(i / 256f, Saturation, Value);
                }

                Style.CommitPalette();
                Style.PaletteIsValid = true;
            }

            Profiler.EndSample();
        }

        private void UpdateAtom(int id)
        {
            ushort type = frame.AtomTypes[id];

            float u = id / (float) frame.AtomCount;
            float h = (u + Cycle) % 1;
            float s = Mathf.Clamp01(Saturation + (type - 16) / 32f * Variance);

            if (fixedHue) h = Cycle;

            Style.AtomColors[id] = FasterMath.HSV(h, s, Value);
            Style.AtomPaletteIndices[id] = (byte) (u * 256);
            Style.AtomRadiuses[id] = ExampleMolecularStyle.ElementRadiuses[type] * Scaling;
        }
    }
}
