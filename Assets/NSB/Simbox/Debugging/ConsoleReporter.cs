﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Net;
using System.Text;
using Nano;
using Rug.Osc;
using UnityEngine;

namespace NSB.Simbox.Debugging
{
    internal class ConsoleReporter : IReporter
    {
        private object syncLock = new object();

        public static bool LogToUnityConsole; 

        public IOscMessageFilter OscMessageFilter { get; set; }

        public ReportVerbosity ReportVerbosity { get; set; }

        public ConsoleReporter()
        {

        }

        public void PrintBlankLine(ReportVerbosity verbosity)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                Debug.Log("");
            }
        }

        public void PrintDebug(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Debug);
        }

        public void PrintDebug(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint == null ? "" : endPoint.ToString(), format, args, ReportVerbosity.Debug);
        }

        public void PrintDebug(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Debug);
        }

        public void PrintDetail(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Detail);
        }

        public void PrintDetail(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint == null ? "" : endPoint.ToString(), format, args, ReportVerbosity.Detail);
        }

        public void PrintDetail(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Detail);
        }

        public void PrintEmphasized(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Emphasized);
        }

        public void PrintEmphasized(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint == null ? "" : endPoint.ToString(), format, args, ReportVerbosity.Emphasized);
        }

        public void PrintEmphasized(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Emphasized);
        }

        public void PrintError(string format, params object[] args)
        {
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteLine(format);
                }
                else
                {
                    WriteLine(string.Format(format, args));
                }
            }
        }

        public void PrintError(Direction direction, IPEndPoint origin, string format, params object[] args)
        {
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteMessage(direction, origin.ToString(), format);
                }
                else
                {
                    WriteMessage(direction, origin.ToString(), string.Format(format, args));
                }
            }
        }

        public void PrintError(Direction direction, string ident, string format, params object[] args)
        {
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteMessage(direction, ident, format);
                }
                else
                {
                    WriteMessage(direction, ident, string.Format(format, args));
                }
            }
        }

        public void PrintException(Exception ex, string format, params object[] args)
        {
            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteException(format, ex);
                }
                else
                {
                    WriteException(string.Format(format, args), ex);
                }
            }
        }

        public void PrintNormal(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Normal);
        }

        public void PrintNormal(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint == null ? "" : endPoint.ToString(), format, args, ReportVerbosity.Normal);
        }

        public void PrintNormal(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Normal);
        }

        public void PrintOscPackets(Direction direction, params OscPacket[] packets)
        {
            foreach (OscPacket packet in packets)
            {
                if (packet is OscMessage)
                {
                    PrintOscMessage(direction, packet as OscMessage);
                }
                else if (packet is OscBundle)
                {
                    OscBundle bundle = packet as OscBundle;

                    foreach (OscPacket sub in bundle)
                    {
                        PrintOscPackets(direction, sub);
                    }
                }
            }
        }

        public void PrintOscPackets(Direction direction, IPEndPoint endPoint, params OscPacket[] packets)
        {
            foreach (OscPacket packet in packets)
            {
                if (packet is OscMessage)
                {
                    PrintOscMessage(direction, endPoint, packet as OscMessage);
                }
                else if (packet is OscBundle)
                {
                    OscBundle bundle = packet as OscBundle;

                    foreach (OscPacket sub in bundle)
                    {
                        PrintOscPackets(direction, endPoint, sub);
                    }
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteLine(format);
                }
                else
                {
                    WriteLine(string.Format(format, args));
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteMessage(direction, endPoint.ToString(), format);
                }
                else
                {
                    WriteMessage(direction, endPoint.ToString(), string.Format(format, args));
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, string ident, string format, params object[] args)
        {
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteMessage(direction, ident, format);
                }
                else
                {
                    WriteMessage(direction, ident, string.Format(format, args));
                }
            }
        }

        private void Print(string format, object[] args, ReportVerbosity reportVerbosity)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteMessage(Direction.Action, "", format);
                }
                else
                {
                    WriteMessage(Direction.Action, "", string.Format(format, args));
                }
            }
        }

        private void Print(Direction direction, string ident, string format, object[] args, ReportVerbosity reportVerbosity)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                if (args.Length == 0)
                {
                    WriteMessage(direction, ident, format);
                }
                else
                {
                    WriteMessage(direction, ident, string.Format(format, args));
                }
            }
        }

        private void PrintOscMessage(Direction direction, OscMessage oscMessage)
        {
            if (OscMessageFilter != null && OscMessageFilter.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            lock (syncLock)
            {
                WriteMessage(direction, oscMessage.Origin.ToString(), oscMessage.ToString());
            }
        }

        private void PrintOscMessage(Direction direction, IPEndPoint endPoint, OscMessage oscMessage)
        {
            if (OscMessageFilter != null && OscMessageFilter.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            lock (syncLock)
            {
                WriteMessage(direction, endPoint.ToString(), oscMessage.ToString());
            }
        }

        private bool ShouldPrint(ReportVerbosity verbosity)
        {
            return (int) ReportVerbosity <= (int) verbosity;
        }

        private void WriteException(string message, Exception ex)
        {
            Debug.LogError(message);
            Debug.LogException(ex);
        }

        private void WriteLine(string message)
        {
            Debug.Log(message);
        }

        private void WriteMessage(Direction direction, string ident, string message)
        {
            StringBuilder sb = new StringBuilder();

            switch (direction)
            {
                case Direction.Transmit:
                    sb.Append("TX ");
                    break;

                case Direction.Receive:
                    sb.Append("RX ");
                    break;

                case Direction.Action:
                    sb.Append("// ");
                    break;

                default:
                    break;
            }

            if (ident != null)
            {
                sb.Append(ident.ToString() + " ");
            }

            sb.Append(message);

#if UNITY_EDITOR
            if (LogToUnityConsole)
            {
                Debug.Log(sb.ToString());
            }
#endif
        }

        public void PrintHeading(string format, params object[] args)
        {
            throw new NotImplementedException();
        }
    }
}