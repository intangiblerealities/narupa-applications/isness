﻿namespace NSB.Examples.Utility.Tweening
{
    class Cubic : IEaser
    {
        public float EaseIn(float ratio)
        {
            return ratio * ratio * ratio;
        }

        public float EaseOut(float ratio)
        {
            return (ratio -= 1f) * ratio * ratio + 1f;
        }

        public float EaseInOut(float ratio)
        {
            return (ratio < 0.5f) ? 4f * ratio * ratio * ratio : 4f * (ratio -= 1f) * ratio * ratio + 1f;
        }
    }
}
