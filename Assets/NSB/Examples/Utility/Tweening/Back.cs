﻿namespace NSB.Examples.Utility.Tweening
{
    class Back : IEaser
    {
        protected static float s = 1.70158f;

        public float EaseIn(float ratio)
        {
            return ratio * ratio * ((s + 1f) * ratio - s);
        }

        public float EaseOut(float ratio)
        {
            return (ratio -= 1f) * ratio * ((s + 1f) * ratio + s) + 1f;
        }

        public float EaseInOut(float ratio)
        {
            return ((ratio *= 2f) < 1f) ? 0.5f * (ratio * ratio * ((s * 1.525f + 1f) * ratio - s * 1.525f)) : 0.5f * ((ratio -= 2f) * ratio * ((s * 1.525f + 1f) * ratio + s * 1.525f) + 2f);
        }

    }
}
