﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
#if UNITY_EDITOR
using UnityEditor;

namespace NSB.Examples.Utility
{
    public static class BuildAllAssetBundles
    {
        [MenuItem("Tools/Build All Asset Bundles")]
        public static void DoIt()
        {
            BuildPipeline.BuildAssetBundles("Assets/StreamingAssets/",
                BuildAssetBundleOptions.None,
                EditorUserBuildSettings.activeBuildTarget);
        }
    }
}
#endif
