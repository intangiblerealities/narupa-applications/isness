﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Examples.Utility
{
    public class InspectorAction : MonoBehaviour
    {
        [System.Serializable]
        public class Action : UnityEngine.Events.UnityEvent { };
    
        public Action onInvoked;

        public void Invoke()
        {
            onInvoked.Invoke();
        }
    }
}
