﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Linq;

namespace NSB.Examples.Utility
{
    public static partial class Extensions
    {
        public static bool ContainsIgnoreCase(this string source, string target)
        {
            return source.IndexOf(target, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }

        public static bool EqualsIgnoreCase(this string source, string target)
        {
            return string.Compare(source, target, true) == 0;
        }

        public static string NiceTypeName(this Type type)
        {
            if (type.IsGenericType)
            {
                string name = type.Name.Substring(0, type.Name.Length - 2);
                var args = type.GetGenericArguments();

                return $"{name}<{string.Join(", ", args.Select(arg => arg.Name))}>";
            }
            else
            {
                return $"{type.Name}";
            }
        }
    }
}
