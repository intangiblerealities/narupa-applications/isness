﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using Text = TMPro.TextMeshProUGUI;

namespace NSB.Examples.Utility
{
    public class DebugToUIText : MonoBehaviour
    {
        [SerializeField]
        private Text text;

        private void Start()
        {
            Application.logMessageReceived += HandleLog;
        }

        private void HandleLog(string logString, string stackTrace, LogType type)
        {
            string color = "white";

            if (type == LogType.Error || type == LogType.Exception) color = "red";
            if (type == LogType.Warning) color = "yellow";

            if (text == null)
                return;

            text.text += $"<color={color}>{logString}</color>\n";
        }
    }
}