﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Linq;
using NSB.Examples.Utility;
using NSB.Utility;
using UnityEngine;
using Text = TMPro.TextMeshProUGUI;

namespace NSB.Examples.Player
{
    public class ErrorPopup : MonoBehaviour
    {
        [SerializeField]
        private Text messageText;

        [SerializeField]
        private InstancePoolSetup buttonsSetup;
        private InstancePool<ButtonEntry> buttons;

        [SerializeField]
        private ButtonView cancelButton;

        private void Awake()
        {
            buttonsSetup.Finalise(ref buttons);
        }

        public void Show(string body, params ButtonEntry[] buttons)
        {
            gameObject.SetActive(true);

            messageText.text = body;

            if (buttons.Length > 0)
            {
                cancelButton.gameObject.SetActive(true);
                cancelButton.SetConfig(buttons[0]);
            }
            else
            {
                cancelButton.gameObject.SetActive(false);
            }

            this.buttons.SetActive(buttons.Skip(1));
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
