# README #
This repository contains the legacy version of Isness, as shown at Breaking 
Convention.

# Dependencies #
* Unity 2018.2
* FinalIK (https://assetstore.unity.com/packages/tools/animation/final-ik-14290)

# Running #
Isness requires a Narupa server to run the molecular dynamics and coordinate 
multiple participants. The server used at breaking convention is including in 
the `Isness Server.zip` archive in this repository. 

# Breaking Convention Setup
* Mudra gloves
* Two lighthouses
* Scenes 1-13
* Heart pulls: 3, 93, 193, 293
* Scene fade duration: 40s
